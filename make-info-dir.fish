#! /usr/bin/fish

# make-info-dir.fish

# Copyright 2023 cnngimenez

# Author: cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function make-info-dir --description="Creates the dir file indexing all info."
    set -l ignore_file ./index.info ./texinfo/tangled/images.info

    rm dir
    install-info ./index.info dir && echo "Adding ./index.info first";    
    for infofile in (find -name '*.info')
        if contains "$infofile" $ignore_file
            echo "Ignoring $infofile"
        else
            echo "Adding $infofile"
            install-info "$infofile" dir
        end
    end
end

set -l cmd (status current-command)
if test "$cmd" != "fish"
    return
end

make-info-dir
