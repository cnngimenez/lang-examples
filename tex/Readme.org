
* What is TeX?

# It is a macro-based language

** TODO Engines

- TeX
- XeTeX
- LuaTeX

** TODO Framework

- LaTeX
- ConTeXt

* TODO Compiling

: tex

: pdftex

* Defining characters
:PROPERTIES:
:header-args:tex: :tangle tangled/define_characters.tex :noweb yes :mkdirp yes
:END:

A character can be defined by using ~\chardef~. 

#+BEGIN_SRC tex
<<license>>   
New characters can be defined:
\chardef\D=65

\D\D\D\D?????

\char65
#+END_SRC

Always, use the ~\bye~ macro at the end of the file. Then, the compiler can stop processing it.
#+BEGIN_SRC tex
\bye          
#+END_SRC

* Assign
:PROPERTIES:
:header-args:tex: :tangle tangled/assign.tex :noweb yes
:END:

#+BEGIN_SRC tex
  <<license>>
  {\bf Let and assign.}
#+END_SRC

A macro definition can be used to store string constants. The following snippets store a short sentence inside the ~\foo~ macro.

#+BEGIN_SRC tex
  \def\foo{ A short sentence. }
#+END_SRC

An alias can be create with ~\let~. The following creates the ~\j~ alias for the ~\foo~ command.

#+BEGIN_SRC tex
  \let\j=\foo
#+END_SRC

The following command defines a macro that defines another one. First, it defines a ~\setfoo~ macro with one parameters. When called, it defines ~\foo~ with a personalized text. Thus ~\setfoo{hello}~ creates a macro ~\foo~ which prints ``hello''.

#+BEGIN_SRC tex
  \def\setfoo#1{\def\foo{#1}}
  \setfoo{this is my first text}

  First comment: \foo.
#+END_SRC

Let test it again with another value.

#+BEGIN_SRC tex
  \setfoo{this is my second text}

  Second comment: \foo
#+END_SRC


#+BEGIN_SRC tex
  \bye
#+END_SRC

* Math
:PROPERTIES:
:header-args:tex: :tangle tangled/math.tex :noweb yes
:END:

#+BEGIN_SRC tex
  <<license>>

  {\bf Numbers and math.}
#+END_SRC

Integer numbers are supported by using ``counters''. A counter can store numbers and the results of math operations. They are defined with ~\newcount~ and they are written with the command syntax.

The following snippet creates a new counter call ~\mycounter~. The next instruction stores the number 2 into it.

#+BEGIN_SRC tex      
  Integer:

  \newcount\mycounter
  \mycounter=2
#+END_SRC

To print the counter value, the ~\the~ macro is used. This macro requires one parameter, which is the counter.

#+BEGIN_SRC tex
$mycounter = $ \the\mycounter
#+END_SRC

The math operations are advance, multiply, and divide. Their syntax is ~\OPERATION\COUNTER by NUMBER~. The following snippet show how to use each of them:

#+BEGIN_SRC tex
\advance\contador by 4
$contador + 4 =$ \the\contador

\multiply\contador by 2
$contador \times 2 = $ \the\contador

\divide\contador by 2
$contador / 2 = $\the\contador
#+END_SRC

Lastly, the end of the parsing command.

#+BEGIN_SRC tex
\bye
#+END_SRC




* License
CC-By-SA 4.0
[[https://i.creativecommons.org/l/by-sa/4.0/88x31.png]]

This work is licensed under a [[http://creativecommons.org/licenses/by-sa/4.0/][Creative Commons Attribution-ShareAlike 4.0 International License]].

** TeX header
#+name: license
#+BEGIN_SRC tex
  %% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
  %% See http://creativecommons.org/licenses/by-sa/4.0/.
#+END_SRC



* Meta     :noexport:

# ----------------------------------------------------------------------
#+TITLE:  TeX and family
#+SUBTITLE:
#+AUTHOR: Christian Gimenez
#+DATE:   07 oct 2021
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 
#+COLUMNS: %40ITEM(Task) %17Effort(Estimated Effort){:} %CLOCKSUM

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick

#+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)

# -- Export
#+LANGUAGE: en
#+LINK_UP:   
#+LINK_HOME: 
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
# #+export_file_name: index

# -- HTML Export
#+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
#+HTML_LINK_UP: index.html
#+HTML_LINK_HOME: index.html
#+XSLT:

# -- For ox-twbs or HTML Export
# #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
# -- -- LaTeX-CSS
# #+HTML_HEAD: <link href="css/style-org.css" rel="stylesheet">

# #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
# #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>


# -- LaTeX Export
# #+LATEX_CLASS: article
#+latex_compiler: xelatex
# #+latex_class_options: [12pt, twoside]

#+latex_header: \usepackage{csquotes}
# #+latex_header: \usepackage[spanish]{babel}
# #+latex_header: \usepackage[margin=2cm]{geometry}
# #+latex_header: \usepackage{fontspec}
# -- biblatex
#+latex_header: \usepackage[backend=biber, style=alphabetic, backref=true]{biblatex}
#+latex_header: \addbibresource{tangled/biblio.bib}
# -- -- Tikz
# #+LATEX_HEADER: \usepackage{tikz}
# #+LATEX_HEADER: \usetikzlibrary{arrows.meta}
# #+LATEX_HEADER: \usetikzlibrary{decorations}
# #+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing}
# #+LATEX_HEADER: \usetikzlibrary{shapes.geometric}
# #+LATEX_HEADER: \usetikzlibrary{shapes.symbols}
# #+LATEX_HEADER: \usetikzlibrary{positioning}
# #+LATEX_HEADER: \usetikzlibrary{trees}

# #+LATEX_HEADER_EXTRA:

# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
