#! /usr/bin/awk -f
 
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# archivos.awk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


BEGIN {
    # awk puede tener varias salidas, la salida estándar y archivos.
    
    # Las salidas pueden ser:
    
    # /dev/stdin
    # /dev/stdout
    # /dev/stderr
    # /dev/fd/n    (el archivo asociado al descriptor n)

    # Para conexiones TCP/IP

    # /inet/tcp/lport/rhost/rpost  Para lport=local_port rhost=remote_host rport=remote_port. lhost=0 significa que el sistema escojerá un puerto local de salida. 
    # /inet4/tcp/lport/rhost/rpost
    # /inet6/tcp/lport/rhost/rpost

    # Para UDP 

    # /inet/udp/lport/rhost/rpost  Para lport=local_port rhost=remote_host rport=remote_port. lhost=0 significa que el sistema escojerá un puerto local de salida. 
    # /inet4/udp/lport/rhost/rpost
    # /inet6/udp/lport/rhost/rpost

    # La salida de un archivo se realiza simplemente con el símbolo de redirección ">" o de redirección a salida aditiva ">>".

    print "hola mundo" >"ejemplo.txt"

    close("ejemplo.txt") # Asegurarse de cerrar el archivo cuando se deje de usar o cuando se vuelve abrir para leer.
    
    # La intrada es análoga por medio de el símbolo "<"
    getline j<"ejemplo.txt"
    print j

    # También se puede usar fflush() para hacer que los bufferes se escriban en el archivo:

    print "hola">"hola.txt"
    getline a<"hola.txt"
    print "De hola.txt se lee: \"" a "\"" # no se va a leer nada si es la primera vez que se ejecuta y el archivo "hola.txt" no existe.
    
    print "chau">"chau.txt"
    fflush() # o también se puede usar fflush("chau.txt")
    getline b<"chau.txt"
    print "De chau.txt se lee: \"" b "\""


    # La diferencia entre ">" y ">>" es que el archivo se vuelve a crear de cero en el primer caso, o se agrega el contenido al final en el segundo.

    print "hola">"a.txt"
    close("a.txt") # Al cerrar el archivo "a.txt", cuando se use ">" se eliminará, pero cuando se use ">>" se abrirá para seguir escribiendo.
    print "hola otra vez" >> "a.txt"
    close("a.txt")
    print "chau">"a.txt" # a.txt se volverá a crear con "chau", borrando el contenido anterior ("hola").
    close("a.txt")
    print "nos vemos!" >> "a.txt" # al usar ">>" no se borrará el contenido anterior.
}
