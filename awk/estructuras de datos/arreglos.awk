#! /usr/bin/awk -f
 
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# arreglos.awk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


BEGIN {

    # Los arreglos son estructuras dinámicas en awk, sin límite máximo, con tipos de datos dinámicos en sus valores y sus índices.

    # Los arreglos se definen así:

    a[0] = 10
    a[1] = "hola"
    a[2] = 20 * 5
    a["hola"] = /nose*/

    # Se pueden recorrer así:

    for (i in a){
	print i "=" a[i]
    }

    # Esto no imprimirá nada (y dependiendo de ORS el valor que posee al final, por defecto una nueva línea "\n") puesto que a[3] no está definido.
    print a[3] 

    # Los parámetros de entrada del programa es un arreglo de los argumentos:
    # ARGV[0] Siempre es el nombre del programa awk usado.
    for (i in ARGV){
	print "Parámetro " i " es: " ARGV[i]
    }
}
