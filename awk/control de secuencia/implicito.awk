#! /usr/bin/awk -f
 
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# implicito.awk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# El control de secuencia implícito en AWK se basa en los siguientes pasos:
#
# 1) Tomar un registro hasta encontrar el RS
#     a) Asignar el registro a $0
# 2) Separar en campos el registro usando a FS como patrón o string divisor para el caso de ser necesario. 
#     a) Asignar cada campo a $1, $2, ...
# 3) Ejecutar el patrón "BEGIN"
# 4) Tomar el siguiente patrón del script:
#     a) Si matchea con el registro $0: ejecutar las acciones.
#     b) Si no matchea: no hacer nada.
#     c) En cualquier caso: volver al paso (4) hasta terminar todos los patrones.
#
# 5) Ejecutar el patrón "END"
#
# Obsérvese que si hay dos o más patrones que matcheen con $0, entonces se ejecutarán las acciones respectivas en el orden en que aparecen escritas.

BEGIN {
    print "Esto se ejecuta sí o sí al principio."
    # Muy útil para inicializar variables del programador y variables de control (RS, FS, etc.)
    # RS="\n" # por defecto los registros de la entrada se separan por cada nueva línea.
    # FS=" " # por defecto los campos de cada registro se separan por cada espacio.
    #
    # OFS=" " # Los campos de salida se separan por espacio (se imprime un espacio por campo).
    # ORS="\n" # Los registros de salida se separan por nueva línea (por ejemplo, 'print "hola"' imprime el registro formado por "hola" y lo separa una línea, produciendo en la salida "hola\n".
}


/^a.*b/ { # Todo string que empiece con "a" y termine con "b" realiza esta acción.
    print "Empieza con a y termina con b."
}


/a.*c.*b/{ # Toda cadena que empiece con "a", tenga a "c" en el medio y termine con "b" realiza estas acciones:
    print "Empieza con a, tiene c y termina con b."

}

/^a.*/ { # Toda cadena que empiece con "a" sin importar como termine realiza esta acción.
    print "Empieza con a."
    next # Next indica que ya se terminó con el procesado de este registro, por lo que se buscará el siguiente registro y se volverá a intentar todos los matcheos.
    # Por lo que si hay una cadena que cumpla con este patrón, las reglas que están debajo se saltearán.
}
/^ab.*/ {
    # Jamás se ejecutará esta regla por culpa del "next" en la anterior.
    print "Empieza con ab."
}

/^b.*/ { # Toda cadena que empiece con "b" hace ejecutar estas acciones:
    print "Empieza con b."
    exit 0 # Esto hace que termine el parseo.
}

END {
    print "Esto se ejecuta sí o sí al final."
}
