#! /usr/bin/awk -f

 
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# subprograma.awk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


BEGIN {
    print factorial(3)
}

END {
    print factorial(4)
}


# Función factorial 
function factorial(n){
    if (n == 0){
	return 1
    } else {
	return n*factorial(n-1)
    }
}
    
