#! /usr/bin/awk -f
 
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# registros-comas.awk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Ejecutar así:
# 
#     ./registros-comas.awk archivo-ejemplo
#

BEGIN {
    # Se separarán los registros por comas
    RS=","          # Record separator    
    #Por defecto, FS=" " 
}

/^a.*/ {
    # Se imprime el registro:
    print
    # Se imprime el registro (esto es lo mismo que lo anterior):
    print "--(" $0 ")--"
}

/^b.*/ {
    # Al empezar sólo con b se imprime el prime y segundo campo.
    print ">>>" $1 "-" $2 "<<<"
}

