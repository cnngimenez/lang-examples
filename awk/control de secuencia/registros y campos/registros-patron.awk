#! /usr/bin/awk -f
 
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# registros-patron.awk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


BEGIN {
    # Ahora usaremos un patrón. 
    # El divisor de registros será toda cadena que empiece con 0 o más veces "a", tenga una "b" y luego siga con 0 o más veces "a".
    # Lo que se describe en RS no es la divisón, sino qué forma tendrá cada registro.
    RS=/a*ba*/
    # Por defecto: FS=" "
}




/.*/ {
    # imprimir los registros.
    print "(" $0 ")"
    print ">>" $1 "-" $2 "<<"
    next
}

END {
}
