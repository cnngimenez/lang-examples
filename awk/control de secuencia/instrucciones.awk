#! /usr/bin/awk -f
 
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# instrucciones.awk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Para el control de secuencia explícito a nivel instrucciones se cuenta con iterativas y selecciones

BEGIN {
    # Selección simple de dos vías
    print "selección de dos vías"
    a = 0
    if (a == 1) {
	print "a es 1"
    }else{
	print "a no es 1"
    }

    # Selección de vías múltiples
    print "selección de múltiples vías"

    s = "hola"
    switch (s){
    case "hola": {print "es \"hola\"."}
    case /.*ol.*/ : {print "Tiene ol en el medio."}
    case "chau": {print "es \"chau\"."}
    default: print "Ninguna de las anteriores."
    }

    # Iterativas con prueba lógica
    print "iterativa con prueba lógica."
    print "while ..."
    i = 0
    while (i < 20){
	print i
	i = i + 1
    }

    print "do ... while"
    i = 0
    do {
	print i
	i = i + 1
    } while (i < 20)


    # Iterativas basada en contador
    print "iterativa basada en contador"
    for (i=1; i < 20; i++) {
	print i
    }

    # Iterativas basada en estructuras de datos
    print "iterativa basada en estr. datos."
    arreglo[0] = 1
    arreglo[20] = 2
    arreglo[3] = 3
    for (indice in arreglo){
	print indice "=" arreglo[indice]
    }
    
}
