Aquí encontrará una serie de archivos de ejemplos para varios lenguajes de programación.

Se intentará explayar sobre:

- Estructuras de control
- Tipos de datos
    - Clases
    - Estructuras
- Comandos de E/S
- Archivos



# Licencias
A menos que ese indique lo contrario, se aplica lo siguiente:

- los códigos de programación en este proyecto se encuentran bajo la licencia GNU General Public License versións 3 (GPLv3). Véase el arhivo [COPYING-GPL.txt](COPYING-GPL.txt) o visite el sitio Web [http://www.gnu.org/licenses/gpl.html](http://www.gnu.org/licenses/gpl.html).
- textos, imágenes, sonidos y videos se encuentra bajo la Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional. Véase el sitio Web [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).



Se adjuntan copias de las siguientes licencias:

- [GPLv3](COPYING.txt) (Clic [aquí](http://www.gnu.org/licenses/gpl.html) para ver GPLv3 en http://www.gnu.org). ![gplv3](http://www.gnu.org/graphics/gplv3-88x31.png)

