Aquí encontrará una serie de archivos de ejemplos para varios lenguajes de programación.

Se intentará explayar sobre:

- Estructuras de control
- Tipos de datos
    - Clases
    - Estructuras
- Comandos de E/S
- Archivos



# Licencias

Se intentarán utilizar las licencias libres acorde al lenguaje(GPLv3 mayormente).

Cáda código tendrá su respectivo encabezado indicando la licencia.



Se adjuntan copias de las siguientes licencias:

- [GPLv3](COPYING.txt) (Clic [aquí](http://www.gnu.org/licenses/gpl.html) para ver GPLv3 en http://www.gnu.org). ![gplv3](http://www.gnu.org/graphics/gplv3-88x31.png)

