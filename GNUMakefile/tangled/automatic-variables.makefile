names = $(basename $(wildcard ../example-files/*.tex))
pdfs = $(addsuffix .pdf,$(names))

.PHONY: shownames all

all: $(pdfs)

shownames:
	@echo $(names)

%.pdf: ../example-files/%.tex
	@echo 'Runing rule %.pdf:'
	@echo '$$@ = ' $@
	@echo '$$% = ' $%
	@echo '$$< = ' $<
	@echo '$$? = ' $?
	@echo '$$^ = ' $^
	@echo '$$+ = ' $+
	@echo '$$| = ' $|
	@echo '$$* = ' $*
	@echo '$$(@D) = ' $(@D) 
	@echo '$$(@F) = ' $(@F) 
	@echo '$$(*D) = ' $(*D) 
	@echo '$$(*F) = ' $(*F) 
	@echo '$$(%D) = ' $(%D) 
	@echo '$$(%F) = ' $(%F) 
	@echo '$$(<D) = ' $(<D) 
	@echo '$$(<F) = ' $(<F) 
	@echo '$$(^D) = ' $(^D) 
	@echo '$$(^F) = ' $(^F) 
	@echo '$$(+D) = ' $(+D) 
	@echo '$$(+F) = ' $(+F) 
	@echo '$$(?D) = ' $(?D) 
	@echo '$$(?F) = ' $(?F)
