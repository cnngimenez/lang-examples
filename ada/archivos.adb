-- archivos.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO; 

procedure Archivos is
   F1 : File_Type;
begin
   -- modos: 
   -- in_file, out_file, append_file
   Open(F1, In_File, "example.txt"); 
   
Read_All:
    while not End_Of_file(F1) loop
       Put_Line(Get_Line(F1));
    end loop Read_All;
    
    Close(F1);
   
end Archivos;
