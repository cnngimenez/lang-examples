with Ada.Text_IO;

-- Cualquier procedimiento puede ser el programa principal de Ada. 
-- No debe llamarse main necesariamente, en este caso se llama Hello.
procedure Hello_World is
begin
   Ada.Text_IO.Put_Line("Hello world!");
end Hello_World;


-- Programa obtenido desde Wikipedia.
