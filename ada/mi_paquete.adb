-- mi_paquete.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

package body Mi_Paquete is
   
   procedure Un_Procedimiento(Par: in Integer) is
      A : Integer;
   begin
      A := Par;
   end Un_Procedimiento;
   
   function Una_Funcion (Par:in Integer; Par2:in Integer) return Integer is
   begin
      if Par > Par2 then
	 return Par;
      else
	 return Par2;
      end if;	
   end Una_Funcion;
   
end Mi_Paquete;
