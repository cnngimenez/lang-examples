-- recorrer.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Characters.Latin_1;
with Ada.Strings;
with Ada.Text_Io;
use Ada.Characters.Latin_1;
use Ada.Strings;
use Ada;

procedure Recorrer is
   S : String := ("hola mundooooo" & Lf & "como estas" & Lf & "bien?");
begin
   Text_Io.Put_Line("length : " & Integer'Image(S'Length));
   Text_Io.Put_Line(S);
   
Recorrer_Loop:   
   for I in Integer range 1..S'Length loop
      Text_Io.Put(Integer'Image(Character'Pos(S(I))));
      Text_Io.Put(":");
      Text_Io.Put(Character'Image(S(I)));
      Text_Io.Put(" - ");
      -- Text_Io.Put(S(I)); 
      
   end loop Recorrer_Loop;
   
end Recorrer;
