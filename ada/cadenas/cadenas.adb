-- cadenas.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

-- Ada soporta tres tipos de strings
-- (están ordenados de mayor eficiencia a menor y de menor flexibilidad o dinamismo a mayor):
--
-- * Fixed-length o Strings de tamaño fijo: Debido a que usan asignación de memoria secuencial y estática.
-- * Bounded-length o Strings de tamaño ligado: Debido a que usan asignación de memoria secuencial y en la pila.
-- * Unbounded-length o Strings sin tamaño ligado: Debido a que usan asignación de memoria por chunks o no secuencial y en la pila.

with Ada;
use Ada;

with Ada.Strings;
use Ada.Strings;
with Ada.Strings.Bounded;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

with Ada.Text_Io;
use Ada.Text_Io;

procedure Cadenas is
   -- Observar: ¡está dentro del procedure! 
   -- Ada.Strings.Bounded. Generic_Bounded_Length es un paquete genérico (que no posee ciertas cosas declaradas).
   -- Bounded strings requieren de una instanciación del paquete (cual si fuera una clase).
   package SB is new Ada.Strings.Bounded.Generic_Bounded_Length ( Max => 200 );
   
   Str1: String := "|---hi world 1!---|"; -- Requiere inicialización
   Str1b: String(1..20); -- También se puede indicar la longitud con un subrango.
   Str2: SB.Bounded_String;
   Str3: Unbounded_String;
   
   I : Integer;
   Str_I : String := "100";
begin
   -- Saltará un Warning y un Constraint_error en tiempos de ejecución:
   -- el largo de la cadena asignada no es el indicado...
   -- Seguramente el chequeo se realiza en tiempos de ejecución y capás que se puede sacar con algún parámetro de compilación.
   -- Str1 := "hi world 1!"; 
   
   -- No saltará ni surgirá error un warning: usé justamente 20 caracteres! :-P
   
   Str1b := "hi world 1b!-------|"; 
   Str1 := "|---hi world 2!---|";
   
   Str2 := SB.To_Bounded_String("hi world 2!"); -- Ada.Strings.Bounded_String (en realidad la instanciación de este paquete).
   Str3 := To_Unbounded_String("hi world 3!"); -- Ada.Strings.Unbounded.To_Unbounded_String(...)
   
   
   Put_Line(Str1);
   Put_Line(Str1b);
   Put_Line(SB.To_String(Str2)); 
   
   -- No hay confución, se usa Ada.String.Unbound.To_String ya que se determina por medio del parámetro:
   Put_Line(To_String(Str3)); 
   
   --
   -- Conversions
   --
   
   -- conversión entre integer -> string
   I := 11;
   Str_I := Integer'Image(I);
   I := I + 1;
   Str_I := I'Img;
   I := I + 1;
   
   -- string -> integer
   I := Integer'Value(Str_I);
   
   Put_Line(Str_I);
   Put_Line(Integer'Image(I));
   
   -- Wide_Wide_String -> String
   
   To_String
   
   
end Cadenas;
