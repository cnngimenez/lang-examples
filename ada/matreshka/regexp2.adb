-- regexp2.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Wide_Wide_Text_IO;
with Ada.Text_IO;

with Ada.Strings;
use Ada.Strings;

with League.Regexps;
use League.Regexps;

with League.Strings;
use League.Strings;

procedure Regexp2 is
   Expression : League.Strings.Universal_String;
   Str : League.Strings.Universal_String;
   Pattern : Regexp_Pattern;
   Match : Regexp_Match;
begin
   
   -- ¡Guarda! Estamos usando instancias del tipo Wide_Wide_String en vez el String convencional.
   Str :=  League.Strings.To_Universal_String( "aaabaaa");
   Expression := League.Strings.To_Universal_String("aa*ba*");
   Pattern := Compile(Expression); -- Debemos compilar el patrón, como se hace en C.
   Match := Pattern.Find_Match(Str); -- Match es el objeto que almacena los resultados.
   
   -- Para imprimir en terminal usamos Wide_Wide_Text_IO.Put/Put_Line, el Text_IO usa instancias de String, este usa Wide_Wide_String's.
   Ada.Wide_Wide_Text_IO.Put_Line("String = '" & To_Wide_Wide_String(Str) & "'");
   Ada.Wide_Wide_Text_IO.Put_Line("Expression = '" & To_Wide_Wide_String(Expression) & "'");
   
   -- Is_Matched es una función con un solo parámetro: Self. 
   -- Ver en /usr/include/matreshka/league/league-regexps.ads.
   if Match.Is_Matched then
      Ada.Wide_Wide_Text_Io.Put_Line("Match found:" &
				       Integer'Wide_Wide_image(Match.First_Index) &
				       " .. " &
				       Integer'Wide_wide_image(Match.Last_Index) &
				       " => '" & 
				       League.Strings.To_Wide_Wide_String(Match.Capture) &
				       "'");
   end if;
   
end Regexp2;
