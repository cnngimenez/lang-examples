-- regexps.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

-- Obtenido de http://forge.ada-ru.org/matreshka/wiki/League/Regexp
with Ada.Command_Line;
with Ada.Strings.Wide_Wide_Fixed;
with Ada.Wide_Wide_Text_IO;

with League.Regexps;
with League.Strings;

procedure Regexps is

   function Read (File_Name : String) return League.Strings.Universal_String;

   ----------
   -- Read --
   ----------

   function Read (File_Name : String) return League.Strings.Universal_String is
      File   : Ada.Wide_Wide_Text_IO.File_Type;
      Buffer : Wide_Wide_String (1 .. 1024);
      Last   : Natural;

   begin
      Ada.Wide_Wide_Text_IO.Open
        (File, Ada.Wide_Wide_Text_IO.In_File, File_Name, "wcem=8");
      Ada.Wide_Wide_Text_IO.Get_Line (File, Buffer, Last);
      Ada.Wide_Wide_Text_IO.Close (File);

      return League.Strings.To_Universal_String (Buffer (1 .. Last));
   end Read;

   Expression : League.Strings.Universal_String :=
     Read (Ada.Command_Line.Argument (1));
   String     : League.Strings.Universal_String :=
     Read (Ada.Command_Line.Argument (2));
   Pattern    : League.Regexps.Regexp_Pattern :=
     League.Regexps.Compile (Expression);
   Match      : League.Regexps.Regexp_Match :=
     Pattern.Find_Match (String);

begin
   if Match.Is_Matched then
      Ada.Wide_Wide_Text_IO.Put_Line
        ("Match found:"
           & Integer'Wide_Wide_Image (Match.First_Index)
           & " .."
           & Integer'Wide_Wide_Image (Match.Last_Index)
           & " => '"
           & League.Strings.To_Wide_Wide_String (Match.Capture)
           & "'");

      for J in 1 .. Match.Capture_Count loop
         Ada.Wide_Wide_Text_IO.Put_Line
           ("         \"
              & Ada.Strings.Wide_Wide_Fixed.Trim
	      (Integer'Wide_Wide_Image (J), Ada.Strings.Both)
              & ":"
              & Integer'Wide_Wide_Image (Match.First_Index (J))
              & " .."
              & Integer'Wide_Wide_Image (Match.Last_Index (J))
              & " => '"
              & League.Strings.To_Wide_Wide_String (Match.Capture (J))
              & "'");
      end loop;

   else
      Ada.Wide_Wide_Text_IO.Put_Line ("Not matched");
   end if;
end Regexps;
