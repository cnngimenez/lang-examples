-- subprogramas.ads --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

package Subprogramas is
   
   procedure Proc1(A,B: in Integer; C:out Integer);
   procedure Proc2(A : in out Integer);
   function Func1(A : in Integer) return Integer;
   function Func2(A : out Integer) return Integer;
   
end Subprogramas;
