-- alias.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------


with Ada.Text_Io;

-- Debe estar dentro de un procedure o package:

procedure Alias is

   --  function Us(Item : Wide_Wide_String) return League.Strings.Universal_String
   --    renames League.Strings.To_Universal_String;
   
   --
   -- El alias (Pl en este caso) debe poseer la misma signatura al que renombra.
   --
   procedure Pl(Item : String) renames Ada.Text_Io.Put_Line;
  
begin
   Pl("hola mundo");
end Alias;
