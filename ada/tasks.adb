-- task.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Numerics;
with Ada.Numerics.Discrete_Random;
with Ada.Text_Io;
use Ada.Text_Io;

procedure Tasks is
   
   subtype Random_Type is Integer range 10 .. 40;
   package Random_Pack is new Ada.Numerics.Discrete_Random(Random_Type);
   
   G : Random_Pack.Generator;
   
   task type Imprimir is
      --
      -- Las tareas pueden comunicarse usando entradas.
      -- Son como procedimientos, pero son ejecutadas por las tareas que los definen y no por las tareas "llamadoras".
      -- La tarea "llamadora" es suspendida hasta que la llada realice la entrada.
      entry Empezar(Num : in Integer);
      
      entry Terminar;
   end Imprimir;

   task body Imprimir is
      
      --
      -- Recordar que las tareas son independientes de las demás.
      -- Por consiguiente, sólo comparten las variables del ambiente englobador.
      -- Si se pasa por parámetro algo, es necesario copiar la información a alguna estructura interna.
      --
      
      Ya : Boolean ;
      Fin : Boolean := False;
      Veces : Integer := 0;
      Num_Interno : Integer;
      Max : Integer;
   begin -- Todo este cuerpo se ejecuta en paralelo.      
      while not Fin loop 
	 Ya := False;
	 
	 Put("Loop comenzado...");
	 Put(Integer'Image(Veces));
	 Put_Line(" veces");
	 
	 --
	 -- Aquí hay un punto de encuentro, cuando alguien llama a Empezar, tanto uno como el otro se esperan:
	 -- * Imprimir espera a que alguien llame a Empezar.
	 -- * El llamador espera a que Imprimir llege al "accept Empezar" y termine de ejecutar su cuerpo.
	 --
	 -- El select se utiliza para indicar que el punto de encuentro a usarse puede ser Empezar o Terminar, 
	 -- así la primer tarea que llame a Empezar o Terminar será ejectada, sino se usara select entonces 
	 -- esta tarea (Imprimir) esperaría primero por Empezar y luego por un Terminar.
	 -- 
	 select accept Empezar(Num : in Integer) do
	    Ya := True;
	    Num_Interno := Num;
	    
	    Random_Pack.Reset(G);
	    Max := Random_Pack.Random(G);
	    
	    Put("Se imprimirán ");
	    Put(Integer'Image(Max));
	    Put(" cantidad de ");
	    Put(Integer'Image(Num));
	    Put_Line("'s:");
	 end Empezar;
	 or accept Terminar do
	    Fin := True;
	 end Terminar;
	 end select;
	 
	 if Ya then
	 Forloop:
	    for  I in Integer range 1..Max loop
	       Put(Integer'Image(Num_Interno));
	    end loop Forloop;
	    
	    Veces := Veces + 1;
	 end if;

      end loop;
   end Imprimir;
   
   Imps : array (1..4) of Imprimir;
   
   
   task Imp2;
   task body Imp2 is
   begin
      for I in Integer range 1..40 Loop
	 Put("b");
      end loop;
   end Imp2;
   
begin
   Put_Line("Ejecutar varias veces para ver cómo cambia el resultado debido al paralelismo.");
Mainloop:
   for I2 in Integer range 1..4 loop
      Imps(i2).Empezar(I2);
      Put(">"); Put(Integer'Image(I2)) ; Put_Line("<");
      end loop Mainloop;
      
   for I2 in Integer range 1..4 loop
      Imps(I2).Terminar;      
   end loop;
end Tasks;
