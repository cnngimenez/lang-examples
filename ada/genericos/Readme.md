# Genéricos

Los genéricos son como templates: poseen algo que no está definido al momento de escribir el código.

Por ejemplo:

* falta un tipo de dato
* falta alguna función o procedimiento dado.
* otro

Se puede hacer genéricos (templates) lo siguiente:

* procedimientos y funciones.
* tipos de datos.
* paquetes.

# Definición

## Procedimientos

```
generic
   type Elemento is private;
   procedure Swap(X, Y : in out Elemento);
```

## Paquetes

`Elemento` es un parámetro del template.

```
generic
   type Elemento is private;
package Intercambio is
   procedure Swap(X, Y : in out Elemento);
private
end Intercambio;
```

## Tipos

Se utiliza `<>` o `private` para indicar en el generico:

```
generic
    type ...
```

Por ejemplo:

* `type Tipo is private` indica que el tipo de dato hay que definirlo para Tipo.
* `type Tipo(<>) is private` indica que el tipo de dato hay que definirlo y se debe usar una asignación inicializadora.
* `type Tipo is (<>)` cualquier dato discreto (Integer, modular o enumeration).
* `type Tipo is range <>` cualquier entero con signo.
* otros.


# Uso

Los generics utilizan como "parámetros", estos se pueden dar (orden de paridad) según el orden de definición o se puede indicar.

## Procedimientos

`procedure UnNombre is new GenericProc(Par1, Par2, ...)`

Par1 y Par2 se pueden designar con paridad explícita.


Ej.:

```
type Int is range 1..255;
procedure Swap_Int is new Swap(Int);
```

O con paridad explícita:

```
type Int is range 1..255;
procedure Swap_Int is new Swap(Elemento => Int);
```

## Paquetes


Dentro de un procedure:

```
procedure Algo is
   type Int is range 1..255;
   package MiInstancia is new Intercambio(
      Elemento => Int;
   );

begin

...

end Algo;
```
