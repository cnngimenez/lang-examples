
with Gnat.Sockets;
use Gnat.Sockets;

with Ada.Text_Io;

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

procedure Simple_Server is
   Address : Sock_Addr_Type;
   Server : Socket_Type;
   Socket : Socket_Type;
   Channel : Stream_Access;
   Client_Addr : Sock_Addr_Type;
   Terminar : Boolean;
   Message : Unbounded_String;
begin
   Address.Addr := Addresses(Get_Host_By_Name(Host_Name), 1);
   
   Address.Port := 8000;
   
   Create_Socket(Server);
   
   Set_Socket_Option(Server, Socket_Level, (Reuse_Address, True));
   
   Bind_Socket(Server, Address);
   
   Ada.Text_Io.Put_Line("Listening...");   
   Listen_Socket(Server);   
   Ada.Text_Io.Put_Line("Accepting...");   
   Accept_Socket(Server, Socket, Address);
   
   Channel := Stream(Socket);
   Client_Addr := Get_Address(Channel);
   
   Ada.Text_Io.Put_Line(
			"Recieving from " & 
			  Image(Client_Addr)
		       );
   
   Terminar := False;
   
   delay 0.2;
   
   Ada.Text_Io.Put_Line("Recieving...");
   Message := Unbounded_String'Input(Channel);
   Ada.Text_Io.Put_Line(To_String(Message));
   Unbounded_String'Output(Channel, Message);
   
   Ada.Text_Io.Put_Line("Done Recieveing.");   
   
   Ada.Text_Io.Put_Line("Good bye!");
   Close_Socket(Socket);
   Close_Socket(Server);
   
end Simple_Server;
