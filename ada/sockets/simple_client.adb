--

with Gnat.Sockets; use Gnat.Sockets;

with Ada.Text_Io;
with Ada.Exceptions; use Ada.Exceptions;

procedure Simple_Client is
   Address  : Sock_Addr_Type;
   Socket   : Socket_Type;
   Channel  : Stream_Access;
begin

   --  See comments in Ping section for the first steps

   Address.Addr := Addresses (Get_Host_By_Name (Host_Name), 1);
   Address.Port := 8000;
   Create_Socket (Socket);

   Set_Socket_Option
     (Socket,
      Socket_Level,
      (Reuse_Address, True));

   --  Force Ping to block

   delay 0.2;

   --  If the client's socket is not bound, Connect_Socket will
   --  bind to an unused address. The client uses Connect_Socket to
   --  create a logical connection between the client's socket and
   --  a server's socket returned by Accept_Socket.

   Connect_Socket (Socket, Address);

   Channel := Stream (Socket);

   --  Send message to server Pong

   String'Output (Channel, "Hello world");

   --  Force Ping to block
   delay 0.2;

   --  Receive and print message from server Pong

   Ada.Text_IO.Put_Line (String'Input (Channel));
   Close_Socket (Socket);

end Simple_Client;
