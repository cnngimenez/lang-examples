-- telnetserver.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Gnat.Sockets; use Gnat.Sockets;
with Ada.Text_Io; use Ada.Text_Io;

procedure Telnetserver is
   Address : Sock_Addr_Type;
   Server : Socket_Type;
   Socket : Socket_Type;
   Channel : Stream_Access;
   Client_Addr : Sock_Adr_Type;   
begin
   Address.Addr := Addresses(Get_Host_By_Name(Host_Name),1);
   Address.port := 8000;
end Telnetserver;
