-- sockets_example.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Gnat.Sockets;
use Gnat;

procedure Sockets_Example is
   Sock:Sockets.Socket_Type;
   Server : Sockets.Sock_Addr_Type := "127.0.0.1";
begin

   Sockets.Create_Socket (Sock);
   Sockets.Connect_Socket (Sock, Server);
   
   -- Here Sock is connected to the remote server
   -- Use Sockets.Stream to convert Sock to a stream
   
   -- First send the integer 42
   Int'Write (Sockets.Stream (Sock), 42);
   
   -- Now send the array
   Int_Array'Write(Sockets.Stream(Sock), (1 => 42, 2 => 128, 3 => 6));
end Sockets_Example;
