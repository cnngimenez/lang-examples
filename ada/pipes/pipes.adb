-- pipes.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_Io;
use Ada.Text_Io;

procedure Pipes is
   F1 : File_Type;
begin    
   Open(F1, Append_File, "thepipe.pipe");
   
   loop
   declare
      S : String := Get_Line(F1);
   begin
      -- Put("--> Recieved : ");
      -- Put_Line(S);
      Put_Line(F1, S);
   end;
   end loop;
   
   Close(F1);   
end Pipes;
