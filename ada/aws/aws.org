
* URL Encoding
Use the Encode function for changing spaces and symbols.

Import the [[file:~/Ada/installs/include/aws.static/aws-url.ads][Aws.Url]] package.

#+begin_src ada :tangle out/urlenc.adb
with Aws.Url;
use Aws;
#+end_src

#+begin_src ada :tangle out/urlenc.adb
with Ada.Text_Io;
with Ada.Strings.Unbounded;
use Ada.Text_Io;
use Ada.Strings.Unbounded;
#+end_src

#+begin_src ada :tangle out/urlenc.adb
procedure Urlenc is
#+end_src

Calling the [[file:~/Ada/installs/include/aws.static/aws-url.ads::function%20Encode][Encode]] function will change spaces and other characters into ASCII codes.

#+begin_src ada :tangle out/urlenc.adb
      Strenc: String := Url.Encode("http://startpage.com/?q=hello world");
#+end_src


#+begin_src ada :tangle out/urlenc.adb
begin
    Put_Line(Strenc);
end Urlenc;
#+end_src

** Gpr file 

#+begin_src ada :tangle out/urlenc.gpr
  with "aws.gpr";

  project urlenc is
    
      for Source_Files use ("urlenc.adb");
      for Main use ("urlenc.adb");
    
  end urlenc;
#+end_src


* Get a Page
:PROPERTIES:
:header-args: :tangle out/getpage.adb
:END:

The first step is importing the AWS libraries needed.

#+begin_src ada
with AWS.Url;
with AWS.Response;
with AWS.Client;
with AWS.Containers.Tables;
with AWS.Headers;

use AWS;
#+end_src

This is a library for printing the response and processing Unbounded_Strings. The Argument library helps to retrieve the URL string from the command line when invoking the program.

#+begin_src ada 
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Text_Io;
use Ada.Text_Io;
with Ada.Command_Line;
use Ada.Command_Line;
#+end_src

Start the main procedure and set a defalut URL as as simple string. Use a Url variable to store the actual URL to visit.

#+begin_src ada 
procedure Getpage is
    Default_Url : constant string := "http://www.startpage.com";
    Url : Unbounded_String;
#+end_src

The [[file:~/Ada/installs/include/aws.static/aws-response.ads::type%20Data%20is%20private;][Data]] type stores the response information (headers, cookies and body).

#+begin_src ada 
    Rdata : Response.Data;
#+end_src

An Unbounded_String is used for storing the string data.

#+begin_src ada 
    Resp : Unbounded_String;
#+end_src

This procedure list all headers in the given list to standar output.

#+BEGIN_SRC ada
procedure Put_Headers (Headers : AWS.Headers.List) is
    use AWS.Containers.Tables;
    header_table : constant Table_Type := Table_Type (Headers);
begin
    for I in Positive'First .. Count (Header_Table) loop
        Put_Line (Get_Name (Header_Table, I)
                    & ": "
                    & Get_Value (Header_Table, I));
    end loop;
end Put_Headers;
#+END_SRC


#+BEGIN_SRC ada
begin
#+END_SRC

First check if the user provides any argument. Use the default URL when there is no argument.

#+BEGIN_SRC ada
    if Argument_Count = 1 then
        Url := To_Unbounded_String (Argument (1));
    else
        Url := To_Unbounded_String (Default_Url);    
    end if;
#+END_SRC


#+begin_src ada      
    Put ("Retrieving data from: ");
    Put_Line (To_String (Url));
#+end_src

The function [[file:~/Ada/installs/include/aws.static/aws-client.ads::function%20Get][Get]] retrieves the Web page through Internet. 

#+begin_src ada 
    RData := Client.Get (URL => To_String (Url));
#+end_src

#+begin_src ada 
    Put_Line ("Done.");
#+end_src 

Show all header data, such as status code, content length, and content type.

#+BEGIN_SRC ada
    Put_Headers (Response.Header (Rdata));

    Put_Line ("Status code: " &
                Response.Status_Code (Rdata)'Image);
    Put_Line ("Content length: " &
                Response.Content_Length (Rdata)'Image);
#+END_SRC


The [[file:~/Ada/installs/include/aws.static/aws-response.ads::function%20Message_Body%20(D%20:%20Data)%20return%20String%20with%20Inline;][Message_Body]] function returns the string in the HTTP body response. It returns a String, Unbounded_String or a Stream_Element_Arary. 

#+begin_src ada 
    Resp := Response.Message_Body (Rdata);
#+end_src

Print the Response.

#+begin_src ada 
    Put_Line (To_String (Resp));

end Getpage;
#+end_src

** Gpr file

#+begin_src gpr :tangle out/getpage.gpr
with "aws.gpr";

project getpage is
    
    for Source_Files use ("getpage.adb");
    for Main use ("getpage.adb");

    package Compiler is
        for Default_Switches ("Ada") use
          (
           "-g",  --  with debugging symbols
           "-gnatef"  --  print full path
          );
    end Compiler;
    
end getpage;
#+end_src


* Meta     :noexport:

  # ----------------------------------------------------------------------
  #+AUTHOR:    Giménez, Christian
  #+DATE:      31 mar 2019
  #+EMAIL:
  #+DESCRIPTION: 
  #+KEYWORDS: 

  #+STARTUP: inlineimages hidestars content hideblocks entitiespretty indent fninline latexpreview
  #+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)
  #+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
  #+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc tex:imagemagick
  #+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:~/emacs_stuff/dists/scripts/org-info.js
  #+EXPORT_SELECT_TAGS: export
  #+EXPORT_EXCLUDE_TAGS: noexport
  #+LINK_UP:   
  #+LINK_HOME: 
  #+XSLT:

  # Local Variables:
  # org-hide-emphasis-markers: t
  # org-use-sub-superscripts: "{}"
  # fill-column: 80
  # org-src-preserve-indentation: t
  # End:
