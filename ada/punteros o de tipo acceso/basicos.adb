-- basicos.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Unchecked_deallocation;
use  Ada;


with Ada.Text_Io;
use  Ada.Text_Io;

-- Algunas cosas fueron obtenidas del wikilibro (wikibooks.org) en inglés "Ada Programming".
procedure Basicos 
is
   type Person is record
      First_Name : String(1..10);
      Last_Name : String(1..10);
   end record;
   
   type Person_Access_A is access Person; -- Aquí se está definiendo un pool de objetos, no un puntero a un solo objeto.
   
   -- for Person_Access_A'Size use 0; -- Esto implica que el pool no contendrá nada: no se puede asignar ningún espacio para un objeto.
   -- No se puede usar un número < 64 en GNAT! :-)
-- También se puede designar un Storage_Pool personalizado así:
-- for Person_Access_A'Storage_Pool use My_Pool;


   type Person_Access is access Person;
   
   Father : Person_Access := new Person; -- no inicializado
   Mother : Person_Access := new Person'("Juana     ", "Rodriguez "); -- inicializado
   
   procedure Free_Person is new Unchecked_Deallocation(Object => Person, Name => Person_Access);
   
begin

   -- Se accede al puntero usando .all:
   Father.all.Last_Name := "Gonzales  ";
   Father.all.First_Name := "Pedro     ";
   Mother.all := (Last_Name => Father.Last_Name, First_Name => Mother.First_Name); -- casamiento
   
   Mother := new Person'("1234567890", "0987654321");

   -- También se puede utilizar el acceso implícito.
   Put_Line(Mother.First_Name);
   Put_Line(Mother.Last_Name);
   
   -- Se puede liberar el puntero de la siguiente forma (ver Free_Person):
   Free_Person(Mother);
   Put_Line(Mother.First_Name); -- error! :-/
   
end Basicos;
