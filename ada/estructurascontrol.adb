with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Strings;
use Ada.Strings;

procedure EstructurasControl is   
   X: Integer := 3; 
   I: Integer := 0;
   Arr : array (1..20) of Integer;
   Str : String := "Hola Mundo";
   begin
   -- No usa short circuit, para eso se debe usar "and then" y "or else" en vez de "and" y "or".
   if 1 < 2 then
      Put_Line ("1 < 2");
   elsif 1 = 2 then
      Put_Line("1 = 2");
   else
      Put_Line("1 >= 2");
   end if;
   
   case X is 
      when 1 =>
	 Put_Line("X = 1");
      when 2 =>
	 Put_Line("X = 2");
      when 3 | 4 =>
	 Put_Line("X = 3 o 4");
      when others => 
	 Put_Line("otro");
   end case;
   
   -- endless loops
   
   --  Endless_Loop:
   --      loop
   --  	  Put_Line("no end");
   --      end loop Endless_Loop;
   
   X := 1;
While_Loop:
    while X <= 5 loop
       X := X + 1;
       Put_Line(Integer'Image(X));
    end loop While_Loop;
    
Until_Loop:
    loop 
       X := X - 1;
       Put_Line(Integer'Image(X));
       exit Until_Loop when X <= 0;
    end loop Until_Loop;
    
    -- exit ... when ... puede ser usado en el medio de la repetitiva para salir del ciclo cuando se cumpla una condición.
    
    UntilB_Loop:
    loop 
       X := X + 1;
       
       exit UntilB_Loop when X > 3;
       
       Put_Line(Integer'Image(X));

    end loop UntilB_Loop;
    
For_Loop:
    for I in Integer range 1..10 loop
       Put_Line(Integer'Image(I));
    end loop For_Loop;
    
    
    
ForArr_Loop:
    for I in Arr'Range loop
       Arr(I) := I;
       Put_Line(Integer'Image(Arr(I)));
    end loop ForArr_Loop;
    
    
   String_Loop:
      for I in Str'Range loop
	 Put(Integer'Image(I));
	 Put(" : ");
	 Put(Character'Image(Str(I)));
	 Put(" : ");
	 Put_Line(Integer'Image(
				Character'Pos(Str(I))
			       ));
      end loop String_Loop;
      
end EstructurasControl;



