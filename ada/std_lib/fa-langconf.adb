-- fa-langconf.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Strings;
use Ada.Strings;
with Ada.Wide_Wide_Text_Io;
use Ada.Wide_Wide_Text_Io;

with League.Strings;
use League.Strings;

package body Fa.Langconfig is 
   
   function To_Us(Item : Wide_Wide_String) return League.Strings.Universal_String
     renames League.Strings.To_Universal_String;

   
   procedure Get_Langregexps (F1 : in File_Type ; Lr : out Langregexps ) is
      aux : Wide_Wide_String;
   begin
      aux := Get_Line(F1);
      Lr.Name := To_Us(Aux);
      Aux := Get_Line(F1);
      Lr.Subprog := To_Us(Aux);
      Aux := Get_Line(F1);
      Lr.Decname := To_Us(Aux);
      Aux := Get_Line(F1);
      Lr.Arg := To_Us(Aux);
      Aux := Get_Line(F1);
      Lr.Ret := To_Us(Aux);
   end Get_Langregexps;
   
   procedure Parse_File(File : in String ; Langregexps : out Langs_Lst) is 
      F1: File_Type;
      Aux : Langregexps;
   begin
      Open(F1, In_File, File);
      
   Read_All:
      while not End_Of_File(F1) loop
      Get_Langregexps(F1, Aux);
      end loop Read_All;
      
      Close(F1);
   end Parse_File;
      
end Fa.Langconfig;
