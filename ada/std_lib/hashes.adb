-- hashes.adb --- 

-- Copyright 2016 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------


with Ada.Containers;
with Ada.Containers.Hashed_Maps;
with Ada.Text_Io;
use Ada.Text_Io;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

procedure Hashes is
   function Hash_Fnc(Key : Integer) return Ada.Containers.Hash_Type is
   begin
      return Ada.Containers.Hash_Type (Key);
   end Hash_Fnc;

   function Eq_Fnc (Left, Right : Integer) return Boolean is
   begin
      return Left = Right;
   end Eq_Fnc;

   -- function "=" (Left, Right : Element_Type) return Boolean is <>;

   package Hashtype is new Ada.Containers.Hashed_Maps(Key_Type => Integer, 
						      -- ¡No funciona con String! 
						      -- El problema: No sabe el tamaño máximo de cada string.
						      Element_Type => Unbounded_String, 
						      Hash => Hash_Fnc, 
						      Equivalent_Keys => Eq_Fnc);

   procedure Imprimir(Index : Hashtype.Cursor) is
      Elt : constant Unbounded_String := Hashtype.Element(Index);
      Key : constant Integer := Hashtype.Key(Index);
   begin
      Put(Integer'Image(Key));
      Put(" -> ");
      Put_Line(To_String(Elt));
   end Imprimir;
   
   H : Hashtype.Map;
begin 
   
   H.Include(1, To_Unbounded_String("hola"));
   H.Include(3, To_Unbounded_String("mundo"));
   
   H.Iterate(Imprimir'Access);   
end Hashes;
