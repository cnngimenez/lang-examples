-- vectores.adb --- Ejemplo de vectores.

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_Io;
use Ada.Text_Io;
with Ada.Containers.Vectors;

-- La diferencia con un array de 200 elementos es que el array ya reserva espacio en memoria
-- de 200 elementos, lo cual ocupa mucho.
-- Un vector, en cambio, no: ocupa lo que necesita dando un máximo de 200 elementos por el índice.

procedure Vectores is   

   type Index is range 1..200;
   -- Un máximo de 200 elementos. ¡Podemos definir todo el máximo que deseamos!
   -- No se crearán 200 espacios sino que se reservará lo necesario.
   package Int_Vec is new Ada.Containers.Vectors(
						 Index_Type => Index,
						 Element_Type => Integer
						);
   V : Int_Vec.Vector;
   Max : Integer;

begin

   Put_Line("Insertando num: 1.");

   V.Append(1);

   Max := Integer(V.Capacity);
   
   Put_Line("Max capacity: " & Integer'Image(Max));


end Vectores;
