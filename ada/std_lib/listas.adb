-- listas.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Containers.Doubly_Linked_Lists;
with Ada.Text_Io;
use Ada.Text_Io;

procedure Listas is 

   package Declist is new Ada.Containers.Doubly_Linked_Lists(Element_Type => Integer);
   
   Lista : Declist.List;
   Cant : Integer;
   Pos : Declist.Cursor;
   
   procedure Imprimir(Index : Declist.Cursor) is
      
      -- Elto es el elemento, no hace falta referenciar a la lista.
      -- Aparentemente el cursor Index lo apunta a la lista de origen.
      -- ¡Observar que es constante!
      Elto : constant Integer := Declist.Element(Index); 
      
   begin
      Put_Line(Integer'Image(Elto));
   end Imprimir;
   
begin
   -- agregando tres elementos...

   Lista.Append(15);
   Lista.Append(22);
   Lista.Append(34);
   
   Cant := Integer(Declist.Length(Lista));
   
   Put_Line("Cantidad de elementos");
   Put_Line(Integer'Image(Cant));
   
   Put_Line("El segundo elemento es:");
   Pos := Lista.First;
   Declist.Next(Pos);
   Put_Line(Integer'Image(Declist.Element(Pos)));
   
   -- Imprimiendo todo
   Put_Line("Imprimiendo todo:");
   Lista.Iterate(Imprimir'Access);
   
end Listas;
