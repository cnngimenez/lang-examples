-- subprogramas.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;

package body Subprogramas is

   procedure Proc1(A,B: in Integer; C:out Integer) is
   begin
      if A > B then
	 C := A;
      else 
	 C := B;
      end if;
   end Proc1;

   procedure Proc2(A : in out Integer) is
   begin
      A := A + 1;
   end Proc2;


   -- out no debería usarse en una función.
   function Func1(A : in Integer) return Integer is
   begin
      return A + 1;
   end Func1;
   
   function Func2(A : out Integer) return Integer is
   begin
      A := 20;
      return 20;
   end Func2;

   
end Subprogramas;
