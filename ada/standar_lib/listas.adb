-- listas.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_Io;
with Ada.Containers.Doubly_Linked_Lists;

procedure Listas is
   package  Lst_Int is new Ada.Containers.Doubly_Linked_Lists (Element_Type => Integer);

   L : Lst_Int.List;  
   
   procedure Mostrar(Item : Lst_Int.Cursor) is
      Element : Integer ;
   begin
      Element := Lst_Int.Element(Item); -- No se le entrega la lista L, algo raro...

      Ada.Text_Io.Put_Line(Integer'Image(Element));
   end Mostrar;
begin
   L.Append(10);
   L.Append(11);
   
   L.Iterate(Mostrar'Access);
end Listas;
