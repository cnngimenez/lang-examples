-- task.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

procedure Tasks is
   task Imprimir is
      --
      -- Las tareas pueden comunicarse usando entradas.
      -- Son como procedimientos, pero son ejecutadas por las tareas que los definen y no por las tareas "llamadoras".
      -- La tarea "llamadora" es suspendida hasta que la llada realice la entrada.
      entry Empezar(Str : in String);
   end Imprimir;

   task body Imprimir is
   begin
      accept Empezar(Str : in String) do

      Forloop:
	 for  I in Integer range 1..10 loop
	    Put_Line(Str);
	 end loop;

      end Empezar;
   end Imprimir;

begin
end Tasks;
