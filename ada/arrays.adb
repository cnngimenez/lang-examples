-- arrays.adb --- 

-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

-- Asegúrese de aprender primero el tipo subrango.

procedure Arrays is
   -- Tamaño conocido Fixed:
   type Positivo is mod 255; -- Tipo de entero sin signo (positivos) 
			     -- esto es lo mismo que:
			     -- type Positivo is range 0..255;

   type Farr is array (Positivo) of Integer; 
   type Farrb is array (Integer range 1..20) of Integer; 
   type Farrc is array (1..20) of Integer; -- lo mismo que el de arriba, pero más resumido.

   -- se puede usar cuálquier tipo de dato discreto:
   type Char_Counter is array (Character range 'A'..'Z') of Integer;

   -- Tamaño desconocido al codear, pero conocido en tiempos de compilación.
   -- si se desea de tamaño aleatorio, usar Ada.Containers.Vector.
   -- se utiliza el box ("<>").
   type Uarr is array (Integer range <>) of Integer;


   A1 : Farr;
   A2 : Farrb;
   A3 : Farrc;
   A4 : Char_Counter;
   A5 : Uarr(1..30); -- necesita inicialización...
   
   -- ¡¡¡No hace falta declarar los índices!!!
   
   -- P : Positivo;
   -- I : Integer;
begin

Init_A1:
   -- Los índices se declaran automáticamente.
   for P in Positivo range 1..10 loop -- ¡se debe usar un rango de los positivos!
      A1(P) := Integer(P); -- se debe hacer un casting explícito con el valor de A1(P) sino surgira error.
   end loop Init_A1;

Init_A2:
   for I in Integer range 1..20 loop
      A2(I) :=
	I + 1;
   end loop Init_A2;

Init_A3:
   for I in 1..20 loop
      A3(I) := I + 2;
   end loop Init_A3;


end Arrays;
