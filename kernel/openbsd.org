
* Adding a module to the kernel
The OpenBSD kernel is completely monolitic, it has no dynamic modules. All modules must be programmed and compiled with the kernel itself.

Then, to add a module, you need to download the source code from a mirror. Sources are divided into three part: sys which is the kernel sources, src the OpenBSD system, www the webpages, and xenocara. This is explained at https://www.openbsd.org/anoncvs.html, and a list of mirrors is at https://www.openbsd.org/ftp.html.

The following script downloads sys from a mirror and creates a CUSTOM kernel. Compilation takes too long at first. However, after modifying a file from the source, make will compile only the required ones. It is assumed that an AMD 64 bits architecture computer is used.

#+caption: Download the kernel sources from a particular mirror.
#+BEGIN_SRC fish
  cd 
  wget -c https://ftp.jaist.ac.jp/pub/OpenBSD/7.2/sys.tar.gz
  tar -xvzf sys.tar.gz
#+END_SRC

#+caption: Compiles the downloaded kernel.
#+BEGIN_SRC fish  
  cd sys/arch/amd64/conf
  cp GENERIC CUSTOM
  # edit CUSTOM # uncomment to edit and configure.
  config CUSTOM
  cd ../compile/CUSTOM
  make
#+END_SRC

The config command creates the Makefile inside sys/arch/amd64/compile/CUSTOM. It adds any required C file, such as modules and dependencies, to be compiled by cc and make.

In this work, when a file is referenced as sys/dev/rnd.c, it means the file inside the extracted directory created by the tar command shown before.

** Understanding important elements to add a module
In this work, dev files are files at /dev/*. They are also known as "nodes". There are two types of dev files: character and block files. Character dev files sends data byte by byte, and block dev files uses cache to send by byte blocks. This files, if root provides access, can be red and written by regular users with =dd,= =cat=, or =echo= terminal commands.

The /dev/MAKEDEV creates all device nodes with =mknod= command. In other words, dev files must be created by a privileged user (root) using a terminal command.

The sys/arch/amd64/amd64/conf.c has the major number and the driver function association. Each module must be added to the =cdevsw[]= array, informing the kernel which function can use for specific events. If a new module must be added, it can be inserted at the last module. At OpenBSD 7.2 the last module is inserted at index 100 of this array. A comment is written next to the added declaration to indicate the array index to the reader.

  - There is a =bdevsw[]= and =cdevsw[]= tables with modules/drives declarations. Warning: there is a structure with the same names!
  - cdevsw is for character devices, bdevsw is for block devices.
  - The cdevsw index is the major number.

The sys/conf.h defines the =cdev_*_init macros= to associate each dev stage function.

  - Defines the cdevsw and bdevsw structures (not the arrays!) with several function pointers as its fields.
  - Define a =dev_init= macro. It creates the function name to register. It concatenates the module name with the open, close, ioctl, and other suffixes. Therefore, it creates the function name to point.
  - Defines all =cdev_*_init= macros. They help to create the cdevsw structure for a specific device. Some are reused for more than one device. It uses the =dev_init= macro to create pointers for the functions.

For example, the /dev/random (or /dev/urandom) is a dev file already present in the OpenBSD system. It is created and managed by the kernel because of the following items:

- The sys/dev/rnd.c file defines the functions implementations for randomopen, randomclose, randomread, randomwrite, and randomioctl.
- sys/conf.h defines =cdev_random_init(c, n)= which creates the cdevsw structure definition for random.
- sys/arch/amd64/amd64/conf.c uses =cdev_random_init(1, random)= at index 45 to add it to the cdevsw array (which it is an array with elements of type =struct cdevsw=, both the type and array have the same name).
- kernel drivers without associated devices, do not require to be included on the configuration file such as GENERIC or GENERIC.MP.
- sys/conf/files list all files to be ¿?

** Summary: Steps to add a hello module
Then, a new module can be added by following these steps:

1. Create the C file to manage the device, for example dev/hello.c.

   It must define helloopen(), helloclose(), helloread(), hellorwrite(), helloioctl(), and hellokqfilter() functions. See the example at Section [[*The sys/dev/hello.c file][The sys/dev/hello.c file]]. 
      
2. Add =cdev_random_init(1,hello)= at the end of =cdevsw= array at sys/arch/amd64/conf.c.

   The =cdev_random_init= macro is used here, but a specific macro can be created for hello.c. This macro states the functions defined on the file that the kernel should use.
3. Add =cdev_decl(hello)= at sys/conf.h.

   \thinkingface{} I think this step is not needed, and without it, it reduces some compilation time.
4. Add =file dev/hello.c= line at sys/conf/files.
5. Run =config CUSTOM= to add the new file to Makefiles at sys/arch/amd64/compile/CUSTOM.

The defined functions in hello.c file are not minimal. In case another ones are used, the =cdev_random_init= macro should not be included, and a new =cdev_hello_init= macro must be defined, stating the functions the kernel can use.

* A hello world module

To add this module to the kernel

** The sys/dev/hello.c file
:PROPERTIES:
:header-args:c: :tangle tangled/hello.c :mkdirp yes
:END:

The hello.c file manages the device file. It is the driver itself.

In this example, the file is based on sys/dev/rnd.c file. However, all random generation code were deleted and printf statements added to debug and understand the code.

*** Includes
#+BEGIN_SRC c
#include <sys/param.h>
#include <sys/event.h>
#include <sys/ioctl.h>
#include <sys/malloc.h>
#include <sys/timeout.h>
#include <sys/atomic.h>
#include <sys/task.h>
#include <sys/msgbuf.h>
#include <sys/mount.h>
#include <sys/syscallargs.h>
#include <sys/systm.h> /* printf uiomove */
#+END_SRC

*** Variables
It is important to avoid clashing with the variables. The modules are part of the kernel, and many variables are already defined. For instance, an =index= variable is present, therefore if any other index defined here should be prefixed with =hello=.

#+BEGIN_SRC c
char *hellomsg = "Hello world!";
#+END_SRC

*** helloopen
This function is called when the user try to open the dev file.

#+BEGIN_SRC c
int
helloopen(dev_t dev, int flag, int mode, struct proc *p)
{
  printf("hello: helloopen()\n");  
  return 0;
}
#+END_SRC

*** helloclose
#+BEGIN_SRC c
int
helloclose(dev_t dev, int flag, int mode, struct proc *p)
{
  printf("hello: helloclose()\n");
  return 0;
}
#+END_SRC

*** helloread
This function is called when the user try to read the device file. First, it reports to the message terminal the intent, and then move the message to the user buffer.

The =uiomove= function copy 11 bytes of the contents of =hellomsg= to the =uio= structure. This structure represent data movement, and it can be from or to user or kernel space. In this case, it moves the string stored on kernel memory to user memory.

#+BEGIN_SRC c
  int
  helloread(dev_t dev, struct uio *uio, int ioflag)
  {
    printf("hello: helloread()\n");
    uiomove(hellomsg, 11, uio);  
    return 0;
  }
#+END_SRC

*** hellowrite
#+BEGIN_SRC c
int
hellowrite(dev_t dev, struct uio *uio, int flags)
{
  printf("hello: hellowrite()\n");
  return 0;
}
#+END_SRC

*** hellokqfilter
#+BEGIN_SRC c
int
hellokqfilter(dev_t dev, struct knote *kn)
{
  switch (kn->kn_filter) {
  case EVFILT_READ:
    break;
  case EVFILT_WRITE:
    break;
  default:
    return (EINVAL);
  }

  return (0);
}
#+END_SRC

*** helloioctl

#+BEGIN_SRC c
int
helloioctl(dev_t dev, u_long cmd, caddr_t data, int flag, struct proc *p)
{
  switch (cmd) {
  case FIOASYNC:
    /* No async flag in softc so this is a no-op. */
    break;
  case FIONBIO:
    /* Handled in the upper FS layer. */
    break;
  default:
    return ENOTTY;
  }
  return 0;
}
#+END_SRC

** Adding the module to cdevsw
Now the module must be added to the cdevsw array. The file sys/arch/amd64/amd64/conf.c defines this array and its elements. The hello module should be added at this arrays as show in the following code:

#+BEGIN_SRC c :tangle no
  struct cdevsw	cdevsw[] =
  {
    cdev_cn_init(1,cn),		/* 0: virtual console */
    cdev_ctty_init(1,ctty),		/* 1: controlling terminal */
    // ...
    cdev_ujoy_init(NUJOY,ujoy),	/* 100: USB joystick/gamecontroller */
    cdev_random_init(1, hello), /* 101: The hello module can be added here */
  };
#+END_SRC

Now, the kernel has the open, close, read, write and other functions registered. They will be called at the proper device events when the user tries to access the dev file.

** Creating the dev file
No dev file (node) are created automatically. In fact, a script /dev/MAKEDEV is used to create all OpenBSD dev files the system requires. This script uses the =mknode= command to create them, which is very similar to the Linux one.

The following instruction creates the hello node as a character device file, and with major number 101. The major number is the index at cdevsw which the kernel associates the device file with the module which manages it. In our case, =cdevsw[101]= has our hello module definition (=cdev_random_init(1, hello)=). Then, to create the dev file execute:

: mknode /dev/hello c 101 0

Now, the kernel associates it with the functions declared at =cdevsw[101]=. Any access to this file triggers the corresponding function declared in the array. In our case, they were implemented at sys/dev/hello.c.

** Testing the module
To test the module, recompile the kernel, copy it to your root directory (for example, run: =doas cp sys/arch/amd64/compile/CUSTOM/obj/bsd /bsdcustom=), and reboot the computer. It is recommended to avoid erasing or modifying the =/bsd= file, leaving it untouched, in order to use it whenever the custom kernel fails.

When the computer starts, it will ask for a kernel filename. If you copied the kernel to =/bsdcustom=, type "bsdcustom" at the prompt and it will start the new kernel.

After loading the system, log-in, and type the following to move 10 bytes from /dev/hello to a file called "output":

: dd if=/dev/hello of=output count=10
: cat output

* Meta     :noexport:

# ----------------------------------------------------------------------
#+TITLE:  Openbsd Kernel Programming
#+SUBTITLE:
#+AUTHOR: Christian Gimenez
#+DATE:   30 sep 2023
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 
#+COLUMNS: %40ITEM(Task) %17Effort(Estimated Effort){:} %CLOCKSUM

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick

#+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)

# -- Export
#+LANGUAGE: en
#+LINK_UP:   
#+LINK_HOME: 
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
# #+export_file_name: index

# -- HTML Export
#+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
#+HTML_LINK_UP: index.html
#+HTML_LINK_HOME: index.html
#+XSLT:

# -- For ox-twbs or HTML Export
# #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
# -- -- LaTeX-CSS
# #+HTML_HEAD: <link href="css/style-org.css" rel="stylesheet">

# #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
# #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>


# -- LaTeX Export
# #+LATEX_CLASS: article
#+latex_compiler: xelatex
# #+latex_class_options: [12pt, twoside]

#+latex_header: \usepackage{csquotes}
# #+latex_header: \usepackage[spanish]{babel}
# #+latex_header: \usepackage[margin=2cm]{geometry}
# #+latex_header: \usepackage{fontspec}
# -- biblatex
#+latex_header: \usepackage[backend=biber, style=alphabetic, backref=true]{biblatex}
#+latex_header: \addbibresource{tangled/biblio.bib}
# -- -- Tikz
# #+LATEX_HEADER: \usepackage{tikz}
# #+LATEX_HEADER: \usetikzlibrary{arrows.meta}
# #+LATEX_HEADER: \usetikzlibrary{decorations}
# #+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing}
# #+LATEX_HEADER: \usetikzlibrary{shapes.geometric}
# #+LATEX_HEADER: \usetikzlibrary{shapes.symbols}
# #+LATEX_HEADER: \usetikzlibrary{positioning}
# #+LATEX_HEADER: \usetikzlibrary{trees}

# #+LATEX_HEADER_EXTRA:

# --  Info Export
#+TEXINFO_DIR_CATEGORY: Language Examples
#+TEXINFO_DIR_TITLE: Openbsd Kernel Programming: (kernel/openbsd)
#+TEXINFO_DIR_DESC: How to create Openbsd drivers.
#+TEXINFO_PRINTED_TITLE: Openbsd Kernel Programming
#+TEXINFO_FILENAME: openbsd.info


# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
