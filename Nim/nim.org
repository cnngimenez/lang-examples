#+PROPERTY: header-args :mkdirp yes :padline yes :comments both

Nim is a statically typed compiled systems programming language. 

Efficient, expressive and elegant are the maxims of Nim.

- Homepage :: https://nim-lang.org/
- Manual :: https://nim-lang.org/docs/manual.html

Two tools are important to remember: ~nim~ and ~nimble~. The latter is used for creating, compiling, documenting whole projects (program or libraries) and as a package manager. There are other tools like ~c2nim~ (create stubs from headers for using C functions in Nim).

* Hello World
  :PROPERTIES:
  :header-args: :mkdirp yes :comments no :padline yes :tangle out/hello.nim
  :END:

  The following code is a simple program to say hello and read a value from standard input.
  
  #+BEGIN_SRC nim
  echo "Hello World"
  let name = readline(stdin)
  echo "Hi ", name, "!"
  #+END_SRC

Compiling this code requires the Nim compiler. 

: nim compile hello.nim

The code can be executed through the compiled binary or by running:

: nim c -r hello.nim
  
Release version can be generated with the ~-d:release~ flag.

** Typed Hello World
:PROPERTIES:
:header-args: :tangle out/hello2.nim
:END:

Nim uses type inference. That is the reason why the variable hasn't got the type. The following code does the same as above:

#+BEGIN_SRC nim
echo "Hello World"
var name: string = readline(stdin)
echo "Hi ", name, "!"
#+END_SRC

* Comments
A comment start with numeral character and continues up to the end of the line.

: # comments

The multiline comments starts with ~#[~ characters and end with ~]#~.

Also the ~discard~ statement can be used with a string:

: discard "a comment here"

* Data Types

** Booleans

** Characters
** Strings
Strings literals are enclosed in double quotes. Backslash characters are interpreted as in C. Raw strings are declared with the r-prefix and do not interpret backslash.

Raw string: ~r"Hello world"~

** Numbers
Numbers accepts underscore for better readability. ~1_000_000~ is the same as ~1000000~. Floating point literals must have got a dot: ~1.0e9~ 

Hexadecimals literals are prefixed with ~0x~, binary with ~0b~ and octal with ~0o~.

*** Integers
There are several built-in types: ~int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64~

Type suffix can be used for declaring a literal of a specific integer type:

: 0'i8   # int8
: 0'i64  # int64
: 0'u    # uint

*** Floats
Same as integers, floats have got subtypes: ~float, float32, float64~

*** Conversions
Integer are not converted automatically to float, nor viceversa. Use ~toInt~ and ~toFloat~ procs for type conversions. 

** Enumerations
#+BEGIN_SRC nim :tangle no
type
  Direction = enum 
    north, east, south, west
#+END_SRC

** Ordinal
Enumerations, integers, char and bool are ordinal types. They supports some common operations: ~ord(x), inc(x), inc(x,n), dec(x), dec(x,n), succ(x), succ(x,n), pred(x), pred(x,n)~.

** Subranges
A subrange is a range of values from an integer or enumeration type.

#+BEGIN_SRC nim
type 
  MySubrange = range[0..5]
#+END_SRC

A ~Natural~ is defined at the system library which is defined as a subrange. Avoid using unsigned integer as a natural number substitution because of the unsigned arithmetic compiler usage.

** Sets
Set's basetype can only be an ordinal type of certain size: ~int8-int16, uint8-uint16, char, enum~.

The following example create a ~CharSet~ type and the ~x~ variable. It set the ~x~ set with charcters from "a" to "z" and from "0" to "9".

#+BEGIN_SRC nim
type
  CharSet = set[char]
var x : CharSet
x = {'a' .. 'z', '0' .. '9'}
#+END_SRC

There are common operations supported by sets: ~+, *, -, ==, <=, <, e in A, e notin A, contains(A, e), card(A), incl(A, e), excl(A, e)~

*** Bit fields
:PROPERTIES:
:header-args: :tangle out/bitfields.nim
:END:

The following example defines four bit names: A, B, C, D. After that, it declares the type ~MyFlags~ as a set of the bit names.

#+BEGIN_SRC nim
type
  MyFlag* {.size: sizeof(cint).} = enum
    A
    B
    C
    D
  MyFlags = set[MyFlag]
#+END_SRC


These procedures transforms the bit fields into numbers and vice-versa.

#+BEGIN_SRC nim
proc toNum(f: MyFlags): int = cast[cint](f)
proc toFlags(v: int): MyFlags = cast[MyFlags](v)
#+END_SRC

The following sentences are all true. See how the bit field A setted return 1 (as in 0b0001) and A, C setted return 5 (as in 0b0101)

#+BEGIN_SRC nim
assert toNum({}) == 0
assert toNum({A}) == 1
assert toNum({D}) == 8
assert toNum({A, C}) == 5
assert toFlags(0) == {}
assert toFlags(7) == {A, B, C}
#+END_SRC

** Arrays
:PROPERTIES:
:header-args: :tangle out/array_type.nim
:END:

Arrays are simple fixed length container. The index type can be any ordinal type.

An array is declared by giving the index and the type between ~[]~.

#+BEGIN_SRC nim
type
  IntArray = array[0..4, int]
#+END_SRC

An array variable can be assigned inside the length.

#+BEGIN_SRC nim
var
  x: IntArray
x = [1, 2, 3, 4, 5]
#+END_SRC

Accessing the array elements is achieved by using the ~[i]~ operand.

#+BEGIN_SRC nim
for i in low(x)..high(x):
  echo x[i]
#+END_SRC

There are built-in procs: ~len(a), low(a)~ and ~high(a)~.

** Sequences
:PROPERTIES:
:header-args: :tangle out/seq_type.nim
:END:

Sequences are dynamical length containers. They are created with ~seq[TYPE]~ and initialized with ~@[ELTS]~. The ~newSeq[TYPE]()~ can be used instead of ~@~ operand. ~SEQ.add(ELT)~ should be used for adding new elements as the ~SEQ[NEW_INDEX]~ raise an out of bounds exception.

The following code creates a new variable which contains integer numbers. It also initializes the variable with four numbers.

#+BEGIN_SRC nim
var
  x: seq[int]
x = @[1,2,3,4]
#+END_SRC

The ~for~ statement use the ~items()~ or ~pairs()~ iterator implicitly. In the following snippet, the ~pairs()~ iterator is used.

#+BEGIN_SRC nim
for i, value in @[4,5,6,7]:
  echo "index:", $i, ", value:", $value
#+END_SRC

** Open Arrays
:PROPERTIES:
:header-args: :tangle out/open_arrays.nim
:END:

Procedures sometimes requires to use an array of unknown length. For this reason, open array parameters can be used.

Open array's Indexes are always integer types starting from zero. The ~len(a), low(a)~ and ~high(a)~ operations are available.

For example, some array or sequence variables are needed.

#+BEGIN_SRC nim
var
  fruits: seq[string]
  capitals: array[3, string]

capitals = ["New York", "London", "Berlin"]
fruits.add("Banana")
fruits.add("Mango")
#+END_SRC

Then, the code should define a procedure with an open array parameter. 

#+BEGIN_SRC nim
proc oa_size(oa: openArray[string]) :int = 
  oa.len
#+END_SRC

Finnaly, it asserts if the values returned are correct. The parameter of the arguments are different in both calls: the firsts one is a sequence and the second one is an array. Both must contain string elements.

#+BEGIN_SRC nim
assert oa_size(fruits) == 2
assert oa_size(capitals) == 3
#+END_SRC

** Varargs
:PROPERTIES:
:header-args: :tangle out/varargs.nim
:END:

Varargs implement passing variable number of arguments to a procedure. The compiler converts these arguments into an array.

#+BEGIN_SRC nim
proc write_args(a: varargs[string]) = 
  for elt in a:
    write(stdout, elt)
  write(stdout, "\n")

write_args("1", "2", "3", "4")
#+END_SRC

The compiler transforms the varargs argument into an array after the call is issued.  

#+BEGIN_SRC nim
write_args(["1", "2", "3", "4"])
#+END_SRC

Varargs will perform type conversions if the conversion function is provided.

#+BEGIN_SRC nim
proc write_args2(a: varargs[string, `$`]) =
  for elt in a:
    write(stdout, elt)
  write(stdout, "\n")

write_args2(1, 2, 3.0, "hello", 4)
#+END_SRC

** Slices
Substrings are created by using Slice objects. Some functions require this type of objects as parameters.

Slices are created specifying subranges which it means that it must define a starting and ending index. There are various ways to define indexs: using zero-based indexs, using the ~^~ index (form the last to zero) and using the excluded ~<~ symbol. For example, the following sentences means the same:

: assert b[0..^1] == b[0..b.len-1] == b[0..<b.len]

** Objects
:PROPERTIES:
:header-args: :tangle out/objects.nim
:END:

This type packs different types of value in a single structure. Each object has a constructor.

#+BEGIN_SRC nim
type
  Person = object
    name: string
    age: int

var person1 = Person(name: "Peter", age: 30)
#+END_SRC

The assignment copies an object. The following example copies the object and modifies a value. Then print both of them to demostrate they are different memory spaces.

#+BEGIN_SRC nim
var person2 = person1
person2.age += 14

echo person1.age # prints 30
echo person2.age # prints 44
#+END_SRC

The object visibility can be declared on the type and/or on the field. This affects if the object and its fields are reachable from outside the module.

#+BEGIN_SRC nim
type
  Person2* = object
    name*: string
    age*: int
#+END_SRC

** Tuples
:PROPERTIES:
:header-args: :tangle out/tuples.nim
:END:

Tuples are simmilar to objects. However, the equivalence between them are structurally typed. Comparison is done by checking for the same type, name, order and contents (but does not check the name of the tuple).

Declaring a tuple can be placed at the ~type~ section or inline. There are three possible syntax: expanded syntax, inline tuple type and with anonymous fields.

#+BEGIN_SRC nim
type
  # Expanded
  Person = tuple
    name: string
    age: int

  # Inline
  Person2 = tuple[name: string, age: int]

  # Anonymous fields
  Person3 = (string, int)
#+END_SRC

It is possible to declare a tuple when creating a variable.

#+BEGIN_SRC nim
var perA: tuple[name: string, age: int]
#+END_SRC

The following code use the types declared.

#+BEGIN_SRC nim
var
  per1: Person
  per2: Person2
  per3: Person3
#+END_SRC

Constructors is written with ~()~ operands. Fields can be used explicity. If it the tuple has got anonymous fields, then it con be created directly with the values.

#+BEGIN_SRC nim
per1 = (name: "Peter", age: 30)
per2 = (name: "John", age: 44)
per3 = ("Peter", 30) # anonymous fields
#+END_SRC

Assignment between tuples is achieed by means of the structured copy. It must be done with equivalent-structured tuples (same fields and order).
Tuples with anonymous fields are compatible with declared fields if they have got the same structure.

#+BEGIN_SRC nim
per1 = per2
per2 = per3
#+END_SRC

Accessors are fieldnames or indexes. Indexes can be used with declared fields or with anonymous fields. 

#+BEGIN_SRC nim
echo per1.name
echo per1.age

echo per1[0]
echo per2[1]

echo per3[0]
echo per3[1]
#+END_SRC

Tuples' fields are always public.

*** Return values
The return values can be used with tuples. They can be unpacked and assigned to different values, but the ~()~ syntax must be used.

#+BEGIN_SRC nim
proc test(): tuple[a: int, b: int] =
  return (1, 2)

var
  a: int
  b: int

(a, b) = test() # This will unpack
echo a
echo b
#+END_SRC



** TODO References and Pointer

** Procedural Type
:PROPERTIES:
:header-args: :tangle out/proc_type.nim
:END:

Procedures can be passed as arguments. This needs to a pointer to a procedure with the signature defined. 

The given example defines a ~forEach~ procedures whose parameter is another procedure. The ~action~ parameter is a procedure type.

#+BEGIN_SRC nim
proc echoItem(x : int) = 
  echo x

proc forEach(action : proc (x: int)) =
  const
    data = [2, 3, 4, 5]
  for d in data:
    action(d)

forEach(echoItem)
#+END_SRC

** TODO Distinct Type

* Objects declarations
This section explains memory objects for storing values. 

** Variables

~var~ statements declares local or global variables. Indentation can be used to declare multiple variables.

#+BEGIN_SRC nim
var z: int
var
  x, y: int
  a, b, c: string
#+END_SRC

The ~let~ statement create a single assignment variable. Once a let variable is assigned, it cannot change its value again. Thus, the following code will generate a compilation error.

#+BEGIN_SRC nim
let x = 1
x = 2 # Error!
#+END_SRC

** Constants
Constants are declared with ~const~. They only receives a literal value or an expression that results into a literal. 

Constants values cannot change.

#+BEGIN_SRC nim
const
  a = 1
  z = a + 5
#+END_SRC

* Control Flow Statements
Statements that control the flow of execution are explained in this section.

** Conditionals

** Repetitions

* Subprograms
:PROPERTIES:
:header-args: :tangle out/procs.nim
:END:

Nim supports functions called procedures. The ~proc~ is the only statement to create a subprogram.

The ~return~ statement is used for returning a value from the procedure. However, it is optional when the last expression is the one to be returned or the ~result~ variable is used.

The base syntax is as follows. The first syntax is a procedure without a return value.

: proc NAME (PARAMETERS) = BODY

The second one, has got a return value.

: proc NAME (PARAMETERS) : RETURN_TYPE = BODY

#+BEGIN_SRC nim
  proc yes(s: string) : bool =
    echo s
    let yesno = readline(stdin)
    yesno == "yes"

  # yes("type yes/no") # Compile Error!
  echo yes("1: type yes/no, then the result will be printed")
  discard yes("type yes/no, the result will be discarded")

  proc no(s: string) =
    echo s
    var yesno = readline(stdin)
    while yesno != "no":
      yesno = readline(stdin)

  no("type no to exit")
#+END_SRC

** Parameters
The parameters of the functions are immutable in the procedure body. ~var~ must be used to create input/output parameters (parameters by reference).

*** Named arguments
Parameters names can be used for named arguments. 

Consider the last example. The ~s~ parameter can be setted using the following sentence.

: discard yes(s="type yes/no")

*** Default values
Parameters can have default values. The "yes" procedure from the example above could have a default value using the following declaration.

: proc yes(s = "type yes/no") : bool =

** The return value
Procedures always return values and they cannot be discarded without a sentence.

There is a ~result~ variable implicit on the function. This variable holds the returned value. If ~return~ is used without parameters is the same as ~return result~

*** Discard sentence
:PROPERTIES:
:header-args: :tangle out/procs_discard.nim
:END:

The first approach is using the ~discard~ sentence:

#+BEGIN_SRC nim
  proc yes(s: string) : bool =
    echo s
    let yesno = readline(stdin)
    yesno == "yes"

  # yes("type yes/no") # Compile Error!
  echo yes("1: type yes/no, then the result will be printed")
  discard yes("type yes/no, the result will be discarded")
#+END_SRC

*** Discard pragma
:PROPERTIES:
:header-args: :tangle out/procs_discard_pragma.nim
:END:
Another approach is using the ~{.discardable.}~ pragma. 

#+BEGIN_SRC nim
  proc yes(s: string) : bool {.discardable.} =
    echo s
    let yesno = readline(stdin)
    yesno == "yes"

  yes("type yes/no") # No Compile Error.
  echo yes("1: type yes/no, then the result will be printed")
  discard yes("type yes/no, the result will be discarded")
#+END_SRC

** Overloaded procedures
Nim supports overloaded procedures. Consider the following declaration.

: proc toString(x : int) : string = 

It can be overloaded with the following one.

: proc toString(x : bool) : string =

** Operators
Nim supports the overload of operators. It is posible to define a symbol as a procedure using backticks.

: proc `$` (x : bool) : string = 

Also, the ~infix notation (a + b)~ and ~prefix notation (+ a)~ can be used. Postfix notation is not supported.

** Forward declaration
Forward declaration of procedures are supported. Simply declare the procedure before the body definition by using its header.


#+BEGIN_SRC nim :tangle no
proc even(n : int) : bool # forward declaration

proc odd(n : int) : bool = 
  assert(n >= 0) 
  if n == 0: false
  else:
    n == 1 or even(n-1) # without the forward declaration would be a compile error here.

proc even(n : int) : bool = 
  assert(n >= 0)
  if (n == 1) : false
  else: 
    n == 0 or odd(n-1) # odd is already declared.
#+END_SRC

* Iterators
:PROPERTIES:
:header-args: :tangle out/iterators.nim
:END:

Iterators are special procedures. They have got several important differences:

- They can only be called from ~for~ loops.
- They cannot contain a ~return~ statement.
- They haven't got the implicit ~result~ variable.
- They don't support recursion.
- They cannot be forward declared.

In the following example, the iteratior will increment from the a value to b. The ~yield~ sentence can be imagined as a substitution for the ~for~ body. Lastly, the ~for-in~ sentence uses the iterator for counting from 1 to 10.

#+BEGIN_SRC nim
  iterator countup(a, b: int): int =
    var res = a
    while res <= b:
      yield res
      inc(res)

  echo "counting to ten:"
  for i in countup(1, 10):
    echo i
#+END_SRC

* Modules
:PROPERTIES:
:header-args: :tangle out/my_module.nim
:END:

Modules in Nim are stored in their own file. They provide information hiding and separate compilation. 

Top-level symbols are exported using an asterisk.

In the following example, the ~my_module~ module is defined. Two elements are exported: ~x~ and ~*~.

#+BEGIN_SRC nim
# File: my_module.nim
# Module my_module

var
  x*, y: int

proc `*` *(a, b: seq[int]) : seq[int] = 
  # allocate a new sequence:
  newSeq(result, len(a))
  # multiply two int sequences:
  for i in 0..len(a)-1: result[i] = a[i] * b[i]

when isMainModule:
  # test the new ``*`` operator for sequences:
  assert(@[1, 2, 3] * @[1, 2, 3] == @[1, 4, 9])
#+END_SRC

** Main module
All modules has an implicit constant ~isMainModule~. Its value is true when the module is compiled as the main file. 

This is useful to embed tests within the modules.

** Importing modules
The ~import~ statement brings all the exported elements to the top-level.

The ~import ... except ...~ imports all elements expcept the enumerated symbols. 

Another sentence is ~from ... import ...~ which is a selective import.

** Including a file
To include a file into another, the ~include~ statement should be used. The difference between import and include is that it looses the abstraction like copying and pasting the code.

* Meta     :noexport:

  # ----------------------------------------------------------------------
  #+TITLE:  Nim
  #+AUTHOR: Christian Gimenez
  #+DATE:   25 Aug 2019
  #+EMAIL:
  #+DESCRIPTION: 
  #+KEYWORDS: 

  #+STARTUP: inlineimages hidestars content hideblocks entitiespretty indent fninline latexpreview
  #+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)
  #+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
  #+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc tex:imagemagick
  #+LINK_UP:   
  #+LINK_HOME: 
  #+XSLT:

  # -- HTML Export
  #+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
  #+EXPORT_SELECT_TAGS: export
  #+EXPORT_EXCLUDE_TAGS: noexport
  #+HTML_LINK_UP: index.html
  #+HTML_LINK_HOME: index.html

  # -- For ox-twbs or HTML Export
  #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
  #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
  #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>
  #+LANGUAGE: en

  # Local Variables:
  # org-hide-emphasis-markers: t
  # org-use-sub-superscripts: "{}"
  # fill-column: 80
  # visual-line-fringe-indicators: t
  # ispell-local-dictionary: "british"
  # End:
