#[
   Copyright 2017 Giménez, Christian
   
   Author: Giménez, Christian   

   qtquick.nim
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
]#

{.link: "/usr/lib/libQtGui.so".}

var 
  app:QGuiApplication 
  ## `app` 
  engine:QQmlApplication

app = QGuiApplication(argc, argv)
engine = QQmlApplicationEngine()

engine.load(QUrl(QStringLineral("main.qml")))
app.exec()


#[
int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
]#
