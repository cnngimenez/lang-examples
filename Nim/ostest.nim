#[
   Copyright 2017 Giménez, Christian
   
   Author: Giménez, Christian   

   os.nim
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
]#

from os import getAppFilename, paramStr, paramCount # , commandLineParams
# from parseopt import paramStr

proc main =
  echo getAppFilename()

  var amount:int = paramCount() ## \
    ## `amount` Amount of parameters

  for i in 0..amount:
    echo paramStr(i)


main()
