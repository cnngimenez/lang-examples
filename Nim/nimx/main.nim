#[
   Copyright 2017 Giménez, Christian
   
   Author: Giménez, Christian   

   main.nim
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
]#


import nimx.window
import nimx.text_field
import nimx.system_logger # Required because of Nim bug (#4433)

proc startApp() =
    # First create a window. Window is the root of view hierarchy.
    var wnd = newWindow(newRect(40, 40, 800, 600))

    # Create a static text field and add it to view hierarchy
    let label = newLabel(newRect(20, 20, 150, 20))
    label.text = "Hello, world!"
    wnd.addSubview(label)

# Run the app
runApplication:
    startApp()
