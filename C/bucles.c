/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * bucles.c
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>

// referencia: i++ significa i=i+1

int main(){
	int i;
    printf("Bucle for\n");
	for(i=0;i<10;i++){	//bucle for, se repite lo que contiene hasta que se cumpla la condicion del medio. La estructura es for(asignacion de la variable auxiliar;condicion de corte;modificacion de la variable al final del ciclo)
		printf("%i\n",i);
		//i se incrementa sola, como especificamos al comienzo del bucle
	}//en este punto i=10
    printf("Bucle while\n");
	while(i<20){
		printf("%i\n",i);
		i++;	//hay que incrementarlo, no se hace solo
	}
	//en este punto i=20
	printf("Bucle do while\n");
	do{
        printf("%i\n",i);
		i++;
	}while(i<30);
}
