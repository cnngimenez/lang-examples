/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * funciones.c
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>

float funcion1();	//declaracion de la funcion1. Devuelve un numero de punto flotante (float) y no recibe parámetros

void funcion2(int a, float b){	//declaracion y definicion de una funcion todo junto. No devuelve ningun parametro y recibe dos, un entero y un numero de punto flotante (en ese orden)
	float res=3*a+b;
	printf("funcion 2: %f\n",res);
}

int main(){	//funcion principal. es la que se ejecuta cuando arranca el programa
	funcion2(2,6.1);	//llamada a funcion2, con parametros 2 y 6.1, en ese orden
	printf("pi = %f\n",funcion1());	//llamada a una funcion dentro de un flujo (stream). El resultado se pasa al objeto cout
}

float funcion1(){	//definicion de la funcion1, declarada mas arriba
	float pi=3.1415926;
	return pi;		//devuelve un valor mediante la llamada return
}
