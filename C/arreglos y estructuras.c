/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * arreglos y estructuras.c
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//estructura Contacto. Adentro contiene dos campos: nombre (de tipo puntero a char) y numero (de tipo int)
struct Contacto{
	char *nombre;
    int numero;
};

int main(){
	struct Contacto agenda[4];  //instanciamos un arrelgo de 4 estructuras Contacto (del 0 al 3)
	char *aux = (char*)malloc(sizeof(char)*20);     //reservamos memoria para un string auxiliar de largo máximo de 20 caracteres
	printf("Ingrese el nombre del nuevo contacto: ");
	scanf("%s",aux);	//lo ingresado por teclado se almacena en la cadena auxiliar
	agenda[0].nombre = (char*)malloc(sizeof(char)*strlen(aux)); //reservamos memoria justa para el nombre ingresado (se obtiene la cantidad necesaria utilizando la funcion strlen, que devuelve la cantidad de caracteres usados)
	strcpy(agenda[0].nombre,aux);   //ahora si se asigna el nombre ingresado al campo del contacto
	printf("Ingrese el numero de telefono del contacto: ");
	scanf("%s",aux);
	agenda[0].numero = atoi(aux);
	printf("Nuevo contacto agregado: \n%s: %i\n",agenda[0].nombre,agenda[0].numero);
	agenda[1].nombre = "Pepito";
	agenda[1].numero = 156282822;	//contacto agregado, no ingresado por teclado;
	agenda[2].nombre = "Oreo";
	agenda[2].numero = 156193875;
	agenda[3].nombre = "Opera";
	agenda[3].numero = 156987542;
	
	int i;
	printf("\nAgenda completa:\n");
	for(i=0;i<4;i++){
        printf("Contacto %i: \n",i+1);
        printf("%s: %i\n",agenda[i].nombre,agenda[i].numero);
	}
	
}
