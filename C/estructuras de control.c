/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * estructuras de control.c
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>

//en C los bloques de código se delimitan con llaves {}

int main(){
	int a;
    printf("Ingrese un numero del 1 al 3:\n");
	scanf("%i",&a);
    printf("usando if y else:\n");
	if(a==1){
        printf("a vale uno\n");
	}else{
		if(a==2){
			printf("a vale dos\n");
		}else{
			if(a==3){
				printf("a vale tres\n");
			}else{
				printf("a tiene otro valor\n");
			}
		}
	}
    printf("usando switch y case:\n");
	switch(a){
		case 1:
			printf("a vale uno\n");
			break;
		case 2:
			printf("a vale dos\n");
			break;
		case 3:
			printf("a vale tres\n");
			break;
		default:
			printf("a tiene otro valor\n");
			break;
	}
}
