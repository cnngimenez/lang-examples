/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * punteros.c
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>

//un puntero es una variable que contiene una dirección de memoria. Se utiliza para manejar indirectamente a otras variables.
//Tiene asociado un tipo, por lo que solo puede apuntar a variables de ese tipo

int main(){
	int a;	//variable entera a
	int *p;	//puntero a entero p. Solo puede apuntar a enteros
	a=3;
	printf("a = %i\n",a);
	p=&a;	//p apunta a la direccion de a (& es el operador de dirección)
	printf("p apunta a la direccion %p y contiene: %i\n",p,*p);	//la expresion *p se interpreta como "lo que contiene el espacio al que apunta p"
	*p=5;	//como p apunta a la direccion de a, modifico el valor de a utilizando el puntero p (* se llama operador de indirección)
	printf("a = %i\n",a);
	return 0;
}
