/* 

   Copyright 2015 Giménez, Christian
   
   Author: Giménez, Christian   

   fifo.c
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/stat.h> // para mkfifo
#include <stdio.h> // para printf
#include <fcntl.h> // para open, write y funciones I/O de bajo nivel
#include <unistd.h> // para close()
#include <string.h> // para manejo de strings: strlen()

int main(char* argv[], int argc){
  if (! mkfifo("myfifo.pipe", S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH)) {
    printf("Fifo couldn't be opened.");
  }

  int fd;
  const char* msg = "HOLA MUNDOOOOO";
  
  fd = open("myfifo.pipe", O_WRONLY);
  write(fd, msg, strlen(msg) + 1);  
  close(fd);
  
  return 0;
}
