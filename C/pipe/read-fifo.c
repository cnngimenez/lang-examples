/* 

   Copyright 2015 Giménez, Christian
   
   Author: Giménez, Christian   

   read-fifo.c
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
  Para implementar el lector de un FIFO sólo hace falta usar los comandos típicos de acceso a archivos.

  Incluso, usando cat en la terminal sobre el pipe funciona.
 */

// #include <sys/stat.h> // Para mkfifo... aunque acá no es necesario.
#include <stdio.h> 
#include <fcntl.h> // para low-level I/O: open(), write()...
#include <unistd.h> // para el close().
#include <string.h> // para memset()

int main(char *argv[], int argc){
  int fd;

  /*
    Abrimos el pipe como si fuera un archivo convencional (en solo lectura).
    Se puede usar O_NONBLOCK como flag para evitar el bloqueo del read.
  */
  fd = open("myfifo.pipe", O_RDONLY);

  char buff[100];
  memset(buff, 0 , sizeof(buff)); // Iniciamos el buffer en 0.
  
  // Sé que no va a llevar más de 100 bytes, pero en una programación real hay que tener cuidado con esto.
  read(fd, buff, sizeof(buff));
  
  printf("%s", buff);
  
  close(fd);
  unlink("mififo.pipe"); // Desvinculamos el pipe (¿esto no lo haría mejor el server?)...
  
  return 0;
}
