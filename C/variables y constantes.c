/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * variables y constantes.c
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//inclusi�n de librer�as
#include <stdio.h>
#include <stdlib.h>

//declaraci�n de variables globales. Son globales porque se declaran fuera de todo segmento de c�digo
//Sintaxis: <tipo> nombre = valor; Se puede no asignar el valor en la declaraci�n
int entero = 5;
float decimal = 3.6;
char letra;

//declaraci�n de una constante
//const <tipo> nombre = valor;
const float pi = 3.1416;

//definici�n de una macro (muy similar a una constante)
#define e 2.718281

//funci�n principal. Se ejecuta al iniciar el programa. Abarca lo encerrado por llaves
int main(){
    int i;          //variable local (solo existe dentro de la funci�n main), sin asignar un valor todav�a
    i=3;            //asignaci�n de un valor a i
    
    int array[3];   //vector (arreglo o array) de enteros de longitud 3 (va desde 0 hasta 2) de nombre array
    array[1]=5;     //asigno el valor 5 al segundo elemento de array

    float matrix[2][3]; //matriz (o array bidimensional) de float
    matrix[0][2]=5.81;  //asigno el valor 5.81 al �ltimo elemento de la primera fila de matrix
    
    return 0;
}
