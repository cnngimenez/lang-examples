/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * bucles y arreglos.c
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define max 10

int main(){
    srand(time(NULL));      //semilla del random
    int matrix[max][max];
    for(int i=0;i<max-1;i++){   //se puede declarar una variable auxiliar dentro del for. No se puede acceder desde fuera
        for(int j=0;j<max-1;j++){
            matrix[i][j]=rand()%90 +10; //se genera un número (pseudo)aleatorio entre 10 y 99
        }
    }
    //en los bucles for anteriores se recorre a la matriz fila por fila y dentro de cada fila columna por columna, siendo i el índice de las filas y j el de las columnas
    
    for(int i=0;i<max-1;i++){
        for(int j=0;j<max-1;j++){
            printf("%i ",matrix[i][j]);
        }
        printf("\n");
    }
    
    return 0;
}
