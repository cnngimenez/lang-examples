;;; forms.el --- 

;; Copyright 2017 Giménez, Christian
;;
;; Author: Giménez, Christian
;; Version: $Id: forms.el,v 0.0 2017/07/10 12:56:32 christian Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'forms)

;;; Code:

(provide 'forms)
(eval-when-compile
  (require 'cl))


(defun form-create ()
  (interactive)

  (define-button-type 'faced
    'face 'custom-button)
  (with-current-buffer (get-buffer-create "form test")
    (delete-region (point-min) (point-max))
    (insert-button "Button with overlay")
    (insert "\n")
    (insert-text-button "Button using text props")
    (insert "\n")
    
    (insert-text-button "Typed" :type 'faced)

    (switch-to-buffer (current-buffer))
    )		     
  )

;; (defun form-xwidgets ()
;;   (interactive)
;;   (with-current-buffer (get-buffer-create "form test")
;;     (delete-region (point-min) (point-max))

;;     (make-xwidget 'webkit "Webkit" 100 150 nil (current-buffer))
    
;;     (switch-to-buffer (current-buffer))
;;     )
;;   )


;;; forms.el ends here
