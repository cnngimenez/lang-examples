;;; structs.el --- 

;; Copyright 2016 Giménez, Christian
;;
;; Author: Giménez, Christian
;; Version: $Id: structs.el,v 0.0 2016/12/27 00:38:58 christian Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'structs)

;;; Code:

(provide 'structs)
(eval-when-compile
  (require 'cl))


(defstruct persona nombre )




;;; structs.el ends here
