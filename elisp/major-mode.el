;;; major-mode.el --- 

;; Copyright 2015 Giménez, Christian
;;
;; Author: Giménez, Christian
;; Version: $Id: fa-mode.el,v 0.0 2015/08/30 06:46:17 christian Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'fa-mode)

;;; Code:

(provide 'fa-mode)

					; --------------------------------------------------
					; Customization Options

(defgroup fa-mode nil
;;; fa-mode.el ends here
  "Finding-Ada mode"
  :tag "Finding Ada Mode"
  :version "Emacs 24.5.1"
  )

(defcustom fa-indent-space 4
  "Amount of indentation spaces."
  :group 'fa-mode
  )

					; --------------------------------------------------
					; Keymap

(defvar fa-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "\r" 'fa-enter-key)
    (define-key map "q" 'fa-mode-exit)
    map)
  "Keymap for `fa-mode`.")

					; --------------------------------------------------
					; Font-lock

(defvar fa-variable-keywords "^\\([^:]*\\)[[:space:]]*:[[:space:]]*\\([[:digit:]]+\\)[[:space:]]*:[[:space:]]*\\([^:]*\\)[[:space:]]*")
(defvar fa-identifier-keywords "^\\([^:]*\\)[[:space:]]*:[[:space:]]*\\([[:digit:]]+\\)[[:space:]]*:[[:space:]]*\\([^:]*\\)[[:space:]]*")

(defface fa-variable-face
  '((t :foreground "chocolate"))
  "Caras de Qriollo Keywords"
  )

(defface fa-identifier-face
  '((t :foreground "chocolate"))
  "Caras de Qriollo Keywords"
  )


(defvar fa-font-locks-keywords
  (list (list qriollo-marron-keywords 2 ''qriollo-marron-keywords-face)
	(list qriollo-verde-keywords 2 ''qriollo-verde-keywords-face)
	(list qriollo-violeta-keywords 2 ''qriollo-violeta-keywords-face)
	(list qriollo-gringeadas-keywords 2 ''qriollo-gringeadas-keywords-face)
	(list "^OJO.[[:space:]]+.*$" 0 ''qriollo-comment-keywords-face)
	(list "\\(^\\|[[:space:]]+\\)\\([[:digit:]]+#?\\)\\($\\|[[:space:]]+\\)"  2 ''qriollo-number-keywords-face)
	)
  "Font-lock keywords for `fa-mode'"
  )

					; --------------------------------------------------
					; Main function

(define-derived-mode fa-mode nil "Finding-Ada"
  "Major Mode for Finding Ada Queries"
  ;; Font-locks
  
  ;; *Supossedly*, in derived-modes it is not required to tell which font-lock variable
  ;; you're going to use, it is automatically setted. But, sometimes is good to tell
  ;; so if you apply the mode twice the font-lock is refreshed.
  
  ;; (set (make-local-variable 'font-lock-defaults) '(qriollo-font-locks-keywords))
  (kill-local-variable 'font-lock-set-defaults)
  (set (make-local-variable 'font-lock-defaults) (list 'fa-font-locks-keywords))
  (font-lock-mode-internal nil)
  (font-lock-mode-internal t)

  ;; Keymaps is automatically applied to the fa-mode-map function
  ;; (MODENAME-map function, where in this case MODENAME = fa-mode).

  ;; Another option for font-lock?
  ;; (setq-local font-lock-defaults '(fa-font-lock-keywords nil t))
  
  ;; (setq-local buffer-read-only t) ;; Read only buffer


  ;; Indentation
  (set (make-local-variable 'indent-line-function) 'fa-indent)
  ;; (set (make-local-variable 'indent-region-function) 'qriollo-indent-region)
  
  )

					; --------------------------------------------------
					; Indentation

(defun fa-indent ()
  "There are ways of writing this function for auto-indenting when tabbing, but the problem is that you finally have to write an entire parser rules for making this works. 

It is better if you implement this in your way by scratch."
  (interactive)
  (message "Indenting...")
  )

					; --------------------------------------------------
					; Keys functions

(defun fa-enter-key ()
  "What to do when the enter key is pressed."
  (interactive)
  (message "Hi world")
  )

(defun fa-mode-exit ()
  "Kill the fa-mode buffer."
  (interactive)
  (let ((curbuff (current-buffer))
	)
    (delete-window (get-buffer-window curbuff))
    (kill-buffer curbuff)
    )
  )
