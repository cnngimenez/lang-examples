;;; subprocess-async.el --- Ejemplo de Procesos Asíncronos

;; Copyright 2015 Giménez, Christian
;;
;; Author: Giménez, Christian
;; Version: $Id: subprocess-async.el,v 0.0 2015/07/14 17:38:14 christian Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'subprocess-async)

;;; Code:

(provide 'subprocess-async)
(eval-when-compile
  (require 'cl))

;; Un sentinela debe tener dos parámetros, el proceso que ejecuta el sentinela, y un evento.
;; Sólo puede usarse un sentinela por proceso, aunque podemos expandir esto usando funciones Advise.
(defun sentinela (proceso evento)
  "Este sentinela se ejecuta cuando le pasa algo al proceso, por ejemplo, cuando se cierra el proceso."
  (with-current-buffer (get-buffer-create "process-output")
    (goto-char (point-max))
    (insert "Proceso hizo algo...")
    (display-buffer (current-buffer))
    )
  )

;; Un filtro se ejecuta cuando el proceso recibe una entrada.
;; El filtro por defecto escribe la salida del proceso a su buffer asignado. Sin embargo, se puede utilizar un filtro propio.
(defun filtro (proceso string)
  (with-current-buffer (get-buffer-create "process-output")
    (goto-char (point-max))
    (insert "\nSalida: " string)
    (display-buffer (current-buffer))
    )
  )

(defun iniciar-subproceso ()
  (interactive)

  ;; Se ejecuta "cat" sin argumentos.
  ;; La salida del proceso estará en el buffer "mi-proc-buffer".
  (start-process "mi-proceso" "mi-proc-buffer" "cat")
   
  (set-process-sentinel (get-process "mi-proceso") 'sentinela)
  (set-process-filter   (get-process "mi-proceso") 'filtro)

  (process-send-string "mi-proceso" "hola mundo\n")
  (process-send-string "mi-proceso" "segundo string...\n")
  )



;;; subprocess-async.el ends here
