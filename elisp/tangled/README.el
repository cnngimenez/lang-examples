(defun my-al-cuadrado (x)
  "Calcula X al cuadrado."
  (* x x))

(my-al-cuadrado 5)

(defun mult-cuadrado (x &optional y)
  (if y
      (* x y)
    (* x 5)))

(list (mult-cuadrado 10)
      (mult-cuadrado 10 10))

(defun my-leer-nombre (nombre apellido)
  "Lee el NOMBRE desde el minibufer y dice hola."
  (interactive "M¿Cuál es tu nombre?\nM¿Cuál es tu apellido?")
  (message "Hola %s, %s.\n ¿cómo te va?" apellido nombre))

(defun my-insertar-nombre (nombre apellido)
  "Lee el NOMBRE desde el minibufer y dice hola."
  (interactive "M¿Cuál es tu nombre?\nM¿Cuál es tu apellido?")
  (insert (format "Hola %s, %s.\n ¿cómo te va?" apellido nombre)))

(message "hola mundo")

(message "Hacen %s grados de temperatura"
         (my-al-cuadrado 5))

(switch-to-buffer-other-window "*Messages*")

(setq my-age (read-number "Enter age:" 25))

(ivy-read "Test: " '("can do" "can't, sorry" "other"))

(completing-read "Your age is between?"
                 '("0-20" "21-50" "Older than 50"))

;; Se ejecuta "cat" sin argumentos.
;; La salida del proceso estará en el buffer "mi-proc-buffer".
(start-process "my-process" "mi-proc-buffer" "cat")

(process-send-string "my-process" "hola mundo\n")
(process-send-string "my-process" "segundo string...\n")

;; Un sentinela debe tener dos parámetros, el proceso que ejecuta el sentinela, y un evento.
;; Sólo puede usarse un sentinela por proceso, aunque podemos expandir esto usando funciones Advise.
(defun sentinela (proceso evento)
  "Este sentinela se ejecuta cuando le pasa algo al proceso, por ejemplo, cuando se cierra el proceso."
  (with-current-buffer (get-buffer-create "process-output")
    (goto-char (point-max))
    (insert "Proceso hizo algo...")
    (display-buffer (current-buffer))
    )
  )

(set-process-sentinel (get-process "my-process") 'sentinela)

;; Un filtro se ejecuta cuando el proceso recibe una entrada.
;; El filtro por defecto escribe la salida del proceso a su buffer asignado. Sin embargo, se puede utilizar un filtro propio.
(defun filtro (proceso string)
  (with-current-buffer (get-buffer-create "process-output")
    (goto-char (point-max))
    (insert "\nSalida: " string)
    (display-buffer (current-buffer))
    )
  )

(set-process-filter (get-process "my-process") 'filtro)

(define-short-documentation-group org-element
  "Parsing functions: Org -> AST"
  (org-element-at-point)
  (org-element-parse-buffer)
  (org-element-context
   :no-eval (org-element-context)
   :result (headline (:raw-value "org-element API"  ... :title "org-element API")))
  "Accessors"
  (org-element-type
   :no-eval (org-element-type parsed-headline-context)
   :result 'headline)
  (org-element-property
   :no-eval (org-element-property :title parsed-paragraph)
   :result-string "org-element API")
  (org-element-contents)
  (org-element-map)
  "Setters"
  (org-element-put-property)
  (org-element-extract-element)
  (org-element-set-element)
  (org-element-insert-before)
  (org-element-adopt-element)
  "AST -> Org"
  (org-element-interpret-data)
  "Examine genealogy"
  (org-element-lineage)
  "Mode info:
🔗 https://orgmode.org/worg/dev/org-element-api.html")

(define-short-documentation-group org
  "Iteration functions"
  (org-map-entries)
  "Enries, headlines, and their properties."
  "Property getters"
  (org-element-property)
  (org-entry-get)
  (org-entry-properties)
  "Property positions"
  (org-entry-beginning-position)
  (org-entry-end-position)
  "Property setters"
  (org-entry-put)
  (org-entry-delete)
  "Inserting raw text"
  (insert))

# Local variables:
# eval: (progn (goto-char (point-min)) (insert (current-time-string)))
# End:

# Local variables:
# eval: (add-hook 'before-save-hook (lambda () (goto-char (point-min)) (insert (current-time-string))) nil t)
# End:

# Local variables:
# org-export-initial-scope: buffer
# eval: (add-hook 'org-export-before-processing-hook (lambda (be) (progn (org-babel-ref-resolve "export-setup") (org-babel-ref-resolve "python-matrix-setup"))) nil t)
# End:

(org-map-entries
 (lambda ()
   (let ((filename (org-entry-get nil "export_file_name")))
     (if (and filename (not (string-blank-p filename)))
         (unless (string-match "-solved$" filename)
           (org-entry-put nil "export_file_name" (concat filename "-solved"))))))
 "solved"
 'file)
(org-map-entries
 (lambda ()
   (let ((filename (org-entry-get nil "export_file_name")))
     (if filename
         (org-entry-put nil "export_file_name" (string-remove-suffix "-solved" filename)))))
 "-solved|unsolved"
 'file)

# Local variables:
# eval: (org-babel-ref-resolve "process-export-filenames")
# eval: (add-hook 'before-save-hook (lambda () (org-babel-ref-resolve "process-export-filenames")) nil t)
# End:

(defun my-add-property ()
  "Add a property to the current title"
  (org-entry-put nil "my-property"
                 (format "The title is: \"%s\""
                         (org-element-property
                          :raw-value
                          (org-element-headline-parser (org-entry-end-position))))))
(org-map-entries #'my-add-property "hello" 'file)

'((((class color) (min-colors 88) (background light))
   :background "darkseagreen2")
  (((class color) (min-colors 88) (background dark))
   :background "darkolivegreen")
  (((class color) (min-colors 16) (background light))
   :background "darkseagreen2")
  (((class color) (min-colors 16) (background dark))
   :background "darkolivegreen")
  (((class color) (min-colors 8))
   :background "green" :foreground "black")
  (t :inverse-video t))

(defun my-show-image (image-path)
  (interactive "fImage path:")
  (with-current-buffer (get-buffer-create "Image")
    (erase-buffer)
    (insert "This is the image:")
    (insert-image (create-image image-path)
                  "The image you selected.")
    (insert "So, that's the image!")
    (switch-to-buffer (current-buffer))))

(defun my-set-widget-appearance ()
  ;; Face when idle
  (setq-local widget-button-face custom-button)
  ;; Face when pressed
  (setq-local widget-button-pressed-face custom-button-pressed)
  ;; Face when mouse over
  (setq-local widget-mouse-face custom-button-mouse)
  ;; Moves the point to a botton when clicking
  (setq-local widget-button-click-moves-point t)
  ;; Removes brackets
  (setq-local widget-push-button-prefix "")
  (setq-local widget-push-button-suffix ""))

(defface my-custom-button
  '((((type x w32 ns haiku pgtk) (class color))
     :box (:line-width 2 :style released-button)
     :background "lightgrey" :foreground "black"))
"My own custom button simmilar to the default one.")

(setq my-custom-button 'my-custom-button)

(defun my-set-my-widget-appearance ()
  (setq-local widget-button-face my-custom-button))

(with-current-buffer (get-buffer-create "widget-test")
  ;; Reset the buffer
  (kill-all-local-variables)
  (let ((inhibit-read-only t))
    (erase-buffer))
  (remove-overlays)

  ;; Button without appearance
  (widget-create 'push-button
                 :notify (lambda (&rest _ignore)
                           (message "Widget push-button says: \"Hello!\""))
                 "Say hello!")    

  ;; Button with apparance
  (my-set-my-widget-appearance)
  (widget-create 'push-button
                 :notify (lambda (&rest _ignore)
                           (message "Widget push-button says: \"Hello!\""))
                 "Say hello!")       

  ;; Widget setup
  (use-local-map widget-keymap)
  (widget-setup)
  (switch-to-buffer (current-buffer)))
