
-- Copyright 2014 Giménez, Christian
--
-- Author: Giménez, Christian   
--
-- lambda.hs
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

-- Funciones Lambdas son aquellas que no poseen nombres.
-- Muy útiles en lenguajes con funciones de orden superior (que reciben como parámetros o devuelven como resultado una o varias funciones en vez de valores).


-- Su notación es \param1 param2 ... -> forma_de_la_fnc

map (\a -> a + 20) [1,2,3,4] -- [21,22,23,24]

{- Cuando se lee que una función requiere como parámetro otra función, se puede usar una fnc. lambda:

> :t map
map :: (a -> b) -> [a] -> [b]

map requiere de una función que tiene un parámetro de tipo "a" y devuelva un valor de tipo "b", una lista de eltos. de tipo "a" y devuelve una lista de eltos. de tipo "b".
-}


{- foldl utiliza un acumulador "acc" en la función lambda.
El segundo parámetro (el número 0 en este caso) es a partir de qué índice del arreglo va a considerar.
-}
foldl (\acc x -> acc +x) 0 [1,2,3,4] -- 10

zipWith (\a b -> a+b) [1,2,3] [4,5,6] -- [5,7,9]
