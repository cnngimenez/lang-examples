
-- Copyright 2014 Giménez, Christian
--
-- Author: Giménez, Christian   
--
-- funciones.hs
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

-- Declaración de parámetros, esto es opcional.
fnc :: Int -> Int -> Int
-- Declaración de la función:
fnc par1 par2 = par1 + par2

fibo :: Int -> Int        
fibo (n) | n == 0 = 1
         | n == 1 = 1
         | n > 1 = fibo(n-1) + fibo(n-2)
         | otherwise = error "Fibonacci no definido para n < 0"
-- "otherwise" significa 'en cualquier otro caso'.
-- se separa con "|" los distintos casos:
-- Forma: "|" expresión de condición "=" expresión de resultado

signo :: Int -> Int
signo (x) | x > 0 = 1
          | x < 0 = -1
          | x == 0 = 0



-- Cláusula where: asigna un valor temporal a un parámetro z.
f :: Int -> Int -> Int
f x y | x > z = 2
      | x < z = -2
  where z = x + y

-- Uso de pattern matching para los parámetros.
fncb (1, x) = x + 55
fncb (2, _) = 3  -- _ es un comodin, cuando no nos importa lo que tiene ese parámetro.
fncb (x, 3) = 2 + x
-- tener en cuenta que matchea en el orden que aparece: fncb(2, 3) devuelve 3 y no 4.
