
-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian   
--
-- Main.hs
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

import System.IO

-- Importamos mi módulo.
import MiModulo (fnc2) -- importa explícitamente fnc2, fnc1 lo ignora. Si no se escribe (...), entonces se importa todo.

{-
Se puede usar MiModulo.fnc2 para referirse a la función, o directamente fnc2.
MiModulo.fnc2 es mejor pues si hay conflictos con el nombre de la función (ya está definida por otro módulo), entonces no hay que reescribir nada.
-}

main :: IO ()
main = do
  putStrLn $ fnc2 "juan"
