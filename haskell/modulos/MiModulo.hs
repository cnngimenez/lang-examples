
-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian   
--
-- MiModulo.hs
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

-- Define el módulo MiModulo
-- Exporta explícitamente fnc1, fnc2.
-- Si no hay escrito (...) en module entonces se exportarán todas las funciones.

module MiModulo (fnc1, fnc2
                ) where -- where debe estar alineado

-- Funciones públicas 

fnc1 :: Int -> Int
fnc1 x = x + 20

fnc2 :: String -> String
fnc2 s = "hi " ++ s

{- fnc3 es una función privada.

-}
fnc3 :: Int -> String
fnc3 n = show n

