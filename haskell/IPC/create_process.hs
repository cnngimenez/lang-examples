
-- Copyright 2015 Giménez, Christian
--
-- Author: Giménez, Christian   
--
-- create_process.hs
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

import System.Process
import System.IO

type ProcessT = (Maybe Handle, Maybe Handle, Maybe Handle, ProcessHandle) 

getStdout :: ProcessT -> Handle
getStdout (a, Just b, c, d) = b
                              
main :: IO()
main = do
  -- Obtenido desde la documentación de System.Process.createProcess
  cp <- createProcess (proc "ls" []){ std_out = CreatePipe }
  str <- hGetContents ( getStdout cp ) 
  putStrLn str


