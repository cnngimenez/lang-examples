-- Copyright 2014 Giménez, Christian

-- Author: Giménez, Christian   

-- curry.hs

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Curry es la cualidad que tiene el lenguaje de tratar una función de muchos parámetros como
-- una función de orden superior que retorna funciones de un solo parámetro con "evaluaciones parciales".

-- En este caso, suma x y será utilizada como una función Curry sum1 donde x se asociará a 1 dejando a y sin ligar hasta que sea evaluada en el intérprete Hugs.


suma :: Int -> Int -> Int
suma x y = x + y

-- Tipo de sum1 es deducido por suma
-- sum1 :: Int -> Int 
-- Use :type sum1 en Hugs para verificar.
sum1 = suma 1
