
-- Copyright 2014 Giménez, Christian
--
-- Author: Giménez, Christian   
--
-- io.hs
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

-- ¡Estas funciones carecen de transparencia referencial!


-- Se debe importar IO para poder usar bien todas las características.
import System.IO

-- Existen varios tipos de IO type donde type puede ser un tipo de dato o "()" (que significa que la operación no retorna nada y sólo realiza acciones.
-- En el caso de tener IO String, es preferible asignar el valor a algún identificador con "<-" como "hGetContents :: Handle -> IO String"
main :: IO ()

-- IO no son son funciones puras de un lenguaje funcional, puesto que no poseen transparencia referencial (la salida posee efectos colaterales).
-- En estos casos, Haskell separa lo "puramente funcional" de lo "impuro". Por lo que todo lo que se realice dentro de do {...} tiene tipos que no son compatibles con nada que esté afuera de do {...} (IO String, IO Boolean, etc.)
-- Básicamente, se puede generar funciones nuevas para poner dentro de do {...}, pero tarde o temprano, todas deben ser invocadas desde do.
main = do h <- openFile "nose.txt" ReadMode
          d <- hGetContents h
          hPrint stdout d


-- El "<-" permite almacenar el resultado de una función en un parámetro, esto indica que ese parámetro debe estar sólo dentro de un "do".
-- Se puede llamar a una función Haskell pura sin inconvenientes, para ello se puede usar la misma función:
main = do  
    putStrLn "Hello, what's your name?"  
    name <- getLine  
    putStrLn $ "Read this carefully, because this is your future: " ++ tellFortune name
-- O usar "let" para "asignarlo" a un parámetro:
main = do  
    putStrLn "What's your first name?"  
    firstName <- getLine  
    putStrLn "What's your last name?"  
    lastName <- getLine  
    let bigFirstName = map toUpper firstName  
        bigLastName = map toUpper lastName  
    putStrLn $ "hey " ++ bigFirstName ++ " " ++ bigLastName ++ ", how are you?"

-- "let" aquí asigna bigFistName a una función "map toUpper firstName" y bigLastName con "map toUpper lastName"
-- recordar que toUpper reciben String's y no "IO String"'s (¡son funciones puras!). Prestar mucha atención a la indentación que es obligatoria.
-- Mirar: http://learnyouahaskell.com/input-and-output
    
-- "do" presenta la posibilidad de usar una secuencia de acciones:
-- * Una acción
-- * Un patrón ligado a al resultado de una acción con <-
-- * Un conjunto de definiciones locales con let

-- Por cada función usada para la entrada/salida estándar (stdin y stdout) existe una función para un Handle dado, las cuales empiezan con "h": por ejemplo: para getContents está hGetContents.
