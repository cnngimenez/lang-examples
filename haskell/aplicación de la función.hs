
-- Copyright 2014 Giménez, Christian
--
-- Author: Giménez, Christian   
--
-- aplicación de la función.hs
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

-- $ nos sirve para indicar que el lado derecho tiene mayor orden de prescendencia que el lado izquierdo:

sqrt 3+4+9 -- 14.73...

sqrt $ 3+4+9 -- 4.0 (ejecuta primero el lado derecho porque posee mayor orden de prescendencia.

sqrt (3+4+9) -- 4.0 (idem al $... pero el uso de paréntesis no es recomendado)


-- Composición de funciones
-- Para componer funciones se puede usar ".": (negate . abs) == negate (abs x)

map (\x -> negate (abs x)) [-4,-3,-2,-1,0,1,2,3,4] -- [-4,-3,-2,-1,0,-1,-2,-3,-4]
map (negate . abs) [-4,-3,-2,-1,0,1,2,3,4] -- ¡idem! 
