{-# LANGUAGE OverloadedStrings #-}

import Database.MySQL.Simple

hello :: IO Int
hello = do
   conn <- connect defaultConnectInfo {connectPassword="pass", connectDatabase="mydb"}
   [Only i] <- query_ conn "select 2 + 2"
   return i

-- El resultado de query_ puede ser interpretado como una tupla (IO QueryResults).
-- Recoradar que son funciones "impuras" (mirar io.hs).

obtener :: Connection -> IO [(Float, String, String)]
obtener conn = do
  query_ conn "SELECT (a,b,c) FROM mitabla;"

