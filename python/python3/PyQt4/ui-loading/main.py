####! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2015 Giménez, Christian
#
# Author: Giménez, Christian   
#
# main.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from mi_form import *

app = QApplication(sys.argv)
window = MiForm()

window.show()
sys.exit(app.exec_())
