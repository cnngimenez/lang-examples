####! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2015 Giménez, Christian
#
# Author: Giménez, Christian   
#
# mi_form.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt4.QtGui import *
from PyQt4.QtCore import *
from ui_miform import Ui_MiForm

class MiForm(QWidget):
    """Formulario particular.

El archivo ui_miform.py puede generarse por medio de:

    from PyQt4.uic import *
    compileUiDir(".")
"""
    def __init__(self):
        QWidget.__init__(self)

        self.ui = Ui_MiForm()
        self.ui.setupUi(self)

        # Conectar señales.
        self.ui.pbHola.clicked.connect(self.hola)

    def hola(self):
        print("Hooolaaaa")
        
        
        
