# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './miform.ui'
#
# Created: Wed Mar  4 03:04:01 2015
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MiForm(object):
    def setupUi(self, MiForm):
        MiForm.setObjectName(_fromUtf8("MiForm"))
        MiForm.resize(146, 75)
        self.verticalLayout = QtGui.QVBoxLayout(MiForm)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(MiForm)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.pbHola = QtGui.QPushButton(MiForm)
        self.pbHola.setObjectName(_fromUtf8("pbHola"))
        self.verticalLayout.addWidget(self.pbHola)

        self.retranslateUi(MiForm)
        QtCore.QMetaObject.connectSlotsByName(MiForm)

    def retranslateUi(self, MiForm):
        MiForm.setWindowTitle(_translate("MiForm", "Form", None))
        self.label.setText(_translate("MiForm", "Formulario de Prueba", None))
        self.pbHola.setText(_translate("MiForm", "Hooolaaaa", None))

