####! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2015 Giménez, Christian
#
# Author: Giménez, Christian   
#
# hello-world.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
This example was found on https://techbase.kde.org/Development/Tutorials/Python-hello_world
'''

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys
 
def greet():
  print ("hello world")
 
#First we create a QApplication and QPushButton
app=QApplication(sys.argv)
 
win = QWidget()
qgl = QGridLayout(win)
button1=QPushButton(win)
button2=QPushButton(win)
button1.setText("greet")
button2.setText("exit")
qgl.addWidget(button1)
qgl.addWidget(button2)
 
# Connect buttons to actions
QObject.connect(button1,SIGNAL("clicked()"),greet)
QObject.connect(button2,SIGNAL("clicked()"),app.exit)
 
win.show()
#Start the evnt loop
sys.exit(app.exec_())
