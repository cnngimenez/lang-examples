'''
   Copyright 2013 Giménez, Christian
   
   Author: Giménez, Christian   

   debug.py
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from pdb import *

def sise() :
    print "esto es un debug"
    print "¿sí?"
    a = True
    if a:
        print "a es True"
    else:
        print "a es False"
        

def nose() :
    print "hola"

    # Aquí comienza el debugging.
    import pdb; pdb.set_trace()

    print "chau"
    sise()
    print "chau chau"
    
    
'''
También se puede usar así para testear una sola función desde python:

    import pdb
    pdb.run('mymodule.test()')

'''
