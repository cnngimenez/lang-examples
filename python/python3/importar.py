'''
   Copyright 2013 Giménez, Christian
   
   Author: Giménez, Christian   

   importar.py
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
Binds the module name in the local namespace to the module object.
'''
# import sys.*

# Import all sys.* features.
# * can be replaced for every identifier you like.
from sys import *

# This can be used for chaging an imported identifier name in this local (file) scope.
#from sys import identifier as othername

print 'this is maxint'
print maxint


