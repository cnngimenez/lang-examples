# -*- coding: utf-8 -*-
'''
Mouse Example from NCURSES howto.



:copyright: 2016 Giménez, Christian
:author: Giménez, Christian
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# mouse-2.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from curses import *

c = 0
choice = 0

# Initialize curses
std = initscr()
std.clear()
noecho()
cbreak()       # Line buffering disabled. pass on everything

# Try to put the window in the middle of screen
startx = (80 - WIDTH) / 2
starty = (24 - HEIGHT) / 2

attron(A_REVERSE)
std.addstr(23, 1, "Click on Exit to quit (Works best in a virtual console)")
refresh()
attroff(A_REVERSE)

# Print the menu for the first time
menu_win = std.newwin(HEIGHT, WIDTH, starty, startx)
print_menu(menu_win, 1)
# Get all the mouse events
mousemask(ALL_MOUSE_EVENTS, NULL)

while True:
    c = wgetch(menu_win)
    switch(c)
    if KEY_MOUSE:
        gm = getmouse()
        if gm.id == 1:
            # When the user clicks left mouse button
            if (event.bstate & BUTTON1_PRESSED):
                report_choice(event.x + 1, event.y + 1, choice)
                if(choice == -1):    # Exit chosen
                    endwin()
                    exit(0)
                    std.addstr(22, 1,
                               "Choice made is : %d String Chosen is \"%10s\"",
                               choice, choices[choice - 1])
                    refresh()
        print_menu(menu_win, choice)

endwin()
