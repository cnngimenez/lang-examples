# -*- coding: utf-8 -*-
'''




:copyright: 2016 Giménez, Christian
:author: Giménez, Christian
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# tui.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from tui import tui, formats

__version__ = "0.1.0"
if __name__ == '__main__':
    o = tui(progname='MooseCounter', main=__file__)
    o.initprog()
    
