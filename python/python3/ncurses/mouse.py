# -*- coding: utf-8 -*-
'''
Mouse

Mouse event capture.

:copyright: 2016 Giménez, Christian
:author: Giménez, Christian
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# mouse.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import curses
from curses import *

startx = 10
starty = 10
height = 3
width = 20


# stdscr = initscr()
def main(stdscr):
    stdscr.clear()
    noecho()
    cbreak()
    # mousemask(ALL_MOUSE_EVENTS)
    mousemask(BUTTON1_CLICKED)

    stdscr.clear()
    stdscr.subwin(30, 30, 0, 0)
    stdscr.addstr(2, 2, "hi world")
    stdscr.refresh()

    # height, width = stdscr.getmaxyx()
    ending = False
    ch = 0
    while not ending:
        stdscr.clear()
        stdscr.subwin(30, 30, 0, 0)
        stdscr.box()
        stdscr.addstr(2, 2, "hi world")
        stdscr.addstr(1, 1, str(ch))
        stdscr.addstr(3, 1, "KEY_MOUSE = " + str(curses.KEY_MOUSE))
        stdscr.refresh()
        win = newwin(height, width, startx, starty)
        win.box()
        win.addstr(1, 2, "Exit! Click here!")
        win.refresh()

        ch = stdscr.getch()

        if ch == curses.KEY_MOUSE:
            mid, mx, my, mz, button = curses.getmouse()
            # ending = True
            if button & BUTTON1_CLICKED == BUTTON1_CLICKED:
                if (mx > startx and mx < startx + width and my > starty and my < starty + height):
                    ending = True
        elif 0 < ch < 256:
            # Is a key pressed
            ch = chr(ch)
            if ch in 'qQ':
                ending = True

    endwin()
    print(str(ch))

curses.wrapper(main)
