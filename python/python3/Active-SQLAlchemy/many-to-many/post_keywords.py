# -*- coding: utf-8 -*-
'''




:copyright: 2015 Giménez, Christian
:author: Giménez, Christian
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# post_keywords.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from database import db
from sqlalchemy import Table


post_keywords = Table('post_keywords', db.metadata,
                      db.Column('post_id', db.Integer,
                                db.ForeignKey('post.id')),
                      db.Column('tag.id', db.Integer,
                                db.ForeignKey('tag.id')))

# class PostKeywords(db.Model):
#     post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
#     tag_id = db.Column(db.Integer, db.ForeignKey('tag.id'))
