# ! /usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright 2015 Giménez, Christian
#
# Author: Giménez, Christian
#
# ej1.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Pydoc-title here

Pydoc description
'''

from active_sqlalchemy import SQLAlchemy

# db = SQLAlchemy('pymysql+mysql://user:password@host:port/mydatabase')
db = SQLAlchemy('sqlite:///test.sqlite')


class User(db.Model):
    name = db.Column(db.String(25), unique=True, nullable=False)
    location = db.Column(db.String(50), default="USA")
    # many-to-many relationship:
    classroom = db.relationship('Classroom', secondary="registered")
    # last_access = db.Column(db.Datetime)

# Relación de muchos a uno


class Telephone(db.Model):
    number = db.Column(db.String(25), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)


# Relación de muchos a muchos

class Classroom(db.Model):
    name = db.Column(db.String(25), unique=True, nullable=False)
    user = db.relationship(User, secondary="registered")

# Relación mucho-a-muchos entre Classroom y User


class Registered(db.Model):
    classroom_id = db.Column(db.Integer, db.ForeignKey(Classroom.id))
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))


db.create_all()

# Creating a record
# user = User(name="Mardix", location="Moon").save()

# From active_sqlalchemy github page.
