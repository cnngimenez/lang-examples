####! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2015 Giménez, Christian
#
# Author: Giménez, Christian   
#
# querying.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Pydoc-title here

Pydoc description
'''

# Get all records

all = User.all()

# Get a record by id

user = User.get(1234)

# Update record

user = User.get(1234)
if user:
    user.update(location="Neptune")

# Soft Delete a record

user = User.get(1234)
if user:
    user.delete()

# Query Records

users = User.all(User.location.distinct())

for user in users:
    ...

# Query with filter

all = User.all().filter(User.location == "USA")

for user in users:
    ...

