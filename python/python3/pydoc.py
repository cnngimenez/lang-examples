####! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2014 Giménez, Christian
#
# Author: Giménez, Christian   
#
# pydoc.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Un resumen de un par de líneas de lo que hace el módulo.

Un texto explicativo muuuuucho más largo, si es necesario, claro. 
El formato siempre tiende a ser el mismo: 

* una línea como resumen
* una línea en blanco
* un texto explicativo largo

Pydoc espera documentación como primer elemento despues del módulo, clase o función
'''


# también acepta anotaciones en los parámetros y retorno de cada función:
# nombre de tipo string y edad de tipo integer.
#
# Las anotaciones puede ser cualquier expresion y puede ser evaluada al cargar
# la definición de la función.
#
# El retorno de funcin.__annotations__ es un diccionario cuyas claves son los
# nombre de los parámetros ("nombre" y "edad" en este caso).
def funcion(nombre: str, edad:int = 1) -> tuple :
    """Ejemplo de usar las anotaciones.

    Véa funcion.__annotations__, retorna un diccionario:
    Use funcion.__annotations__["nombre"] para ver el primer parámetro.
    

    Para ver esta documentación, use print(funcion.__doc__).
    """
    
    (nombre, edad)


def fnc_b(nombre: "string", edad: "integer" = 1) -> "Retorna algo booleano":
    """También se pueden usar strings para explicaciones más largas.
    """
