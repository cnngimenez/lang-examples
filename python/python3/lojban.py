# -*- coding: utf-8 -*-
'''
Lojban definitions search module.

Searchs and retrieve definitions and translations from and to lojban using a dictionary file.

:copyright: 2016 Giménez, Christian
:author: Giménez, Christian
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# lojban.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from re import *


class LojbanDef(object):
    def __init__(self, term, trans, definition):
        self.term = term
        self.translation = trans,
        self.definition = definition

    def __str__(self):
        return self.term + ": " + self.translation


class LojbanDict(object):
    def __init__(self, path):
        self.path = path
        self._file = open(path, 'r')

    # TODO: Change the format of the file into SQLite.
    
    def search_gismu(gismu):
        '''
        Search a Gismu in the file.
        '''
        lst_res = []
        exp = '^ (\a+)\s+(\a+)\s+(\a+)\s+(\a+)\s+(.+)$'
        
        for l in self._file:
            m = match(exp, l)
            if m != None:
                lst_res.push(LojbanDef(m.group(1), m.group(4), m.group(5)))

        return lst_res
        
    
        
