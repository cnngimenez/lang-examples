# -*- coding: utf-8 -*-
'''
Ejemplos de Excepciones.



:copyright: 2015 Giménez, Christian
:author: Giménez, Christian
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# exception.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math

# A veces conviene generar la excepción en la terminal para saber cómo se
# llama.
1/0
# Genera "ZeroDivisionError"

try:
    1/0
except ZeroDivisionError:
    print("Excepción ZDE...") # Manejo de una excepción particular.
else:
    print("Otra excepción...") # En caso de que no suceda excepción.
finally:
    print("Siempre se ejecuta...") # Siempre se ejecuta esta parte, con o sin excepción.

# Veamos una excepción que no está dentro de except...

try:
    math.sqrt(-1) # Raises ValueError
except ZeroDivisionError:
    print("ZDE")
else:
    print("Else")
finally:
    print("Finally")

# Cómo emitir una excepción...
# También se puede hacer una subclase de Exception o  BaseException
try:
    raise Exception("juana")
except Exception as e:
    print("Exception raised:", e)
    print("hi")

try:
    1/0
except:
    print("exc") # Se ejecuta siempre que suceda una excepción.
