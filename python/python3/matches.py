'''
   Copyright 2014 Giménez, Christian
   
   Author: Giménez, Christian   

   matches.py
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
Ejemplo de matching para expresiones regulares.
'''

from re import *

exp="(a|b)*(ab)(a|b)*"
s=""
while s != "exit":
    print("ingrese la cadena para verificar si pertenece al lenguaje expresado por " + exp + " (exit para salir):")
    s=raw_input()
    m = match(exp, s)
    if m != None :
        if m.group(0) == s: # matchea toda la cadena?
            print("pertenece")
        else:
            print("no pertenece")
    else: # No matchea con el comienzo de la cadena
        print("no pertenece") 

    
