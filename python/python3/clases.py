# -*- coding: utf-8 -*-
# Copyright 2013 Giménez, Christian
   
# Author: Giménez, Christian   

# clases.py
   
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


'''
¿Encapsulación? ¿qué es eso?
Aquí este concepto no existe, con lo que la visibilidad tampoco.

'''

import sys

class Persona(object) :
    def __init__(self , nombre, edad) :
        self.nombre = nombre
        self.edad = edad
    def mostrar_edad(self) :
        '''
        La documentación para pydoc se ubica después de la función.
        '''
        print(self.edad)
    def metodo_abstracto(self) :
        pass # pass es un no-op de python para ocupar el lugar. Debe haber algo en la siguiente línea después de un ":".
    def __str__(self):
        '''
        Exportación a string, ¿qué se devuelve al escribir print(persona)?
        '''
        return "persona>> nombre:" + self.nombre

p = Persona('Juana', 10)
# no olvidar los paréntesis, sino muestra información acerca de la función (como GNU R).
p.mostrar_edad() 
print(p)


# Los métodos de clase y los métodos de objeto son indistinguibles excepto por una cosa: el parámetro "self".
#
# Esto es python3, no funciona con python2 por las anotaciones en la función.
class PersonaB():
    def __init__(self, nombre: str):
        self.__nombre = nombre

    def get_nombre(self) -> str:
        return self.__nombre

    def buscar() -> Persona :
        # método de clase...
        print("buscando...")
    
