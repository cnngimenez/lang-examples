#+property: header-args :lang racket
Racket is an interpreter for a Scheme dialect, which in turn is a dialect of LISP.

Scheme is standarized as R6RS. Its home page is http://www.scheme-reports.org/. The new standard is R7RS but Racket 7.4 does not support it.

Racket is a very mature R6RS scheme dialect. It has got its own derivative dialects and several libraries. Also, there is a package manager called ~raco~. Its home page is https://racket-lang.org/.

Other interpreters of Scheme are Guile and MIT-scheme. Guile is an R5RS augmented implementation. It also support R6RS core features and others. By default, it is part of the GNU project and is used mostly as an embebbed language for other programming languages like C.

* Using Emacs
For Emacs users, this Org file provides a local variable at the end, the ~geiser-default-implementation~, which sets the tangled dialect to use. Thank to this setting, if the user calls ~M-x org-babel-tangle~ function, Org Babel will not repeatedly request the scheme dialect for every code block in the file.

There are some tweaks to execute the code using Babel. First download WallyQS' ob-racket.el file[fn:2] and then load it using ~M-x load-file~. This is required because the Racket language hasn't got a supported Babel module. Also, Babel provides its own ob-scheme module by default. In order to set the interpreter to Racket, the following is needed at your Emacs init file. More information can be found at the question "How do I set org-mode to use Racket instead of guile?" at Stack Exchange[fn:3].

#+BEGIN_SRC elisp
(add-hook 'scheme-mode-hook 'geiser-mode)
(setq geiser-default-implementation 'racket)
#+END_SRC

* Hello World
:PROPERTIES:
:header-args: :tangle out/hello-world.rkt
:END:

The hello world is a write function. 

The ~#lang racket~ is not included in the snippets. It indicates that is a module which filename is used as the module id and ~racket~ is the base module. 
In other words, it is the same as the following sentence:

: (module hello-world racket (write "Hello world"))

See the Module Syntax section at the racket guide[fn:1] for more information. Also, there is a [[*Module syntax][Module syntax]] section in this text.

#+BEGIN_SRC racket
  (write "Hello world")
#+END_SRC

#+RESULTS:
: Hello world

* R6RS Guidelines
The following is a quotation of the guidelines from the R6RS report.

#+begin_quote
    1. allow programmers to read each other’s code, and allow development of portable programs that can be executed in any conforming implementation of Scheme;
    2. derive its power from simplicity, a small number of generally useful core syntactic forms and procedures, and no unnecessary restrictions on how they are composed;
    3. allow programs to define new procedures and new hygienic syntactic forms;
    4. support the representation of program source code as data;
    5. make procedure calls powerful enough to express any form of sequential control, and allow programs to perform non-local control operations without the use of global program transformations;
    6. allow interesting, purely functional programs to run indefinitely without terminating or running out of memory on finite-memory machines;
    7. allow educators to use the language to teach programming effectively, at various levels and with a variety of pedagogical approaches; and
    8. allow researchers to use the language to explore the design, implementation, and semantics of programming languages.
#+end_quote

The second guideline emphasises in the small number of rules for forming simple expressions. Scheme has no restrictions about how to compose them. This idea (and several Scheme basis) was declared before on the lambda calculus.

** Homoiconicity and Symbolic Programming
The fourth guideline dictates that a source code can be data or instructions. This allows that a piece of code can be interpreted as data and passed along to other programs written in the same language. This is called ~homoiconicity~.

Homoiconicity ("same representation" translated from greek) is a property of a language. It states that a program can be manipulated as data using the same language in which is written. Thus, the program's internal representation can be inferred by just reading the program itself. All programs can be accessed as data. This action is called "code as data" and the inverse "data as code".

Symbolic programming is a programming paradigm in which the program can manipulate its own formulas and program components as if they were plain data. Advantages of homoiconicity and symbolic programming is that the program can modify itself and appear to "learn".

LISP derivaties (i.e. Scheme) and Prolog programs are symbolic.

** Declarative Programming
Declarative programming is a programming paradigm. It express the logic of a computation (the desired results) without describing its control flow. Some definitions show the constrast between it and imperative languages: 

- It is any style that is not imperative.
- A high-level program that describes what a computation should perform (i.e. not how it should perform).
- Any programming language that lacks side-effects.
- A language with clear correspondence to mathematic logic.

Prolog is considered as a declarative programming language. Pure functional programming like Haskell are considered the same too. 
LISP and Scheme are a mixture of functional and procedural programming styles. Thus, they are not fully declarative.


* Cheat-sheet
  Execute ~help~ and click on the [[file:/usr/share/doc/racket/racket-cheat/index.html][cheatsheet link]]. If the function is not available, load it with ~(require racket/help)~.

* TODO Datatypes
** Built-in
These built-in types are described at the R6RS scheme standard. The Racket interpreter provides them as it is without importing libraries.

*** Boolean
To constants exists: ~#t~ and ~#f~. The uppercase are defined but the lower form is preferred.

The function ~boolean?~ recognizes these constants.

Boolean expressions resulting in other types are recognized as true.
*** Numbers
Numbers are divided into two categories: exact and inexact. It is possible to force parsing a litteral into exact or inexact prefixing the number with ~#e~ or ~#i~ respectivelly.

For example, 0.5 is an inexact floating point, but it can be an exact rational with the ~#e~ prefix.

#+BEGIN_SRC racket
#e0.5
#+END_SRC

#+RESULTS:
: 1/2

The inexact value will look like as follows.

#+BEGIN_SRC racket
0.5
#+END_SRC

#+RESULTS:
: 0.5

**** Exact Numbers
Exact numbers are integer or rational, real or complex numbers that represents the a ratio of two arbitrary integers. For instance: ~5, -17, 1/2, -3/4, 1+2i~ and ~1/2+3/4i~.

**** Inexact Numbers
Floating points are considered as inexact numbers. ~2.0~ and ~3.14e+87~ are examples of these. Also, complex numbers with real and imaginary parts that are floating points are inexact numbers (ex. ~2.0+3.0i~)

Infinite are represented as ~+inf.0~ and ~-inf.0~. Not-a-number is written as follows: ~+nan.0~ or ~-nan.0~. They have their complex derivatives like ~-inf.0+nan.0i~.

**** Converting between exact and inexact
The computation between an inexact value always generate inexact results. However, both types be converted using ~exact->inexact~ and ~inexact->exact~ functions.

**** Interpretation of digits
Binary, octal and hexadecimal literals representation are declared using the ~#b, #o~ and ~#x~ prefix. 

#+BEGIN_SRC racket
#b110010
#+END_SRC

#+RESULTS:
: 50

#+BEGIN_SRC racket
#xa20
#+END_SRC

#+RESULTS:
: 2592

**** Comparison
The ~=~ function compares using numerical equality. It converts inexact numbers to exact before comparing. The ~eqv?~ and ~equal?~ compares considering if both are exact numbers and then the numerical equality.

#+BEGIN_SRC racket :results output
(writeln (= 1 1.0))
(writeln (eqv? 1 1.0))
#+END_SRC

#+RESULTS:
: #t
: #f

Comparison between inexact numbers will lead to unexpected results as they are transformed into exact values. In the next example, comparing 1/10 with 0.1 results into false because 0.1 converted to exact is not 1/10.

#+BEGIN_SRC racket :results output
(writeln (= 1/2 0.5))
(writeln (= 1/10 0.1)) 
(writeln (inexact->exact 0.1))
(writeln (= 3602879701896397/36028797018963968 0.1))
#+END_SRC

#+RESULTS:
: #t
: #f
: 3602879701896397/36028797018963968
: #t

*** TODO Characters

*** TODO Strings

*** TODO Bytes and Byte strings

*** TODO Symbols

*** TODO Keywords

*** TODO Pairs and Lists

*** TODO Vectors

*** TODO Hash tables

*** TODO Boxes

*** TODO Void and Undefined

** TODO Programer-defined

* TODO Expressions and Definitions

** Function calls

** Local binding

** Conditionals

** Sequencing

* Closure
:PROPERTIES:
:header-args: :tangle out/closure.rkt
:END:
Usually, when a Scheme interpreter finds a let, it creates an environment, evaluates the body and forget the local variables. There is no way to retrieve the local variables after the let body is evaluated.

If a the let body contains a lambda expression, the local environment is not forgotten. Instead, it becomes associated with the procedure created by the lambda expression. It works as follows:

1. The scheme creates the procedure and its environment when it finds the lambda expression.
2. Whenever the procedure is called, the interpreter restore the environment and execute the procedure code within that environment again.

In the following code, it defines a function generator and use it to create a new function. The new ~a-function~ has got its own environment associated. Every time it is called, it uses the same local variables. As a result, calling ~a-function~ twice would not provide the same value.

#+BEGIN_SRC racket :lang racket
  (define (number-generator)
    (let ((my-num 0))
      (lambda ()
        (set! my-num (+ my-num 1))
        my-num
        ) ;; lambda
      ) ;; let
    ) ;; define

  (define a-function (number-generator))

  (write (a-function))
  (write "-")
  (write (a-function)) ;; writes 1 and 2!
#+END_SRC

#+RESULTS:
: 1"-"2

* Modules
Modules organize code into multiple files. Each module resides in its own file. It also provides abstraction by hiding local functions.

** Defining modules
:PROPERTIES:
:header-args: :tangle out/my-module.rkt
:END:

The following code is an example of a module definition. 

#+BEGIN_SRC racket
  #lang racket

  (provide func-a)

  (define (func-a n)
    (write n))

  (define (func-b n)
    (write "func-b:" n))
#+END_SRC

The ~func-a~ function is exported because ~provide~ declares the symbol to export. ~func-b~ is a local function.

** Using modules
:PROPERTIES:
:header-args: :tangle using-modules.rkt
:END:

Importing modules is achieved by the ~require~ function. 

The following code imports the module that has just been described and use the exported ~func-a~ function.

#+BEGIN_SRC racket
(require "out/my-module.rkt")

(func-a 1)
#+END_SRC

** Module syntax
:PROPERTIES:
:header-args: :tangle out/my-module-2.rkt
:END:

The ~#lang~ line is a shorthand for a ~module~ form. This line is cannot be used at the REPL interpreter because it must be terminated by the end-of-file.

The ~module~ form is as follows:

: (module NAME-ID INITIAL-MODULE-PATH DECL ...)

NAME-ID is the name of the module (in ~#lang~ is the filename), INITIAL-MODULE-PATH is an initial import.

For example, the module code from the previous section can be rewrited as the following snippet. This example can be evaluated in the REPL.

#+BEGIN_SRC racket
(module my-module racket
 
  (provide func-a)
 
  (define (func-a n)
    (write n))

  (define (func-b n)
    (write "func-b:" n))
)
#+END_SRC

#+RESULTS:


** Working locally in REPL
Using ~,switch~ (~,switch-namespace~), the user can change the current namespace to a local one. This means that any symbol within a module can be made available by switching to its namespace. However, the command for using a module namespace is ~,enter~, because switch will create a new one.

Thus, on a REPL, typing  ~,enter my-module~ should switch to the namespace inside the module. And, ~,toplevel~ should exit the module's namespace to the toplevel one.
* TODO Contracts
* TODO Classes and Objects
* TODO Units
Separate programs into separately compliable and reusable components.
* TODO Reflection and Dynamic Evaluation
* TODO Macros
* TODO Creating Languages
* TODO Concurrency and Synchronization
* TODO Parallelism

* Meta     :noexport:

  # ----------------------------------------------------------------------
  #+TITLE:  Racket Scheme Implementation
  #+AUTHOR: Christian Gimenez
  #+DATE:   24 may 2019
  #+EMAIL:
  #+DESCRIPTION: 
  #+KEYWORDS: 

  #+STARTUP: inlineimages hidestars content hideblocks entitiespretty indent fninline latexpreview
  #+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)
  #+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
  #+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc tex:imagemagick
  #+LINK_UP:   
  #+LINK_HOME: 
  #+XSLT:

  # -- HTML Export
  #+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
  #+EXPORT_SELECT_TAGS: export
  #+EXPORT_EXCLUDE_TAGS: noexport
  #+HTML_LINK_UP: index.html
  #+HTML_LINK_HOME: index.html

  # -- For ox-twbs or HTML Export
  #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
  #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
  #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>
  #+LANGUAGE: en

* Footnotes

[fn:3] https://emacs.stackexchange.com/questions/14110/how-do-i-set-org-mode-to-use-racket-instead-of-guile/14111 

[fn:2] https://github.com/wallyqs/ob-racket 

[fn:1] https://docs.racket-lang.org/guide/Module_Syntax.html#%28part._module-syntax%29 
  # Local Variables:
  # org-hide-emphasis-markers: t
  # org-use-sub-superscripts: "{}"
  # fill-column: 80
  # visual-line-fringe-indicators: t
  # ispell-local-dictionary: "british"
  # geiser-default-implementation: "racket"
  # End:
