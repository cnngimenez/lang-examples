

- R^{6}RS https://www.r6rs.org/

Implementations:

- Racket
- Guile

* Meta     :noexport:

# ----------------------------------------------------------------------
#+TITLE:  Scheme
#+SUBTITLE:
#+AUTHOR: Christian Gimenez
#+DATE:   16 ene 2024
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick

#+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)

# -- Export
#+LANGUAGE: en
#+LINK_UP:   
#+LINK_HOME: 
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
# #+export_file_name: index

# -- HTML Export
#+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
#+HTML_LINK_UP: index.html
#+HTML_LINK_HOME: index.html
#+XSLT:

# -- For ox-twbs or HTML Export
# #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
# -- -- LaTeX-CSS
# #+HTML_HEAD: <link href="css/style-org.css" rel="stylesheet">

# #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
# #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>


# -- LaTeX Export
# #+LATEX_CLASS: article
#+latex_compiler: xelatex
# #+latex_class_options: [12pt, twoside]

#+latex_header: \usepackage{csquotes}
# #+latex_header: \usepackage[spanish]{babel}
# #+latex_header: \usepackage[margin=2cm]{geometry}
# #+latex_header: \usepackage{fontspec}
# -- biblatex
#+latex_header: \usepackage[backend=biber, style=alphabetic, backref=true]{biblatex}
#+latex_header: \addbibresource{tangled/biblio.bib}
# -- -- Tikz
# #+LATEX_HEADER: \usepackage{tikz}
# #+LATEX_HEADER: \usetikzlibrary{arrows.meta}
# #+LATEX_HEADER: \usetikzlibrary{decorations}
# #+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing}
# #+LATEX_HEADER: \usetikzlibrary{shapes.geometric}
# #+LATEX_HEADER: \usetikzlibrary{shapes.symbols}
# #+LATEX_HEADER: \usetikzlibrary{positioning}
# #+LATEX_HEADER: \usetikzlibrary{trees}

# #+LATEX_HEADER_EXTRA:

# --  Info Export
#+TEXINFO_DIR_CATEGORY: A category
#+TEXINFO_DIR_TITLE: Scheme: (Readme)
#+TEXINFO_DIR_DESC: One line description.
#+TEXINFO_PRINTED_TITLE: Scheme
#+TEXINFO_FILENAME: Readme.info


# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
