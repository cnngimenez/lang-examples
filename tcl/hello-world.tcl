
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# hello-world.tcl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

puts "Hello world"

# Idem al comando anterior.
puts {Hello world}

# Se puede usar sin comillas pero sólo si no hay espacios.
puts Helloworld ;# un comentario después del comando.

# Cuando está entre corchetes, el parámetro se evalúa previo a realizar el comando que lo engloba.
puts [string length "hola"]
# Primero ejecuta string length "hola", luego imprime el resultado.
