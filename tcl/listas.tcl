
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# listas.tcl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set x [list a b c d] ;# genera una lista con cuatro elementos
set x2 [list 1 2 3 4]
set z [split {hola mundo}] ;# divide {hola mundo} en dos palabras (una lista de dos eltos.)
set z [split {hola/mundo} /] ;# divide {hola/mundo} en dos usando / como separador.

puts [lindex $x 1] ;# devuelve "b" (el segundo elemento, el primero es 0).
puts [llength $x]


puts "foreach 1:"
foreach a $x { puts "a: $a" }

puts "\nforeach 2:"
foreach {a b} $x { puts "a: $a b:$b" }

puts "\nforeach 3:"
foreach a $x b $x2 {puts "a: $a b:$b" }


