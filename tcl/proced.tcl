
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# proced.tcl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set x 1

proc sum {arg1} {
    set arg1 2 ;# pasaje por copia.
    
    return $arg1 ;# retorno al valor de función
}

puts "x es antes de sum $x"
puts "sum retorna [sum $x]"
puts "x es ahora $x"

# b es un parámetro opcional, su valor por defecto es 1.
proc sumar {a {b 1}} {
    return [expr {$a + $b}]
}

proc sumar2 {a {b 1} args} {
    puts $a
    puts $b
    puts $args    
}
