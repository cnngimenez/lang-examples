
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# string.tcl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Este texto reemplaza las variables con su valor.

set que mundo

puts "hola $que"

# Los textos entre llaves no reemplazan los valores.

puts {hola $que}

# Observar que Tcl considera a todo un string y dependiendo del contexto lo interpretará como un número u otro tipo de dato.

puts hola
puts Juan

# Es por ello que se pueden usar sin llaves ni comillas.


# El parseo de Tcl es por palabras, las cuales se separan por espacios excepto cuando se usan los limitadores de palabras que son las llaves: {una palabra} es una sola palabra.

# El enter es un limitador de instrucciones, igual que el ";".
