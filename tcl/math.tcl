
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# math.tcl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


puts [expr 9 + 1]

# Las expresiones se pueden evaluar por medio de expr. Funciona con casi cualquier expresión lógica o matemática.

# Se recomienda encerrar entre llaves para aumentar la performance (no parsea varios argumentos, sino uno solo).

puts [expr {9 + 1}]


set x {10 + 20}
puts "Expr:"
puts $x
puts "Res:"
puts [expr $x]

# Se puede utilizar las funciones (observar que son funciones, ¡no comandos!).

# Los comandos deben ser evaluados previo a usarse:

puts "hola mundo + 20:"
puts [string length "hola mundo"]
puts [expr [string length "hola mundo"] + 20]
