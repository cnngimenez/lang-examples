
# Copyright 2014 Giménez, Christian

# Author: Giménez, Christian   

# estruc.tcl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# then y else es opcional ya que el cuerpo y las expresiones deben ser sólo una palabra.
# en la expresión no es necesario utilizar expr.

# ----------------------------------------------------------------------------------------------------

set x 1 

# Las expresiones pueden retornar yes/no true/false 0/!=0
# Los cuerpos y las expreisones pueden darse por medio de comillas o llaves. Recordar que las comillas permiten la sustitución, las llaves no.
if {$x == 1} then {puts "x es 1"} elseif {$x == 2} then {puts "x es 2"} else {puts "x no es 1"}

if {$x == 1} {
    puts "x es 1"
} {
    puts "x no es 1"
}

# Recordar que las palabras se separan con espacios, con retornos de carro se separan los comandos.

# ----------------------------------------------------------------------------------------------------

puts "ejemplo switch"

switch $x {
    1 { puts "1" }
    2 { puts "2" }
}

switch $x 1 { puts "1" } 2 { puts "2" }

# ----------------------------------------------------------------------------------------------------

puts "ejemplo while"

while { $x < 5 } {
    puts $x
    set x [expr {$x + 1}]
}

# ----------------------------------------------------------------------------------------------------

puts "ejemplo for"

for {set i 0} {$i < 10} {incr i} {
    puts $i
}
