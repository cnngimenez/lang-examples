/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * bucles.cpp
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
using std::cout;
using std::endl;

int main()
{
	int i;
	cout<<"Bucle for"<<endl;
	for(i=0;i<10;i++)	//bucle for, se repite lo que contiene hasta que se cumpla la condicion del medio. La estructura es for(asignacion de la variable auxiliar;condicion de corte;modificacion de la variable al final del ciclo)
	{
		cout<<i<<endl;
	}
	//en este punto i=10
	cout<<"Bucle while"<<endl;
	while(i<20)
	{
		cout<<i<<endl;
		i++;	//hay que incrementarlo, no se hace solo
	}
	//en este punto i=20
	cout<<"Bucle do while"<<endl;
	do
	{
		cout<<i<<endl;
		i++;
	}while(i<30);
}
