/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * punteros.cpp
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
using std::cout;
using std::endl;

int main()
{
	int a;	//variable entera a
	int *p;	//puntero a entero p. Solo puede apuntar a enteros
	a=3;
	cout<<"a = "<<a<<endl;
	p=&a;	//p apunta a la direccion de a
	cout<<"p apunta a la direccion "<<p<<" y contiene: "<<*p<<endl;	//la expresion *p se interpreta como "lo que contiene el espacio al que apunta p"
	*p=5;	//como p apunta a la direccion de a, modifico el valor de a utilizando el puntero p
	cout<<"a = "<<a<<endl;
}
