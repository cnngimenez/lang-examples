/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * estructuras de control.cpp
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
	int a;
	cout<<"Ingrese un numero del 1 al 3:"<<endl;
	cin>>a;
	cout<<"usando if y else:"<<endl;
	if(a==1)
	{
		cout<<"a vale uno"<<endl;
	}
	else
	{
		if(a==2)
		{
			cout<<"a vale dos"<<endl;
		}
		else
		{
			if(a==3)
			{
				cout<<"a vale tres"<<endl;
			}
			else
			{
				cout<<"a tiene otro valor"<<endl;
			}
			
		}
	}
	cout<<"usando switch y case:"<<endl;
	switch(a)
	{
		case 1:
			cout<<"a vale uno"<<endl;
			break;
		case 2:
			cout<<"a vale dos"<<endl;
			break;
		case 3:
			cout<<"a vale tres"<<endl;
			break;
		default:
			cout<<"a tiene otro valor"<<endl;
			break;
	}
}
