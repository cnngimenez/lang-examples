/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * clases.cpp
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
using namespace std;

class Clase{
	private:
		int campo1;
		char campo2;
		float campo3;
	public:
		Clase(){ campo1=0;campo2='a';campo3=0;}	//constructor de la clase sin parámetros, definido dentro de la definicion de la clase
		~Clase(){ cout<<"Objeto Destruido"<<endl;}	//Destructor que imprime un mensaje por pantalla
		Clase(int c1,char c2='a',float c3=0);	//constructor de la clase con 3 parámetros, los dos últimos tienen valor por defecto... si no se especifica, se usa el valor por defecto
		void imprimir_campos();
		void set_campos(int c1,char c2,float c3);
};
Clase::Clase(int c1,char c2,float c3)	//definición fuera de la clase, del contructor declarado dentro de la clase (los parametros con valor por defecto no se deben volver a especificar)
{
	campo1=c1;
	campo2=c2;
	campo3=c3;
}
void Clase::imprimir_campos()
{
	cout<<"campo1: "<<campo1<<endl<<"campo2: "<<campo2<<endl<<"campo3: "<<campo3<<endl;
}
void Clase::set_campos(int c1,char c2,float c3)
{
	campo1=c1;
	campo2=c2;
	campo3=c3;
}
int main()
{
	Clase *pc = new Clase();	//puntero a un nuevo objeto Clase (como no tiene parametros llama al primer constructor
	Clase oc(3,'b');			//nuevo objeto Clase, evitamos el ultimo parametro porque tiene un valor por defecto (opcional)
	pc->set_campos(1,'z',3.51);
	cout<<"Los campos de pc son:"<<endl;
	pc->imprimir_campos();		//se llama al método con -> por ser un puntero
	cout<<"Los campos de oc son:"<<endl;
	oc.imprimir_campos();		//se llama al métoro con . por ser directamente el objeto
	delete pc;	//al tratarse de un puntero no se destruye el objeto automaticamente y se debe liberar la memoria con la funcion delete, que llama al destructor
				//oc no se debe eliminar, cuando termina el programa se ejecuta el destructor automaticamente
}
