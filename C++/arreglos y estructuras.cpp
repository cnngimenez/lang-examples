/*Copyright 2014 Rabinovich, Ariel
 * 
 *
 * Author: Rabinovich, Ariel   
 *
 * arreglos y estructuras.cpp
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string.h>
using std::cout;
using std::cin;
using std::endl;
using std::string;

struct Contacto{
	string nombre;
	int numero;
};

int main(){
	Contacto agenda[4];	//arrelgo de 4 estructuras Contacto
	cout<<"Ingrese el nombre del nuevo contacto: ";
	cin>>agenda[0].nombre;	//lo ingresado por teclado se almacenara en el primer contacto del arreglo agenda, en el campo nombre
	cout<<"Ingrese el numero de telefono del contacto: ";
	cin>>agenda[0].numero;
	cout<<"Nuevo contacto agregado: "<<endl<<agenda[0].nombre<<": "<<agenda[0].numero<<endl;
	agenda[1].nombre = "Pepito";
	agenda[1].numero = 156282822;	//contacto agregado, no ingresado por teclado;
	agenda[2].nombre = "Oreo";
	agenda[2].numero = 156193875;
	agenda[3].nombre = "Opera";
	agenda[3].numero = 156987542;
	
	int i;
	cout<<endl<<"Agenda completa:"<<endl;
	for(i=0;i<4;i++)
	{
		cout<<"Contacto "<<i+1<<":"<<endl;
		cout<<agenda[i].nombre<<": "<<agenda[i].numero<<endl;
	}
	
}
