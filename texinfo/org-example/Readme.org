#+property: header-args :results silent

This is an Org example. Its objective is to show how to configure the file to export Info files.

In Org-mode, the defaulte exporter is ox-texinfo. It can be loaded with the following code:

#+BEGIN_SRC elisp
(load-library "ox-texinfo")
#+END_SRC

The dir file is not generated.

* The ox-texinfo meta-lines
The Org file requires some information for the ox-texinfo exporter. The code ref:code:texinfo-configs is the usual configuration for the exporter.

The first three lines specify the directory file entry to be used. When  This directory file is generated from the info file information and is required for info to show the entry.

Also, note the directory title name: It is a title and the top node of the info file. This is explained at the texinfo (see Node [[info:texinfo#Other Info Files][texinfo#Other Info Files]]). In summary, the syntax can be one of these posibilities:

: FIRST-ENTRY-NAME:(FILENAME)NODENAME.     DESCRIPTION
: (FILENAME)SECOND-NODE::                  DESCRIPTION

Remember, to create the dir file, use the =install-info= program. If it is already created, it will add a new entry only for the sepecified info file. For example, the following command can be used to create the directory file, and add it with the Readme.info.

: install-info Readme.info dir

#+name: code:texinfo-configs
#+caption: Org configurations in a file.
#+BEGIN_SRC org
  # --  Info Export
  ,#+TEXINFO_DIR_CATEGORY: A category
  ,#+TEXINFO_DIR_TITLE: Org Example: (Readme)
  ,#+TEXINFO_DIR_DESC: One line description.
  ,#+TEXINFO_PRINTED_TITLE: Org Example
  ,#+TEXINFO_FILENAME: Readme.info
#+END_SRC

# --  Info Export
#+TEXINFO_DIR_CATEGORY: Language Examples
#+TEXINFO_DIR_TITLE: Org to texinfo example: (texinfo/org-example/Readme)
#+TEXINFO_DIR_DESC: This is an org file example to export a texinfo file.
#+TEXINFO_PRINTED_TITLE: Org to texinfo example
#+TEXINFO_FILENAME: Readme.info



* Meta     :noexport:

# ----------------------------------------------------------------------
#+TITLE:  Org Example
#+SUBTITLE: How to configure to export Info files.
#+AUTHOR: Christian Gimenez
#+DATE:   12 feb 2023
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 
#+COLUMNS: %40ITEM(Task) %17Effort(Estimated Effort){:} %CLOCKSUM

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick

#+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)

# -- Export
#+LANGUAGE: en

# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
