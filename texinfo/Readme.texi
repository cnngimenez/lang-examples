\input texinfo    @c -*- texinfo -*-
@c %**start of header
@setfilename Readme.info
@settitle Texinfo
@documentencoding UTF-8
@documentlanguage en
@c %**end of header

@dircategory Language Examples
@direntry
* Texinfo: (texinfo/Readme). Texinfo language examples.
@end direntry

@finalout
@titlepage
@title Texinfo
@author Christian Gimenez
@end titlepage

@contents

@ifnottex
@node Top
@top Texinfo
@end ifnottex

@menu
* Compiling::
* A Texinfo template::
* Images::
* Documentation and resources::

@detailmenu
--- The Detailed Node Listing ---

A Texinfo template

* Directory entry data::
* Title::
* Table of contents::
* Ending::

Images

* Minimal header::
* Images without scaling::
* Showing images in info program::
* Images with alterantive text::
* Image scaled::
* End of document::

@end detailmenu
@end menu

@node Compiling
@chapter Compiling

@float Listing,orgbf59f51
@example
makeinfo Readme.texi
info -f Readme.info
@end example
@caption{Commands to compile the texinfo file and read it.}
@end float

@float Listing,orgc4834d0
@example
rm dir
install-info Readme.info dir
@end example
@caption{Recreating the dir file.}
@end float

@node A Texinfo template
@chapter A Texinfo template

The @samp{\input texinfo} line states to use texinfo.tex file for the @TeX{} compiler.
The comment tells Emacs to use texinfo major mode.

@example
\input texinfo     @@c -*- texinfo -*-
@end example

@example
@@setfilename template.info
@@settitle A Texinfo template
@@documentencoding UTF-8
@@documentlanguage en
@end example

@menu
* Directory entry data::
* Title::
* Table of contents::
* Ending::
@end menu

@node Directory entry data
@section Directory entry data

@example
@@dircategory Language Examples
@@direntry
* A Texinfo template
@@end direntry
@end example

@node Title
@section Title

The @samp{@@finalout} command indicates to @TeX{} to avoid printing the black warning rectangles (common on draft @LaTeX{} modes).

@example
@@finalout
@@titlepage
@@title A Texinfo template
@@author Christian Gimenez           
@@end titlepage
@end example

@node Table of contents
@section Table of contents

@example
@@contents
@end example

@node Ending
@section Ending

This commands indicates that the texinfo file has ended. @TeX{} compilers stop processing the file when this command is red.

@example
@@bye
@end example

@node Images
@chapter Images

@menu
* Minimal header::
* Images without scaling::
* Showing images in info program::
* Images with alterantive text::
* Image scaled::
* End of document::
@end menu

@node Minimal header
@section Minimal header

@example
\input texinfo     @@c -*- texinfo -*-
@@settitle Image example
@@node Top
@@top Image example
@end example

@node Images without scaling
@section Images without scaling

@example
This is a banner from GNU Web page: https://www.gnu.org/graphics/chan-i-love-fs-banner.html

It is called ``I Love Free Software'' by Carlos Chan.

You can view this image with Emacs using X window. Info would not show anything.

@@image@{../imgs/chan-i-love-fs-banner,,,,.png@}      
@end example

@node Showing images in info program
@section Showing images in info program

The images can be seen in Emacs running under X window. But in terminal, info or Emacs would not show the images.

An ASCII art can be used to replace the PNG image. Simply, create a txt file with the same name. For instance, the following code shows the @samp{chan-i-love-fs-banner-500x167.txt} file in info, but in Emacs under X it shows @samp{chan-i-love-fs-banner-500x167.png} file.

@example
Info can show this image as an ASCII art. Emacs shows it as a PNG file.

@@image@{../imgs/chan-i-love-fs-banner-500x167,,,,.png@}
@end example

@node Images with alterantive text
@section Images with alterantive text

@example
Now, the original image, but with an alternative text. Info should show the alternative text.

@@image@{../imgs/chan-i-love-fs-banner,,,I love Free Software banner,.png@}
@end example

@node Image scaled
@section Image scaled

@node End of document
@section End of document

@example
@@bye
@end example

@node Documentation and resources
@chapter Documentation and resources

@itemize
@item
Info manual: @ref{Top,texinfo#Top,,texinfo,}
@end itemize

@bye