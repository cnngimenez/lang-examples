<?php 
/* 

   Copyright 2016 Giménez, Christian
   
   Author: Giménez, Christian   

   helloworld.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


newt_init();
newt_cls();

newt_draw_root_text(10,10,"hi world!");
newt_refresh();
sleep(1);

newt_draw_root_text(10,11, "Help line test...");
newt_push_help_line("Done and done!");
newt_push_help_line("testing 2...");
newt_push_help_line("help line...");
newt_refresh();
sleep(1);
newt_pop_help_line();
newt_refresh();
sleep(1);
newt_pop_help_line();
newt_refresh();
sleep(1);

newt_finished();