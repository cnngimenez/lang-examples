<?php 
/* 

   Copyright 2016 Giménez, Christian
   
   Author: Giménez, Christian   

   forms.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

newt_init();
newt_cls();

newt_open_window(10,10,40,20, "Ejemplo de formulario");

// crear componentes del formulario
$b1 = newt_button(1,1, "Hola");
$b2 = newt_compact_button(1,8, "Chau");

// Crear formulario
$form = newt_form();
// Agregar componentes al formulario.
// Se puede usar newt_form_add_component para agregar uno solo.
newt_form_add_components($form, [$b1, $b2]);   

// Arrancar el formulario...
newt_refresh();
newt_run_form($form);

newt_form_destroy($form);

newt_finished();
