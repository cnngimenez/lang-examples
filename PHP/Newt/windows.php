<?php 
/* 

   Copyright 2016 Giménez, Christian
   
   Author: Giménez, Christian   

   windows.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
newt_init();
newt_cls();

// Las ventanas se manejan como una pila de ventanas, y siempre la que
// está en el tope es la que está actualmente en foco.

// Agrega una ventana a la pila de ventanas...
newt_centered_window(10,10, "Title");
newt_refresh();

newt_wait_for_key();

// Agrega otra ventana...
newt_open_window(20,20,10,10, "New Window");
newt_refresh();

newt_wait_for_key();

// Remueve una ventana del tope de la pila
newt_pop_window();
newt_refresh();

newt_wait_for_key();

newt_finished();
