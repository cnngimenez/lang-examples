<?php 
/* 

   Copyright 2016 Giménez, Christian
   
   Author: Giménez, Christian   

   hello-world.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

ncurses_init(); // Inicializa la terminal para usar con NCurses.

ncurses_addstr("hi world"); // Imprime en pantalla.
ncurses_refresh();


// VENTANA "HI 2"

// Crear una ventana.
$win = ncurses_newwin(20,20,0,0);
// Dibujar bordes: win, left, right, top y bottom; esquinas: tl, tr, bl, br.
ncurses_wborder($win, 0,0,0,0,0,0,0,0);
ncurses_wrefresh($win);

ncurses_waddstr($win, "hi 2!"); // Imprime en la venana $win    
ncurses_wgetch($win); // Espera por un caracter en la ventana.


ncurses_end(); // Siempre terminar con end.
