<?php 
/* 

   Copyright 2016 Giménez, Christian
   
   Author: Giménez, Christian   

   colors.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

ncurses_init(); // Inicializa la terminal para usar con NCurses.

// Chequea e inicializa los colores.
if (! ncurses_has_colors()){
    echo "No permite cambiar colores.";
}
ncurses_start_color();

// Define el color de frente y fondo como el número "1".
ncurses_init_pair(1, NCURSES_COLOR_YELLOW, NCURSES_COLOR_BLUE);
ncurses_color_set(1); // Indica que se va a usar el par de color definido como "1".

ncurses_mvaddstr(10,10, "Hi world!");
ncurses_refresh();
    
ncurses_getch();

ncurses_end(); // Siempre terminar con end.
