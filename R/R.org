#+property: header-args :session s1

* Data Types

** Integer

#+BEGIN_SRC R :tangle out/types.R
i <- 10
i <- -10
#+END_SRC

** Double

#+BEGIN_SRC R :tangle out/types.R
f <- 10.5
#+END_SRC

** Infinitie and Not a Number
~Inf~ means infinite, a possible result for a function. ~NaN~ means "Not a Number" and can be returned when some calculations have no results, for example 0/0.
#+BEGIN_SRC R :tangle out/types.R
n <- Inf 
n2 <- NaN
#+END_SRC
** Character

String types are called "Character" in R.

#+BEGIN_SRC R :tangle out/types.R
s <- "hola mundo"
#+END_SRC

** Booleans

#+BEGIN_SRC R :tangle out/types.R
  b <- TRUE # También puede usarse cualquier valor distinto de 0
  b2 <- FALSE # También puede usarse el valor 0
  ## Operadores
  b <- b | b # Or
  b <- b & b # And
  b <- !b
#+END_SRC

** Primitive Complex 

Some common functions are:

| Function | Description                   |
|----------+-------------------------------|
| names()  | Get the names of the headers  |
| length() | Return the length of the list |
| ncol()   | Return the number of columns  |
| nrow()   | Return the number of rows.    |

Most of them are defined at the ~base~ package, which is available at runtime.

*** List
Can contain elements of mixed types. It doesn't do cohertion and maintain the data types individually.

#+BEGIN_SRC R :tangle out/types.R
l <- list(1, 2, "hola", 3.5)
#+END_SRC

#+RESULTS:
| 1 | 2 | hola | 3.5 |

List can have fields:

#+BEGIN_SRC R
l <- list(name="Peter", age=7)
l$name
#+END_SRC

#+RESULTS:
: Peter

#+BEGIN_SRC R :results output
names(l)
#+END_SRC

#+RESULTS:
: [1] "name" "age"


*** Vector
Can contain only one data type. Maintains the simmilarity with math vectors. When the user tries to assign various objects of mixed data types, R do an implicit cohertion to the most adecuate type.


The first use integer types. The second changes all the elements into a double type. Finally, the third interprets the number as strings which is the most adecuate to store all the elements.

#+BEGIN_SRC R :tangle out/types.R
v1 <- c(1, 2, 3, 4) # Vector con componentes enteros
v2 <- c(1, 3.5, 2, 4) # Cambia el tipo de los componente a punto flotante
v3 <- c(1, 2, "hola", 3) # Cambia el tipo de los componentes a string
print(list(v1, v2, v3))
#+END_SRC

#+RESULTS:
| 1 |   1 |    1 |
| 2 | 3.5 |    2 |
| 3 |   2 | hola |
| 4 |   4 |    3 |

*** Data Frames

Data frames are table of data.

There are some data frames available by default. For listing the load ones use ~data()~ function.

#+BEGIN_SRC R :tangle out/types.R
  data()
#+END_SRC

#+RESULTS:

The following will generate a data frame which column is the sum of the women's height and weight. ~women~ is dataset available by default which has two columns: ~height~ and ~weight~.

#+BEGIN_SRC R :tangle out/types.R
  partial <- women$height + women$weight
#+END_SRC

#+RESULTS:
| 173 |
| 176 |
| 180 |
| 184 |
| 188 |
| 192 |
| 196 |
| 200 |
| 205 |
| 209 |
| 214 |
| 219 |
| 224 |
| 230 |
| 236 |

~attach()~ makes available data frame's columns for direct access. ~dettach()~ does the opposite.

Accessing ~women$height~:

#+BEGIN_SRC R :tangle out/types.R
attach(women)
height
#+END_SRC

#+RESULTS:
| 58 |
| 59 |
| 60 |
| 61 |
| 62 |
| 63 |
| 64 |
| 65 |
| 66 |
| 67 |
| 68 |
| 69 |
| 70 |
| 71 |
| 72 |

* Control structure

** Conditionals
#+BEGIN_SRC R :tangle out/est_control.R
  a <- 1

  if (a == 1)
      print("if 1: true")

  if (a == 2){
      print("if 2: true")
  }else{
      print("if 2: false")
  }
#+END_SRC

#+RESULTS:
: if 2: false

This snippet will generate error because R cannot find the ~else~ keyword.

#+BEGIN_SRC R
  a <- 1
  if (a == 1)
      print("hola")
  else 
      print("chau")
#+END_SRC

#+RESULTS:
: hola

Else-if is supported:

#+BEGIN_SRC R :tangle out/est_control.R
  a <- 1
  if (a == 1){
      print("if 3: a = 1")
  }else if (a == 2) {
      print("if 3: a = 2")
  }else{
      print("if 3: a tiene otro valor")
  }

#+END_SRC

#+RESULTS:
: if 3: a = 1

** Switchs
~switch~ is different from other languages. In this case is a function and not a keyword or primitive structure. 

The function receives two parameters:

- An expression, which will be evaluated in order to obtain an index (an integer number or string).
- A list, which contains indexes associated to an expression.

The first parameter selects one index from the second returning the resulting value.

#+BEGIN_SRC R :tangle out/est_control.R
  a <- 2
  switch(a, "hola", "mundo", "probando", "switch")
#+END_SRC

#+RESULTS:
: mundo

#+BEGIN_SRC R :tangle out/est_control.R
  nom <- "ciruela"
  switch(nom, manzana=33, pera=41, ciruela=10, durazno=23)
#+END_SRC

#+RESULTS:
: 10

** Repetitions
The most basic is ~repeat~, which will repeat until a ~break~ is founded.

#+BEGIN_SRC R :tangle out/est_control.R
  i <- 0
  repeat {
      print("repeat-break: ")
      print(i)
      i <- i + 1
      if (i >= 10) {
          break 
      }       
  }
#+END_SRC

#+RESULTS:

~while~ repeats while the condition evaluates to true.
#+BEGIN_SRC R :tangle out/est_control.R
  i <- 0
  while (i <= 10){
      print("while:")
      print(i)
      i <- i + 1
  }
#+END_SRC

#+RESULTS:

~for~ is different from the C, is more like a "for each". Repeats an action per each element of the sequence. The second parameter can be a list or vector.

#+BEGIN_SRC R :tangle out/est_control.R
  l <- list(1,2,3,4,5)
  for (i in l) print(i)
#+END_SRC

#+RESULTS:

#+BEGIN_SRC R :tangle out/est_control.R
  l <- list(1,2,3,4,5)

  for (i in l){
      i = i + 20
      print(i)
  }
#+END_SRC

#+RESULTS:

Which is the same as:
#+BEGIN_SRC R :tangle out/est_control.R :results output
  l <- 1:5

  for (i in l){
      i = i + 20
      print(i)
  }
#+END_SRC

#+RESULTS:
: [1] 21
: [1] 22
: [1] 23
: [1] 24
: [1] 25

#+BEGIN_SRC R :results output
  for (i in seq(1,5)){
      i = i + 20
      print(i)
  }
#+END_SRC

#+RESULTS:
: [1] 21
: [1] 22
: [1] 23
: [1] 24
: [1] 25

Sometimes, to process each row from a data frame or matrix, a sequence of rows is needed:

#+BEGIN_SRC R :results output
  for (row in seq(nrow(iris))){
    if (row[1] > 141){
      print(paste("Founded!:", row))
    }
  }
#+END_SRC

#+RESULTS:
: [1] "Founded!: 142"
: [1] "Founded!: 143"
: [1] "Founded!: 144"
: [1] "Founded!: 145"
: [1] "Founded!: 146"
: [1] "Founded!: 147"
: [1] "Founded!: 148"
: [1] "Founded!: 149"
: [1] "Founded!: 150"


* Packages

** Instalation
To install a new package from Internet:

#+BEGIN_SRC R
install.packages("package_name")
#+END_SRC

* Meta     :noexport:

  # ----------------------------------------------------------------------------------------------------
  #+AUTHOR:    Giménez, Christian
  #+DATE:      13 ago 2018
  #+EMAIL:
  #+DESCRIPTION: 
  #+KEYWORDS: 

  #+STARTUP: inlineimages hidestars content hideblocks entitiespretty indent fninline latexpreview
  #+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)
  #+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
  #+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc tex:imagemagick
  #+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:~/emacs_stuff/dists/scripts/org-info.js
  #+EXPORT_SELECT_TAGS: export
  #+EXPORT_EXCLUDE_TAGS: noexport
  #+LINK_UP:   
  #+LINK_HOME: 
  #+XSLT:

  # Local Variables:
  # org-hide-emphasis-markers: t
  # org-use-sub-superscripts: "{}"
  # fill-column: 80
  # End:
