
* Conventions

- @@name :: 
- @name :: Class variable
- Class#method ::
- Class::static_method ::
- instance#method? :: Predicate method. It returns true or false.
- instance#method! :: Method that modifies the object. There should be another alternative ~#method~ that returns a copy.

* Tools

- ri
- rdoc
- gem
- raco
- rubocop

* Emacs

** Packages
- yari :: Access ri documentation.
- inf-ruby :: Inferior Ruby REPL.

** Org-mode: ob-ruby
Parameters for ob-ruby Source Blocks:

- :session (none|STRING) :: Use a specific Ruby REPL session. Open a new Ruby REPL if needed. This is useful to preserve the same variables and environment between source blocks. See [[info:org#Environment of a Code Block][org#Environment of a Code Block]].  
- :result-params ::
- :result-type ::
- :ruby :: Command to use. If not specify, ob-ruby use the one specified in =org-babel-ruby-command= which is "ruby" by default.
- :xmp-option ::
- :colname-names ::
- :colnames ::
- :rowname-names ::
- :rownames ::

*** Session example
If a session is specified, then variables and the entire environment is preserved under the given session name. For instance, the following source block create a new environment called "test-session" when executed. In this environment, the variable =a= is set to 1.

#+BEGIN_SRC ruby :session test-session
  a = 1
#+END_SRC

#+RESULTS:
: 1

Now, the same variable can be used again. The following source block just add one to the variable. When executing the block once will give the value 2.

#+BEGIN_SRC ruby :session test-session
  a = a + 1
#+END_SRC

#+RESULTS:
: 2

However, when the =:session= is =none= or not specified, the variable =a= is not present. This is because a new environment is isued in this kind of blocks.

#+BEGIN_SRC ruby
  defined? a
#+END_SRC

#+RESULTS:
: nil

#+BEGIN_SRC ruby :session test-session
  defined? a
#+END_SRC

#+RESULTS:
: local-variable

* Hello World
#+BEGIN_SRC ruby :results output
puts "Hello World!"
#+END_SRC

#+RESULTS:
: Hello World!

For each new session, there should be a new buffer with the REPL running at the background. The buffer name should be the session name surrounded by asterisks. In the example, the buffer ="*test-session*"= should exists. Switch to it with =C-x b=, =M-x switch-to-buffer=, or use the following Elisp code:

#+BEGIN_SRC elisp
(switch-to-buffer-other-window "*test-session*")
#+END_SRC


* Static methods
#+BEGIN_SRC ruby :results output
  class Singleton
    class << self
      def hello
        puts 'hello world'
      end
    end
  end

  Singleton::hello
#+END_SRC

#+RESULTS:
: hello world

* System Commands

#+BEGIN_SRC ruby :results output
  puts "Today is:"
  puts `date`
#+END_SRC

#+RESULTS:
: Today is:
: dom 02 oct 2022 15:42:00 -03


* Graphical User Interfaces

** LibUI
This is the libui Gem hello world found at its Readme file, with more elements.

#+BEGIN_SRC ruby
require 'libui'

UI = LibUI

UI.init

main_window = UI.new_window('hello world', 200, 100, 1)

vbox = UI.new_vertical_box
lbl = UI.new_label('Hello world')
txt = UI.new_entry()
pass = UI.new_password_entry()
search = UI.new_search_entry()
button = UI.new_button('Button')
UI.box_append vbox, lbl, 1
UI.box_append vbox, txt, 1
UI.box_append vbox, pass, 1
UI.box_append vbox, search, 1
UI.box_append vbox, button, 1

UI.button_on_clicked(button) do
  UI.msg_box(main_window, 'Information', 'You clicked the button')
end

UI.window_on_closing(main_window) do
  puts 'Bye Bye'
  UI.control_destroy(main_window)
  UI.quit
  0
end

UI.window_set_child(main_window, vbox)
UI.control_show(main_window)

UI.main
UI.quit
#+END_SRC

** Glimmer DSL
#+BEGIN_SRC ruby
require 'glimmer-dsl-libui'

include Glimmer

class Login

  attr_accessor :name, :surname
  
  def initialize
    @name = nil
    @surname = nil
  end

end

login = Login.new

ventana = window('Glimmer test', 600, 600) {
  margined true

  vertical_box {    
    form {
      entry {
        stretchy false
        
        label 'Nombre:'
        text <=> [login, :name]
      }
      entry {
        stretchy false
        
        label 'Apellido:'
        text <=> [login, :surname]
      }
    }

    button('Guardar') {
      stretchy false
      
      on_clicked do
        msg_box "¡Hola #{login.name} #{login.surname}!"
      end
    }
  }
}

ventana.show
#+END_SRC

* Meta     :noexport:

# ----------------------------------------------------------------------
#+TITLE:  Ruby Language
#+SUBTITLE:
#+AUTHOR: Christian Gimenez
#+DATE:   02 oct 2022
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 
#+COLUMNS: %40ITEM(Task) %17Effort(Estimated Effort){:} %CLOCKSUM

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick

#+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)

# -- Export
#+LANGUAGE: en
#+LINK_UP:   
#+LINK_HOME: 
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
# #+export_file_name: index

# -- HTML Export
#+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
#+HTML_LINK_UP: index.html
#+HTML_LINK_HOME: index.html
#+XSLT:

# -- For ox-twbs or HTML Export
# #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
# -- -- LaTeX-CSS
# #+HTML_HEAD: <link href="css/style-org.css" rel="stylesheet">

# #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
# #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>


# -- LaTeX Export
# #+LATEX_CLASS: article
#+latex_compiler: xelatex
# #+latex_class_options: [12pt, twoside]

#+latex_header: \usepackage{csquotes}
# #+latex_header: \usepackage[spanish]{babel}
# #+latex_header: \usepackage[margin=2cm]{geometry}
# #+latex_header: \usepackage{fontspec}
# -- biblatex
#+latex_header: \usepackage[backend=biber, style=alphabetic, backref=true]{biblatex}
#+latex_header: \addbibresource{tangled/biblio.bib}
# -- -- Tikz
# #+LATEX_HEADER: \usepackage{tikz}
# #+LATEX_HEADER: \usetikzlibrary{arrows.meta}
# #+LATEX_HEADER: \usetikzlibrary{decorations}
# #+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing}
# #+LATEX_HEADER: \usetikzlibrary{shapes.geometric}
# #+LATEX_HEADER: \usetikzlibrary{shapes.symbols}
# #+LATEX_HEADER: \usetikzlibrary{positioning}
# #+LATEX_HEADER: \usetikzlibrary{trees}

# #+LATEX_HEADER_EXTRA:

# --  Info Export
#+TEXINFO_DIR_CATEGORY: A category
#+TEXINFO_DIR_TITLE: Ruby Language: (ruby)
#+TEXINFO_DIR_DESC: One line description.
#+TEXINFO_PRINTED_TITLE: Ruby Language
#+TEXINFO_FILENAME: ruby.info


# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
