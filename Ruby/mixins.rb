# coding: utf-8

# :stopdoc:
# Copyright 2016 Giménez, Christian
#
# Author: Giménez, Christian   
#
# mixins.rb
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# :startdoc:

# = Defs
# Module docs here...
module Defs
  def probar(param)
    puts "#{self}"
    
    # Crea una instancia de cualquier clase que use <tt>extend Defs</tt>
    c = self.new(param)
    
    puts c
    return c
  end
end

# = Defs
# Module docs here...
module Defs2
  def hola
    puts "hola #{@param_1}"
  end
end

# = MixinTest
# Testea el uso de extend. Probar:
#
# <tt> m = MixinTest.probar(1) </tt>
#
# Eso es debido al <tt>extend Defs</tt> que hay más abajo.
class MixinTest

  # Hacer de Defs#probar un método de clase extendiéndola
  #
  # Debido a esto se puede hacer:
  #
  # <code>
  # > m = MixinTest.probar(1)
  # MixinTest
  # #<MixinTest:0x00000001f6dfa0>
  # => #<MixinTest:0x000000027bade8 @param_1=2>
  # </code>
  extend Defs

  # Esto incluye la definición de Defs#probar como un método de instancia
  #
  # Gracias a esto se puede hacer:
  #
  # <code>
  # > m = MixinTest.new(2)
  # > m.hola
  # hola 2
  # </code>
  include Defs2

  def initialize(param1)
    @param_1 = param1
  end

  # --------------------
  # :section: Getters and Setters
  # Getters and Setters Messages
  # --------------------
  attr_reader :param_1


end

