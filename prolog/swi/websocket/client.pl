/*   client
     Author: poo.

     Copyright (C) 2018 poo

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     16 dic 2018
*/


:- module(client, [
	      client/0
	  ]).
/** <module> client: A WebSocket client example.

A WebSocket client that sends a text and start recieving information.
*/

:- use_module(library(http/websocket)).

/**
   wsock(+WS: stream).

A dynamic predicate that will help to store the WebSocket for repeative 
receive_all/0 actions.

@param WS The WebSocket stream.
*/
:- dynamic(wsock/1).


/**
   client.

Start the WebSocket client. The URL is hardcoded to the ws://localhost:8080.

The handler is receive_all/0.
*/
client :-
    http_open_websocket('ws://localhost:8080', WS, []),
    ws_send(WS, text('Hello World!')),
    assert(wsock(WS)),
    receive_all,
    ws_close(WS, 1000, "Goodbye").

/**
   receive_all.

Receive a message and write to the standard output repeatidly. The WebSocket 
used is the one stored at the dynamic predicate wsock/1. 

No parameters is used, because of the repeat/0 statement. The later predicate 
will repeat indefinetly the current predicate while it fails. However, it is
not possible to pass a parameter for unification when the repetition happens.

@see wsock/1
*/
receive_all :-
    repeat,
    wsock(WS),
    ws_receive(WS, Reply),
    write(Reply), nl, flush_output,
    sleep(1),
    fail.
