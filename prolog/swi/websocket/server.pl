/*   server
     Author: poo.

     Copyright (C) 2018 poo

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     16 dic 2018
*/


:- module(server, [
	      produce/0,
	      server/1
	  ]).
/** <module> server: A WebSocket server that sends random numbers.

Creates a WebSocket server that after recieving a data, sends multiple random
number per two seconds.

@author Gimenez, Christian
@license GPLv3
@see http://www.swi-prolog.org/pldoc/man?section=websocket
*/

:- use_module(library(http/websocket), [
		  http_upgrade_to_websocket/3,
		  ws_send/2,
		  ws_receive/2
	      ]).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).

/*
  Handler for HTTP server for answering a GET /.

  HTTP Servers needs to be notified that the client will use a WebSocket. The
  first transaction has the Upgrade HTTP Header for telling the server to 
  change to a WebSocket connection. 

  http_upgrade_to_websocket/3 recieves the echo/1 predicate for handling the
  WebSocket connection.
*/
:- http_handler(root(.),
                http_upgrade_to_websocket(echo, []),
                [spawn([])]).

/**
   wsock(WebSocket: stream).

   Store the the WebSocket for the repeat argument. Just in case, we 
   dynamically store it.
*/
:- dynamic(wsock/1).

/**
   echo(+WebSocket: stream)

Handler for the WebSocket connection.

@param WebSocket Mandatory parameter for the http_upgrade_to_websocket/3.
*/
echo(WebSocket) :-
    ws_receive(WebSocket, Message),
    write(Message),
    assert(wsock(WebSocket)),
    send_data.

/**
   send_data.

Continuosly send random numbers to the WebSocket assigned no wsock/1. For 
each number, wait 2 seconds before sending the next. 
*/
send_data :-
    repeat,
    random(M),
    write(M),nl, flush_output,
    wsock(WebSocket2),
    ws_send(WebSocket2, text(M)),
    sleep(2),
    fail.

    
/**
   server(+Port: int)

Start a new thread which creates a new HTTP server. 

Example:

```
?- server(8080).
```

@param Port Use this port number. 
*/
server(Port) :-
    http_server(http_dispatch,
		[port(Port)]).


/**
   produce.

Write to Std Output random numbers per 2 second. Example of usage for the
repeat/0 statement which does not generate stack overflow.
*/
produce :-
    repeat,
    random(M),
    write(M), nl, flush_output,
    sleep(2),
    fail. %% Fail repeats.
