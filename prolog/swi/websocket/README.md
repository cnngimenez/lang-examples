# Start the server

Start the server:
```
$ swipl

?- consult(server).
true.

?- server(8080).
% Started server at http://localhost:8080/
true.

?- 

```

The server is available in other languages: CoffeeScript and JavaScript.

# Start the Client
Two alternatives: Use the Web Browser or start the SWI Prolog client.

## Use the HTML in a Web Browser
Open Firefox and open the file ./nodeej/ejemplo.html. Then, click on "Connect" and "Data" buttons.


## Use SWI
Start the interpreter like this:

```
$ swipl

?- consult(client).
true.

?- client.
websocket{data:0.21933427218474372,format:string,opcode:text}
websocket{data:0.14759196679409572,format:string,opcode:text}
...
```
