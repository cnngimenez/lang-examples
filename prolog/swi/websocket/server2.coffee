exports = exports ? this
exports.server = exports.server ? {}

# Standard libraries
http = require 'http'

# Importing libraries
cron = require "node-cron"
WebSocketServer = require('websocket').server

# Setting imported libraries into this
exports.http = http
exports.cron = cron
exports.WebSocketServer = WebSocketServer

# --------------------------------------------------

# HTTP Server
server = http.createServer (request, response) ->     
    console.log (new Date()) + ' Received request for ' + request.url
    response.writeHead 404
    response.end()  

server.listen 8080, () ->     
	console.log (new Date()) + ' Server is listening on port 8080'

wsServer = new WebSocketServer
    httpServer: server
	# You should not use autoAcceptConnections for production      
	# applications, as it defeats all standard cross-origin protection      
	# facilities built into the protocol and the browser. You should      
	# *always* verify the connection's origin and decide whether or not      
	# to accept it.      
    autoAcceptConnections: false     

# Control which origin can connect to this WebSocket server.
#
# @return boolean
originIsAllowed = (origin) ->
	# put logic here to detect whether the specified origin is allowed.      
    true

# Set request event
wsServer.on 'request', (request) ->
    if not originIsAllowed request.origin
        # Make sure we only accept requests from an allowed origin      
        request.reject()
        console.log (new Date()) + ' Connection from origin ' +
            request.origin + ' rejected.'
        return

    connection = request.accept 'echo-protocol', request.origin      
	
    console.log (new Date()) + ' Connection accepted.'

    connection.on 'message', (message) ->          
        if message.type is 'utf8'              
            console.log 'Received Message: ' + message.utf8Data
            counter = 0           
            new cron.schedule '* * * * * *', () ->
                counter++
                connection.sendUTF Math.random()
                # console.log "Hello" + counter
                , null, true, "America/Los_Angeles"
        else
            if message.type is 'binary'
                console.log 'Received Binary Message of ' +
                    message.binaryData.length + ' bytes'
                connection.sendBytes message.binaryData

    connection.on 'close', (reasonCode, description) ->
        console.log (new Date()) + ' Peer ' + connection.remoteAddress +
            ' disconnected.'


