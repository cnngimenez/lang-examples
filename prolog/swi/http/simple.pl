/*   simple
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     08 mar 2017
*/


:- module(simple, [	      
	  ]).
/** <module> simple: Http Simple


*/

% :- ensure_loaded(library(prosqlite)).
% :- ensure_loaded(library(db_facts)).
% :- use_module(...).

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).

server(Port) :-
    http_server(http_dispatch,
		[port(Port)]).

:- http_handler(root(.), entry_page, []).
:- http_handler(root(reqinfo), reqinfo, []).

entry_page(_Request) :-
    %% Header
    format('Content-Type: text/html~n~n',[]),
    %% Information
    format('<html><head><title>hola</title></head><body><h1>Hola</h1></body></html>',[]).

reqinfo(Request) :-
    %% Header
    format('Content-Type: text/html~n~n',[]),
    format('<html><head><title>Request info</title></head><body><div>', []),
    print_request(Request),
    format('</div></body></html>',[]).

print_request([]).
print_request([H|T]) :-
        H =.. [Name, Value],
        format('~w: ~w <br/>~n', [Name, Value]),
        print_request(T).
