:- module(cut,_,[assertions]).

:- doc(title, "CUT").
:- doc(author, "Giménez, Christian").
:- doc(copyright, "
cut.pl
Copyright (C) 2014 Giménez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

04 ene 2014
").

%% :- use_package(...).
%% :- use_package(library(...)).
%% :- use_module(...).

% Objetivo: implementar lo siguiente:
% Si X < 3 entonces Y = 0
% Si 3 <= X y X < 6 entonces Y = 2
% Si 6 <= X entonces Y = 4

f(X,0) :-
	X < 3.
f(X,2) :-
	3 =< X,
	X < 6.
f(X,4) :-
	6 =< X.

% Al probar ?- f(1,Y), 2 < Y  que es falso sucede lo siguiente:
% 
% 1.- Prueba f(1,0) (Y = 0, X = 1) si es cierto, dá cierto e intenta con 2 < 0 y es falso. Sucede Backtracking y sigue probando.
% 2.- Prueba f(1,2) (Y = 2, X = 1), da falso. Sucede backtracking otra vez.
% 3.- Prueba f(1,4) (Y = 4, X = 1), da falso. Sucede backtracking otra vez.
% 4.- No hay más que probar: retorna falso.

%%%%%%%%%%%%%%%%%%%%
% Segunda opción:

% Prevenir backtraking innecesario... 
f2(X,0) :- 
	X < 3, !.  % CUT Verde: No prueba con el que sigue.
f2(X,2) :- 
	3 =< X,
	X < 6, !.  % CUT Verde: No prueba con el que sigue.
f2(X,4) :-
	6 =< X.

% Al probar ?- f(1,Y), 2 < Y  que es falso sucede lo siguiente:
% 
% 1.- Prueba f(1,0) (Y = 0, X = 1) si es cierto:
%     1.a.- Intenta 1 < 3 y da verdadero.
%     1.b.- Encuentra CUT: A partir de aquí no sucede más backtracking.
%     1.c.- Es verdadero.
% 2.- Intenta con 2 < 0 (Y = 0) y es falso. No sucede backtracking debido al CUT.
% 3.- No hay más que probar: retorna falso.

%%%%%%%%%%%%%%%%%%%%
% Tercera opción:

% Uso del Cut para cambiar el sentido semántico:
% Si X < 3 entonces Y = 0, 
% en caso contrario, si X < 6 entonces Y = 2,
% en caso contrario, Y  = 4.

f3(X,0) :- X < 3, !. % CUT Rojo: En caso contrario...
f3(X,2) :- X < 6, !. % CUT Rojo: En caso contrario...
f3(X,4).

% Cut verde: cambia el sentido procedural del programa: "no sigas intentando(haciendo backtracking) ya que sé de antemano que el resto no va a unificar".
% Cut rojo: cambia el sentido procedural *y semántico*: "en otro caso..." o "en caso contrario...". Complejiza el algoritmo, por lo que es preferible no usarlo.

