:- module(gramaticas,_,[assertions]).

:- doc(title, "GRAMATICAS").
:- doc(author, "Giménez, Christian").
:- doc(copyright, "
gramaticas.pl
Copyright (C) 2014 Giménez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

17 ene 2014
").

:- use_package(dcg).

% Una gramática con términos, son todos las sentencias que tienen igual cantidad de aes y bes.
s1 --> [a], [b].
s1 --> [a], s1, [b].

% Aquí usamos strings en vez de términos.
s --> "a", "b".
s --> "a", s, "b". 

% Una consulta: ¿"aabb" pertenece a la representación <s>?
main :- 
	s("aabb", []).



% Se puede embeber código entre medio para calcular o realizar acciones al cumplir las reglas.

j(1) --> "a", "b".
j(D) --> "a", j(D2), "b", {D is D2 + 1}.  % D cuenta las iteraciones de la regla j.


% El predicado j acepta un parámetro más que es D:
main2 :-
	j(D, "aaabbb", []),
	display(D).