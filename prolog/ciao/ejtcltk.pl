:- module(ejtcltk,_,[assertions]).

:- doc(title, "TCLTK").
:- doc(author, "Giménez, Christian").
:- doc(copyright, "
tcltk.pl
Copyright (C) 2014 Giménez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

12 ene 2014
").

     :- use_module(library(tcltk)).

     go :-
             tcl_new(X),
             tcl_eval(X,[button,'.b',min(text),dq('Compute!')],_),
             tcl_eval(X,[button,'.c','-text',dq('Quit')],_),
             tcl_eval(X,[entry,'.e1',min(textvariable),'inputval'],_),
             tcl_eval(X,[label,'.l1',min(text),dq('The factorial of ')],_),
             tcl_eval(X,[pack, '.l1','.e1'],_),
             tcl_eval(X,[entry,'.e2',min(textvariable),'outputval'],_),
             tcl_eval(X,[label,'.l2',min(text),dq('is  ')],_),
             tcl_eval(X,[pack, '.l2','.e2'],_),
             tcl_eval(X,[pack,'.b','.c',min(side),'left'],_),
             tcl_eval(X,[bind,'.b','<ButtonPress-1>',
                     br([set,'inputval','$inputval','\n',
                     prolog_one_event,
                     dq(write(execute(tk_test_aux:factorial('$inputval', 'Outputval')))),
                     '\n',
                     set, 'outputval','$prolog_variables(Outputval)'])],
                     _),
             tcl_eval(X,[bind,'.c','<ButtonPress-1>',
                     br([prolog_one_event,
                     dq(write(execute(exit_tk_event_loop)))])],
                     _),
             tk_event_loop(X).
