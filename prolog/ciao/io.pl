:- module(io,_,[assertions]).

:- doc(title, "IO").
:- doc(author, "Giménez, Christian").
:- doc(copyright, "
io.pl
Copyright (C) 2014 Giménez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

04 ene 2014
").

%% :- use_package(...).
%% :- use_package(library(...)).
%% :- use_module(...).

:- pred main_out/0
	# "Ejemplo de uso de la salida a terminal.".

:- use_module(library(write)).
:- use_module(library(read)).

main_out :-
	display('hola mundo'), % es un símbolo
	nl, % nueva línea
	display("hola mundo"), % es un string
	nl,
	tab(2), % aserta cuando hace 2 espacios
	put_code(65), % imprime el caracter 65 ASCII
	write('hola mundo').
	
	
main_in :-
	write('Escriba su nombre(un término: debe terminar con punto)?'),
	read(Nombre),
	write(Nombre).