:- module(oper,_,[assertions]).

:- doc(title, "OPER").
:- doc(author, "Giménez, Christian").
:- doc(copyright, "
oper.pl
Copyright (C) 2014 Giménez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

12 ene 2014
").

:- use_module(library(operators)).

:- pred es_padre(Padre, Hijo) # "@var{Padre} es padre de @var{Hijo}.".

es_padre(pedro,juan).
es_padre(pedro, carlos).
es_padre(agustin,pedro).

%% Esta es una ejecución hecha en el ambiente de referencia del archivo.
:- op(600,xfx,es_padre). 

%% 600 es la prescedencia. Tiene que ser un número entre 0 y 1200. Mirar current_op/3 para ver los operandos.
%% xfx es el formato del operando (infijo, postifjo, prefijo). 
%%
%% Tipos de operadores:
%% - infija: xfx xfy yfx
%% - prefija: fx fy
%% - postfija: xf yf
%%
%% xfy indica que y debe tener más prescedencia que x en caso de ambigüedad. 
%% Por ejemplo: a-b-c
%% Se ejecuta (a-b)-c si - se define como yfx.
%% Se ejecuta a-(b-c) si - se define como xfy.

:- pred hermanos(X,Y) # "@var{X} es hermano de @var{Y}.".

hermanos(X,Y) :-
	Z es_padre X,
	Z es_padre Y.

:- op(600, xfx,hermanos).
