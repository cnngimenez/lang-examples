:- module(conectar_http,_,[assertions]).

:- doc(title, "CONECTAR_HTTP").
:- doc(author, "Giménez, Christian").
:- doc(copyright, "
conectar_http.pl
Copyright (C) 2014 Giménez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

15 ene 2014
").

:- use_package(pillow).

obtener(URL) :-
	url_info(URL, URLInfo),
	display(URLInfo),
	fetch_url(URLInfo, [], Resp),	
	buscar_resp(Resp).

buscar_resp(Resp) :-
	member(status(redirection,303,_Cont), Resp),
	member(location(URL),Resp),
	mostrar_resp(Resp),!,
	obtener(URL).

buscar_resp(Resp) :-
	mostrar_resp(Resp).
	

mostrar_resp([]).

mostrar_resp([status(S,Num, Cont)|R]) :-
	display(S), tab(1), display(Num), tab(1), display_string(Cont),!,nl,
	mostrar_resp(R).

mostrar_resp([content(Str)|R]) :-
	display_string(Terms),!,nl,
	mostrar_resp(R).

mostrar_resp([H|R]) :-
	display(H),nl,
	mostrar_resp(R).