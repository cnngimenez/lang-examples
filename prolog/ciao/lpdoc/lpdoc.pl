:- module(lpdoc,_,[assertions]).

:- doc(title, "LPDOC").
:- doc(author, "Gim�nez, Christian").
:- doc(copyright, "
lpdoc.pl
Copyright (C) 2014 Gim�nez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

05 ene 2014
").

%% :- use_package(...).
%% :- use_package(library(...)).
%% :- use_module(...).

:- doc(summary, "Descripci�n de los distintos elementos de LPDoc").

%% :- doc(section, "El predicado doc").

:- doc(module,"
@section{Inicio}
@begin{itemize}
@item Crear la carpeta @tt{doc}.
@item Crear y editar SETTINGS.pl.
@item Ejecutar @tt{lpdoc html}
@item Ejecutar @tt{lpdoc htmlview}
@end{itemize}

Recordar que el parseo de SETTINGS.pl no provoca error o warning alguno, si est� mal escrito o mal realizada la estructura no generar� documentaci�n.

@section{SETTINGS.pl}
Se debe usar una plantilla y modificar el archivo. La plantilla se localiza usualmente en /usr/local/lib/lpdoc o /usr/lib/lpdoc.

@subsection{A editar}
Se debe editar lo siguiente:

@begin{itemize} 
@item @tt{doc_structure} �Cu�l ser� la estructura de la documentaci�n a generar?
@item @tt{filepath} �D�nde est�n los archivos a generar documentaci�n?
@item @tt{systempath} para buscar los 'imports' del sistema base y generar documentaci�n de estos.
@end{itemize}

@subsection{systempath}
Es necesario indicar d�nde se encuentran los siguientes items para generar documetnaci�n de los predicados y m�dulos b�sicos de Ciao Prologs que se hacen referencia en este proyecto.

@begin{itemize}
@item @tt{lib}.
@item @tt{library}.
@item @tt{contrib}.
@item Otros directorios de posibles proyectos externos usados para construir este.
@end{itemize}

@subsubsection{filepath}
Se asigna los distintos directorios a procesar por lpdoc. Se separan con barras verticales (|).

@subsubsection{doc_structure}
Si @tt{doc_structure} se escribe mal, entonces no se generar� nada. Un ejemplo:

@begin{verbatim}
doc_structure := 'lpdoc' -['otra_doc'].
@end{verbatim}

@tt{lpdoc} es un archivo prolog (@tt{lpdoc.pl}) alcanzable por alg�n directorio indicado por @tt{filepath}, @tt{otra_doc} tambi�n. O sea, @em{todos los s�mbolos deben ser nombres de archivos} prologs existentes y alcanzables pero sin la extesi�n '.pl'.

@section{El Predicado doc}
El predicado @pred{doc/2} posee varias opciones.

En la documentaci�n del lpdoc se representa como formas de uso (@tt{doc(TipoVariable,TipoString)} y qu� puede unificar en cada uno).

El primer par�metro puede unificar con:

@begin{itemize}
@item title
@item subtitle
@item subtitle_extra
@item logo
@item author
@item address
@item ack
@item copyright
@item summary
@item module
@item appendix
@item usage
@item section
@item subsection
@item subsubsection
@item PredName (nombre/aridad de un predicado) 
@item bug
@item version
@item version_maintenance
@item doinclude
@item hide
@item filetype
@item nodoc
@end{itemize}

@subsection{Comandos en el texto}
El string posee una serie de @@ comandos, v�ase el predicado @pred{stringcommand/1} de lpdoc.

@section{Acentos y Problemas con el Encoding}
Posiblemente sea necesario traducir todo a ISO-8859-1 para evitar problemas con la traducci�n.

Para ello, en Emacs es preciso seleccionar todo y presionar @key{M-x} @tt{encode-coding-region} @tt{iso-8859-1}. Al guardar preguntar� el encoding a usar, de nuevo se debe escribir @tt{iso-8859-1}.

Al compilar la documentaci�n con lpdoc se generar� un HTML adecuado.

").