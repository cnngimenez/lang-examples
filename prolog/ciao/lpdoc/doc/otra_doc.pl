:- use_package([assertions]).

:- doc(filetype, documentation).

:- doc(title, "OTRA_DOC").
:- doc(author, "Giménez, Christian").
:- doc(copyright, "
otra_doc.pl
Copyright (C) 2014 Giménez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

06 ene 2014
").

:- doc(module, "Este archivo puede poseer documentación anexa.

Observar que debe usar el enunciado @tt{ :- doc(filetype, documentation). }
y en vez del predicado @tt{:- module} habitual, debe incorporar el paquete assertion directamente, así: @tt{:- use_package([assertions]).}.").


