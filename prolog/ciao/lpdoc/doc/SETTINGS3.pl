:- module(_, _, [ciaopaths, assertions, regtypes, fsyntax]).

:- include(lpdocsrc(lib('SETTINGS_schema'))).
% ****************************************************************************
% This is an LPdoc configuration file. See SETTINGS_schema for documentation *
% ****************************************************************************

:- use_module(library(system)).
:- use_module(library(terms),                   [atom_concat/2]).


:- doc(title, "Configuration File for Document Generation").

:- doc(module, "This is a sample configuration file for
   @apl{lpdoc}. The defaults listed are typically suggestions and/or
   the ones used for local installation in the CLIP group machines.
   These settings should be changed to suit your application.

This files comes with the lpdoc program in the Ciao Prolog proyect.
Authors of the template SETTINGS_DEFAULT.pl are the Ciao group.
@apl{lpdoc} is distributed under the GPL General Public License Version 2.").

:- doc(bug, "Definitions that are overriden by the emacs mode must fit
   in one line").

:- doc(author, "Template: Ciao Prolog Group.").
:- doc(author, "Giménez, Christian Nelson").

:- doc(filetype, user).
filepath := '../' | './'.

systempath := '/usr/local/lib/ciao/ciao-1.15/lib'|'/usr/local/lib/ciao/ciao-1.15/library'|'/usr/local/lib/ciao/ciao-1.15/contrib'.

pathsfile(_) :- fail.  % kludge to avoid compilation error

output_name := _ :- fail.

%%%% hay que editar esto en base a la estructura del documento que se desea.
doc_structure := 	
	'ejemplo' -[
			  'otra_doc',
			  'lpdoc'
		    ].


commonopts := no_bugs|no_patches.
doc_mainopts := ~commonopts.
doc_compopts := ~commonopts.

docformat := texi|ps|pdf|manl|info|html.

index := concept.
index := pred.
index := prop.
index := regtype.
index := modedef.
index := global.

bibfile := '/home/clip/bibtex/clip/clip'.
bibfile := '/home/clip/bibtex/clip/others'.

startpage := 1.

papertype := afourpaper.

libtexinfo := 'yes'.

htmldir := ''.
docdir := '/home/clip/public_html/Local/lpdoc_docs'.
infodir := '/home/clip/public_html/Local/lpdoc_docs'.
mandir := '/home/clip/public_html/Local/lpdoc_docs'.

datamode(perm(rw, rw, r)).
execmode(perm(rwx, rwx, rx)).

%infodir_headfile := ~atom_concat([~lpdoclib, '/Head_clip.info']).
%infodir_tailfile := ~atom_concat([~lpdoclib, '/Tail_clip.info']).
% TODO: This is defined automatically by lpdoc, but not accessible here. Fix
lpdoclib := '/usr/local/lib/lpdoc/lpdoc-3.0'.

% ----------------------------------------------------------------------------
% End of SETTINGS
