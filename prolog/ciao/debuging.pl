:- module(debuging,_,[assertions]).

:- doc(title, "DEBUGING").
:- doc(author, "Giménez, Christian").
:- doc(copyright, "
debuging.pl
Copyright (C) 2014 Giménez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

17 ene 2014
").

%% Comienza el debugging y se detendrá ante un spy, breakpoint o trace.
:- use_package(debug).

%% Nodebug deshabilita todo tipo de debugging permitiendo dejar las cláusulas propias del debug.
% :- use_package(nodebug).

%% Comienza la traza inmediatamente.
% :- use_package(trace).

% trace.

% notrace.

% spy(predicate).

% nospy(predicate).

% breakpt

main :-
	trace, % Comienza la traza aquí.
	display('hola mundo'),	
	repeat.

main2b :- 
	nl,
	display('main2b'),
	nl.

main2 :-
	main2b,
	display('hola 2'),
	spy(main2b/0), % al llamar main2b por segunda vez comienza el debugging 
	main2b.	
	