:- module(listas,_,[assertions]).

:- doc(title, "LISTAS").
:- doc(author, "Giménez, Christian").
:- doc(copyright, "
listas.pl
Copyright (C) 2014 Giménez, Christian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

12 ene 2014
").

%% :- use_package(...).
%% :- use_package(library(...)).
%% :- use_module(...).

:- doc(module, "Las listas son un tipo de dato primitivo. Se escriben con []. 
Se pueden identificar la cabeza del cuerpo de la lista con @code{[Cabeza|Cuerpo]} donde @var{Cuerpo} es una lista.

Los string son listas de números interpretados como caracteres.").

:- pred miembro(X,Y) # "¿@var{X} es miembro de la lista @var{Y}?".

miembro(X, [X|_]).
miembro(X, [A|R]) :-
	X \= A,
	miembro(X,R).

:- pred conc(X,Y,-R) # "@var{R} es la concatenación de @var{X} e @var{Y}.".

conc(X,[], X).
conc([],Y, Y).
conc([X|L],Y,[X|R]) :-
	conc(L,Y,R).