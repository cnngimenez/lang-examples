#! /usr/bin/gprolog --consult-file 

% mundo_cerrado.pl
% Copyright (C) 2014 Giménez, Christian

% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.

% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% 08 sep 2014

%% :- include(...).



%% GNU Prolog no es mundo cerrado a menos que se le indique:
:- set_prolog_flag(unknown, fail).

%% set_prolog_flag/2 permite controlar el comportamiento del intérprete. 

%% unknown = error por defecto.
%% Puede ser: error, fail, warning.

%% current_prolog_flag/2 permite obtener información por defecto o asignada por set_prolog_flag/2.
q :- current_prolog_flag(unknown, A),
     display('mundo cerrado:'),
     display(A).

:- initialization(q).
