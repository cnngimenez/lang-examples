#! /usr/bin/gprolog --consult-file

% modulos.pl
% Copyright (C) 2014 Giménez, Christian

% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.

% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% 07 sep 2014

%% :- include(...).


%% En GNU Prolog no existen módulos, esto es debido a que el estándar ISO de prolog no establece esta característica. Sí establece la posibilidad de incluir archivos con la directiva include/1 o con el predicado consult/1.

%% Se incluye los predicados de mod2.pl (o sea, q2/0).
%% include/1 es una directiva y no puede usarse en el cuerpo de una definición de un predicado.
:- include('mod2.pl').

q :- nl,display('incluyendo hola_mundo.pl'), nl,nl,
     consult('hola_mundo.pl'), %% se le pide a gprolog que busque, precompile y ejecute hola_mundo.pl, luego que ejecute initialization/2 de hola_mundo.pl. 
     nl,nl,
     q2,nl,nl.



%% ¡Guárda con realizar conflictos de nombre! 
%% Al no haber visibilidad por módulos, cualquier predicado en un archivo incluido es visible globalmente.
:- initialization(q).
