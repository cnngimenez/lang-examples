#! /usr/bin/gprolog --consult-file

% hola_mundo.pl
% Copyright (C) 2014 Giménez, Christian

% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.

% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% 07 sep 2014

%% :- include(...). 



%% La primer línea '#! /usr/bin/gprolog --consult-file' es considerada un comentario por el intérprete ya que es utilizada para realizar programas de tipo scripting.

p :- display('hola mundoooo!'),
     nl, 
%     halt. %% Termina el intérprete.
     nl.

%%
% Al consultarlo por medio de consult(...) o del parámetro --consult-file, initialization/1 indica el predicado a ejecutar al terminar la carga.
:- initialization(p).


