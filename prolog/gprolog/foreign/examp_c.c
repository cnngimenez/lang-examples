/* 

   Copyright 2014 Giménez, Christian
   
   Author: Giménez, Christian   

   examp_c.c
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <gprolog.h>

// first_occurrence(+string, +char, -positive).
PlBool first_occurrence(char * str, PlLong c, PlLong *pos){
  char *p;
  
  p = strchr(str, c);
  
  if (p == NULL){
    return PL_FALSE;
  }

  *pos = p - str;
  return PL_TRUE;
}

/*
  Ejemplo sacado del manual de GProlog.
  
  Compilar con:

  gplc examp.pl examp_c.c
*/
